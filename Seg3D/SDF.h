#ifndef SDF_H
#define SDF_H

using namespace std;
using namespace CGAL;

bool doesRayIntersectPlane(const Ray &r, const Plane &p, Vector &V);
void facetItrToPolygon(Facet_iterator F, MyPolygon &P);
ostream& operator <<(ostream &out, MyPolygon &P);
bool doesRayIntersectPolygon(const Ray &r, const MyPolygon &P);
double dist(const Point &P, const Point &Q);
double dist(const MyPolygon &A, const MyPolygon &B);

#endif // SDF_H
