//
//  Graph.h
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 03/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//


#ifndef __HelloCGAL__Graph__
#define __HelloCGAL__Graph__

#include <iostream>
#include <vector>
#include <utility>
#include <map>
#include <set>
#include <tr1/unordered_set>
#include <tr1/unordered_map>

#include "Dijkstra.h"


using namespace std;
using namespace CGAL;


typedef tr1::unordered_map <label_t, weight_t> elist_t;
typedef tr1::unordered_map <label_t, elist_t> adjlist_t;

typedef tr1::unordered_map <label_t, weight_t>::iterator eitr_t;
typedef tr1::unordered_map <label_t, elist_t>::iterator vitr_t;

typedef enum {C_WHITE, C_GRAY, C_BLACK} color_t;

typedef tr1::unordered_map <label_t, weight_t> dmap_t;
typedef tr1::unordered_map <label_t, label_t>  imap_t;
typedef tr1::unordered_map <label_t, color_t>  colormap_t;


class MapAdjList
{
    public:
    adjlist_t adj;
       
    //map <label_t, weight_t> dist;
    //map <label_t, color_t> color;
    dmap_t dist;
    colormap_t color;
    imap_t parent;
    
    bool isDirected;
    
    bool add_vertex(const label_t &v);
    void add_edge(const label_t &u, const label_t &v, const weight_t &w);
    unsigned long numVertices();
    unsigned long numEdges();
    friend ostream& operator<<(ostream& out, MapAdjList &A);
   
    
};




#endif /* defined(__HelloCGAL__Graph__) */
