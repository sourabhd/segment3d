//
//  Dijkstra.h
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 02/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __HelloCGAL__Dijkstra__
#define __HelloCGAL__Dijkstra__

#include <iostream>
#include <map>

#include "common.h"
#include "Graph.h"
#include "CGALPolyhedron2OFF.h"
#include "projutils.h"

using namespace std;

class MapAdjList;


static inline double Unweighted(Vertex x, Vertex y) 
{ return 1.0; }

static inline double Euclidean(Vertex x, Vertex y)
{
    return sqrt(squared_distance(x.point(),y.point()));
}

void conv2Adj(Polyhedron &P, MapAdjList &A, map<Idx, Point> &C, EdgeFn efn, weight_t **M);

typedef pair<weight_t,label_t> vpair_t;

void floyd(weight_t **M, weight_t **D, label_t **P, const int N);


struct ShortestDist
{
    bool operator()(pair<label_t, weight_t> a, pair<label_t, weight_t> b)
    {
        return a.second < b.second;
    }
    
};

void dijkstra(MapAdjList &G, label_t source);
void calcAllDistances(MyMesh &msh, vector<Idx> &F,vector<double> &distance);

#endif /* defined(__HelloCGAL__Dijkstra__) */
