//
//  feature.h
//  Segment3D
//
//  Created by Sourabh Daptardar on 04/12/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __Segment3D__feature__
#define __Segment3D__feature__

#include "common.h"
#include "DihedralAngles.h"
#include "Graph.h"
#include "Dijkstra.h"
#include "CGALPolyhedron2OFF.h"
#include "BGLAlgo.h"


using namespace std;
using namespace boost;

extern map<string,Idx> distinctPartsId;
extern map<Idx,string> distinctPartsName;



void calcUnaryFeatures(Meshes_t &M, const string &category, vector<int> &Ids, map<string,int>& CatStartNum, data_t &data);
void calcUnaryFeatures2(Meshes_t &M, const string &category, vector<int> &Ids, map<string,int>& CatStartNum, data_t &data);
void calcPairwiseFeatures(Meshes_t &M, const string &category, vector<int> &Ids, map<string,int>& CatStartNum, data_t &data);
void calcPairwiseFeatures2(Meshes_t &M, const string &category, vector<int> &Ids, map<string,int>& CatStartNum, data_t &data);
void calcFeatures(Meshes_t &M, const string &category, vector<int> &Ids, map<string,int>& CatStartNum, data_t &data);
void showPairwiseFeatures(MyMesh &msh, const string &onamePairwise, const double minf, const double maxf);
void showUnaryFeatures(MyMesh &msh, const string &onameUnary, const double minf, const double maxf);
void normalizeUnaryFeatures(MyMesh &M, const double minf[], const double maxf[]);

void averageGeodesicDistance(MyMesh &M, double &minagd, double &maxagd, double & avgagd, double &devagd);
void calcMeadianGeodesicDistance(MyMesh &msh, double &medianDist);

#endif /* defined(__Segment3D__feature__) */
