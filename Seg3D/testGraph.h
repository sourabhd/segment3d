//
//  testGraph.h
//  Segment3D
//
//  Created by Sourabh Daptardar on 30/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __Segment3D__testGraph__
#define __Segment3D__testGraph__

#include <iostream>
#include "common.h"
#include "Graph.h"
#include "Dijkstra.h"
#include "projutils.h"
#include "BGLAlgo.h"

using namespace std;

void testGraph(void);


#endif /* defined(__Segment3D__testGraph__) */
