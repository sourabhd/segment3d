#include "common.h"
#include "SDF.h"
#include "DihedralAngles.h"

// Reference for intersection tests: https://sites.google.com/site/justinscsstuff/object-intersection

using namespace std;
using namespace CGAL;

void facetItrToPolygon(Facet_iterator F, MyPolygon &P)

{
    int N = 0;
    double x = 0.0, y = 0.0, z = 0.0;
    double cx = 0.0, cy = 0.0, cz = 0.0;
    Halfedge_facet_circulator circ = F->facet_begin();
    P.vertex.clear();
    do {
        //cout << circ->vertex()->id() << " ";
        Point Current = circ->vertex()->point();       // co-ordinates of current and next vertex
        Point Next = circ->next()->vertex()->point();

        Vector V(Point(0.0,0.0,0.0),Current);

        P.vertex.push_back(V);

        cx = cx + Current.x();
        cy = cy + Current.y();
        cz = cz + Current.z();

        x = x + ((Current.y() - Next.y()) * (Current.z() + Next.z()));
        y = y + ((Current.z() - Next.z()) * (Current.x() + Next.x()));
        z = z + ((Current.x() - Next.x()) * (Current.y() + Next.y()));
        N++;

    } while ( ++circ != F->facet_begin());

    double len = sqrt(x * x + y * y + z * z);
    x = x / len;
    y = y / len;
    z = z / len;

    cx /= double(N);
    cy /= double(N);
    cz /= double(N);

    P.normal = Vector(x,y,z);
    P.centroid = Point(cx,cy,cz);
    assert(N == int(P.vertex.size()));
    P.numVertices = N;
    P.plane = Plane(P.centroid, P.normal);
}

ostream& operator <<(ostream &out, MyPolygon &P)
{
    out << "NumVertices : " << P.numVertices << endl;
    out << "Vertices : " << endl;
    for (int i = 0 ; i < P.numVertices ; i++) {
        out << P.vertex[i].x() << " " << P.vertex[i].y() << " " << P.vertex[i].z() << endl;
    }
    out << "Normal: ";
    out << P.normal.x() << " " << P.normal.y() << " " << P.normal.z() << endl;
    out << "Centroid: ";
    out << P.centroid.x() << " " << P.centroid.y() << " " << P.centroid.z() << endl;
    out << "Plane: ";
    out << P.plane.a() << " " << P.plane.b() << " " << P.plane.c() << " " << P.plane.d() << endl;
    return out;
}

bool doesRayIntersectPlane(const Ray &r, const Plane &p, Vector &V)
{
    V = Vector(0.0,0.0,0.0);
    Vector P_N = p.orthogonal_direction().vector();
    Vector P_P = Vector(Point(0.0,0.0,0.0),p.point());
    Vector R_N = r.direction().vector();
    Vector R_P = Vector(Point(0.0,0.0,0.0),r.source());

    double den = P_N * R_N;   // dot product
    //cout << "Denominator : " << den << endl;

    if ((den < EPS) && (den > -EPS)) {
        return false;
    }

    // return r.pos + r.dir * (-dot(p.n, r.pos - p.a) / nDotD);
    //V = (R_P + (R_N * (P_N * (R_P  - P_P) ) / den));
    double s = -1.0 * P_N * ((R_P - P_P) / den);
    if (s < 0)
        return false;

    V = R_P + s * R_N;
    return true;
}

bool doesRayIntersectPolygon(const Ray &r, const MyPolygon &P)
{
    Vector V(0.0,0.0,0.0);

    bool bInt = doesRayIntersectPlane(r,P.plane,V);
    if (bInt == false)
        return false;

    for (int i = 0 ; i < P.numVertices ; i++) {
        double dotProd = cross_product(P.vertex[(i+1)%P.numVertices] - P.vertex[i], V - P.vertex[i]) * P.normal;
        if (dotProd < 0)
            return false;
    }

    return true;
}

double dist(const Point &P, const Point &Q)
{
    double xd = P.x() - Q.x();
    double yd = P.y() - Q.y();
    double zd = P.z() - Q.z();
    return sqrt((xd*xd+yd*yd+zd*zd));
}


double dist(const MyPolygon &A, const MyPolygon &B)
{
    return dist(A.centroid, B.centroid);
}
