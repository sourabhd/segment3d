//
//  Dual.cpp
//  Segment3D
//
//  Created by Sourabh Daptardar on 26/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//



#include "Dual.h"




void Dual::operator()(HalfedgeDS &hds)
{

    Builder B(hds,true);
    
    //cout << "In dualizer " << mesh.vertices_begin()->id() << endl;
    cout << "F: " << mesh.size_of_facets() << endl;
    cout << "V: "  << mesh.size_of_vertices() << endl;
    B.begin_surface(mesh.size_of_facets(), mesh.size_of_vertices(),0, 0);
    
    
    Vertex_iterator vertexBegin = mesh.vertices_begin();
    Vertex_iterator vertexEnd = mesh.vertices_end();
    
    
    unsigned long int i = 0;
#if 0
    for (Vertex_iterator vitr = vertexBegin; vitr != vertexEnd ; ++vitr) {
        vitr->id() = i;
        i++;
    }
#endif
    
    i = 0;
    
    vector<Vertex> DualVertices;
    DualVertices.clear();
    

    Facet_iterator facesBegin = mesh.facets_begin();
    Facet_iterator facesEnd = mesh.facets_end();
   
#if 0
    for (Facet_iterator fitr  = facesBegin; fitr != facesEnd; ++fitr) {
        fitr->id() = i++;
    }
#endif
    
    // Iterate over faces in the original mesh to vertices in the dual mesh
    
    facesBegin = mesh.facets_begin();
    facesEnd = mesh.facets_end();
    vertex_id_t ver;
    bitmap_t vertex_map;
   
    for (Facet_iterator fitr  = facesBegin; fitr != facesEnd; ++fitr) {
        
        //cout << "Face: " << fitr->id() << endl;
        
        Halfedge_handle h = fitr->halfedge();
        Halfedge_facet_circulator v = h->facet_begin();
        
        vector<Point> V; V.clear();
               do {
            //cout << v->opposite()->face()->id() << endl;
            V.push_back(v->vertex()->point());
            
        } while ( ++v != h->facet_begin() );
        
        
        
#if 0
        // Alternative way, only for triangular meshes
        // cout << "1: " << h->opposite()->face()->id() << endl;
        // cout << "2: " << h->next()->opposite()->face()->id() << endl;
        // cout << "3: " << h->next()->next()->opposite()->face()->id() << endl;
        
        
        // cout << centroid(h->vertex()->point(),h->next()->vertex()->point(),h->next()->next()->vertex()->point()) << endl;
#endif
        
        Vertex dualVertex = Vertex(centroid(V.begin(), V.end()));
        
        dualVertex.id() = fitr->id();  // Face vertex duality
        DualVertices.push_back(dualVertex);    
        ver.insert(make_pair(fitr->id(), dualVertex)); // store the mapping in both directions
        
        vertex_map.insert(make_pair(fitr->id(), false));
        
    } // end fitr loop
    
    // Push all the dual vertices
    for (int i = 0 ; i < int(DualVertices.size()) ; i++) {
        // cout << DualVertices[i].id() << " " << DualVertices[i].point() << endl;
        if (vertex_map[DualVertices[i].id()] == true) {
            
        } else {
            B.add_vertex(DualVertices[i].point());
            vertex_map[DualVertices[i].id()] = true;
        }
    }
    
    // Iterate over all vertices of original mesh to create faces
    
    vertexBegin = mesh.vertices_begin();
    vertexEnd = mesh.vertices_end();
    for (Vertex_iterator vitr  = vertexBegin; vitr != vertexEnd; ++vitr) {
        
        // cout << "Vertex: " << vitr->id() << endl;
        Halfedge_handle h = vitr->halfedge();
        Halfedge_vertex_circulator v = h->vertex_begin();
        
        vector<Idx> V; V.clear();
        Facet F;
        facever_t sid; sid.clear();
        
        do {
            //cout << v->opposite()->face()->id() << endl;
            // cout << v->face()->id() << endl;
            Vertex dualVertex = Vertex(ver[v->face()->id()].point());
            dualVertex.id() = v->face()->id();
                        // Note, face is vertex in the dual representation
            V.push_back(dualVertex.id()); // SDD:check
            //V.push_back(dualVertex.id()-1);
            
        } while ( ++v != h->vertex_begin());
        
       
   //     unsigned long long  numV = V.size();
       
        bool rightDir = B.test_facet(V.begin(), V.end());
        
        if (rightDir) {
            B.add_facet(V.begin(), V.end());
        } else {
            B.add_facet(V.rbegin(), V.rend());
        }
    }
    
    B.end_surface();
}

Polyhedron Dualizer::dual()
{
    Polyhedron output;
    Dual dualizer(mesh);
    output.delegate(dualizer);
    setIDsIfRequired(output);
    //cout << "in dualizer:dual: " << output.vertices_begin()->id() << endl;
    return output;
}

Polyhedron dual(Polyhedron &P)
{
    Dualizer dualizer(P);
    Polyhedron D = dualizer.dual();
    //cout << "ORIGINAL:" << endl << P << endl;
    //cout << "DUAL:" << endl << D << endl;
    return D;
}


