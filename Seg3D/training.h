//
//  training.h
//  Segment3D
//
//  Created by Sourabh Daptardar on 01/12/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __Segment3D__training__
#define __Segment3D__training__

#include <iostream>
#include "common.h"
#include "run.h"
#include "ClassifierWrapper.h"

using namespace std;

void splitDataset(const int ratio_x, const int ratio_y, const int N, vector<int> &exemplars, vector<int> &validation);
void train(Meshes_t &M, const string &category, const vector<int> &exemplarIds);
void learn(const string &outdir, Idx meshid, bool isUnary,data_t &trdata, data_t &tedata, Meshes_t &meshes, string &category, double prob[],tr1::unordered_map<string,int> &CatNumLabels);


#endif /* defined(__Segment3D__training__) */
