dylibdir="/Applications/MATLAB_R2011b.app/bin/maci64"
files="$dylibdir/*.dylib"
for x in $files
do
    origName="`otool -L $x | xargs`"
    echo $origName | awk -v dname=$dylibdir '{ 
        sub(/:$/,"",$1)
        if ( $2 ~ "^@" ) {
            oldName = $2
            sub(/@rpath/,dname,$2)
            sub(/@loader/,dname,$2)
            print "install_name_tool -id", $2, $1 
        }
        for (i = 3 ; i <= NF ; i++) {
            if ( $(i) ~ "^@" ) {
                oldName = $(i)
                sub(/@rpath/,dname,$(i))
                sub(/@loader/,dname,$(i))
                print "install_name_tool -change", oldName, $(i), $1 
            }
        }
    }'
done
