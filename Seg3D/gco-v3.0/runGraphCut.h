#ifndef RUNGRAPHCUT_H
#define RUNGRAPHCUT_H

#include "../common.h"

using namespace std;
using namespace CGAL;
using namespace boost;

void runGraphCut(Meshes_t &meshes, const string &category, const int imgNum,tr1::unordered_map<string,int> & CatNumLabels);

#endif // RUNGRAPHCUT_H
