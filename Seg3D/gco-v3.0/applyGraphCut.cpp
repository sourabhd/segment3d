//
//  applyGraphCut.cpp
//  Segment3D
//
//  Created by Sourabh Daptardar on 08/12/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

//#include "applyGraphCut.h"
#include "common.h"
//#include "GCoptimization.h"

using namespace std;
using namespace CGAL;
using namespace boost;


void graphCut(data_t &tedata, double prob[],data_t &tedataPairwise,double probPairwise[])
{
    int num_labels = 4;
    
    // Unary terms
    
    int num_pixels = int(tedata.X.size());
    int *data = new int[num_pixels*num_labels];

    for (int i = 0 ; i < num_pixels ; i++) {
        cout	 << tedata.unaryIds[i] << endl;
        data[i] = -log(prob[i]);
        
    }
 
    // Pairwise terms
    
    
    
#if 0
    int *result = new int[num_pixels];   // stores result of optimization

    
    try{
        GCoptimizationGeneralGraph *gc = new GCoptimizationGeneralGraph(num_pixels,num_labels);
        gc->setDataCost(data);
        
        int sz = int(tedataPairwise.pairWiseIds.size());
        for (int i = 0 ; i < sz ; i++) {
            //cout << tedataPairwise.pairWiseIds[i].first  << "," << tedataPairwise.pairWiseIds[i].second << endl;
            //cout << "pair wise terms:" <<  -log(probPairwise[i]) << endl;
            gc->setSmoothCost(int(tedataPairwise.pairWiseIds[i].first), int(tedataPairwise.pairWiseIds[i].second),  -log(probPairwise[i]));
            
        }
            
        printf("\nBefore optimization energy is %d",gc->compute_energy());
        gc->expansion(2);// run expansion for 2 iterations. For swap use gc->swap(num_iterations);
        printf("\nAfter optimization energy is %d",gc->compute_energy());
        
        for ( int  i = 0; i < num_pixels; i++ )
            result[i] = gc->whatLabel(i);
        
        delete gc;
    }
    catch (GCException e){
        e.Report();
    }
    
#endif
    
//    delete [] result;
//    delete [] smooth;
    delete [] data;
}