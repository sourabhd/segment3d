//
//  applyGraphCut.h
//  Segment3D
//
//  Created by Sourabh Daptardar on 08/12/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __Segment3D__applyGraphCut__
#define __Segment3D__applyGraphCut__

#include <iostream>
#include "common.h"
//#include "GCoptimization.h"
//#include "LinkedBlockList.h"
//#include "applyGraphCut.h"
//#include "block.h"
//#include "energy.h"
//#include "graph.h"

using namespace std;
using namespace boost;
using namespace CGAL;

void graphCut(data_t &tedata,double prob[],data_t &tedataPairwise,double probPairwise[]);


#endif /* defined(__Segment3D__applyGraphCut__) */
