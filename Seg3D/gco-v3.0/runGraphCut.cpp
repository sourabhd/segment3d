#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <string>
#include <cstring>
#include <ctime>
#include "GCoptimization.h"
#include "runGraphCut.h"
#include "../common.h"

using namespace std;

//typedef  Idx  SiteID;             // index of a site (pixel); default is int32
//typedef  int  LabelID;            // index of a label;        default is int32
//typedef  double  EnergyType;      // total energy value;      default is int64
//typedef  double  EnergyTermType;  // individual energy term;  default is int32

double SmoothCostFnExtraSeg(int s1, int s2, int l1, int l2, void *data)
{
    MyMesh *pmsh = (MyMesh *)data;
    pair <Idx,Idx> pr = make_pair(s1,s2);
    return ((pmsh->PMap[pr].argmaxProb ==  pmsh->PMap[pr].argmaxProb) ? 0.0 : -1.0 * log(pmsh->PMap[pr].maxProb));
}

void runGraphCut(Meshes_t &Meshes, const string &category, const int imgNum,tr1::unordered_map<string,int> & CatNumLabels)
{
    int num_pixels = Meshes[category][imgNum].M.size_of_facets() ;
    int num_labels = CatNumLabels[category];

    cout << "Number of pixels: " << num_pixels << endl;
    cout << "Number of labels: " << num_labels << endl;

    clock_t c1 = clock();
    time_t t1 = time(NULL);


    try {
        GCoptimizationGeneralGraph *gc = new GCoptimizationGeneralGraph(num_pixels,num_labels);

        // smoothness and data costs
        Facet_iterator fBegin = Meshes[category][imgNum].M.facets_begin();
        Facet_iterator fEnd = Meshes[category][imgNum].M.facets_end();
        for (Facet_iterator fitr = fBegin ; fitr != fEnd ; ++fitr) {
            // unary potentials
            Idx i = fitr->id();
            for (int l = 0 ; l < num_labels ; l++) {
                double e = -1.0 * log(Meshes[category][imgNum].UMap[i].probFeatureVector[l]);
                gc->setDataCost(i,l,e);
            }
        }

        Edge_iterator edgesBegin = Meshes[category][imgNum].M.edges_begin();
        Edge_iterator edgesEnd = Meshes[category][imgNum].M.edges_end();

        for (Edge_iterator itr = edgesBegin; itr != edgesEnd; ++itr) {
            // pairwise potenials
            Idx fid1 = itr->face()->id();
            Idx fid2 = itr->opposite()->face()->id();
            if (fid1 >= Meshes[category][imgNum].M.size_of_facets())
                continue;
            if (fid2 >= Meshes[category][imgNum].M.size_of_facets())
                continue;
            pair<Idx,Idx> pr = make_pair(fid1,fid2);

            //cout << fid1 << " " << fid2 << " " << num_pixels << endl;
            gc->setNeighbors(fid1,fid2);
            for (int l = 0 ; l < num_labels ; l++) {
               gc->setSmoothCost(SmoothCostFnExtraSeg,&(Meshes[category][imgNum].M));
            }
        }

        cout << "Energy before optimation: " << gc->compute_energy() << endl ;
        gc->expansion();// run expansion for n iterations. For swap use gc->swap(num_iterations);
        cout << "After optimization energy :" << gc->compute_energy() << endl;


        for (Idx  i = 0; i < num_pixels; i++ ) {
            cout << i << ":" << gc->whatLabel(i) << endl;
            Meshes[category][imgNum].UMap[i].predLabel = gc->whatLabel(i);
        }

        delete gc;
    } catch (GCException e) {
        e.Report();
    }


    clock_t c2 = clock();
    time_t t2 = time(NULL);
    cout << "Time : " << (t2 - t1) << "seconds" << " " << "Clock : " << (c2-c1) / CLOCKS_PER_SEC << " seconds" << endl;

}
