//
//  testDual.cpp
//  Segment3D
//
//  Created by Sourabh Daptardar on 27/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include "testDual.h"
#include "CGALPolyhedron2OFF.h"
#include "ReadDataset.h"


void testDual(void)
{
    
#if  0
    Point p(0.0,0.0,0.0);
    Point q(1.0,0.0,0.0);
    Point r(0.0,1.0,0.0);
    Point s(0.0,0.0,1.0);
    
    
    Polyhedron P;
    P.make_tetrahedron(p,q,r,s);
#endif
    
#if 1
    string modelfile = "/Users/sourabhdaptardar/Datasets/MeshsegBenchmark-1.0/data/off/241.off";
    //string modelfile = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/models/umu/cube.off";
    Polyhedron P;
    CGALPolyhedron2OFF::OFF2Poly(P, modelfile);
#endif
    
#if 1
    Dualizer dualizer(P);
    Polyhedron D = dualizer.dual();
#endif
    
    const string outdir = OutputDir + filesep + "Bird";
    stringstream s1;
    s1 << outdir << filesep << "dual_test_in" << ".off";
    string inFile = s1.str();
    CGALPolyhedron2OFF::poly2OFF(P, inFile);
        
#if 1
    stringstream s2;
    s2 << outdir << filesep << "dual_test_out" << ".off";
    string outFile = s2.str();
    CGALPolyhedron2OFF::poly2OFF(D, outFile);
#endif
    
    
}