#include "common.h"
#include "testCurvature.h"
#include "CGALPolyhedron2OFF.h"
#include "runCurvature.h"
#include "Curvature.h"

using namespace std;
using namespace CGAL;

//const string offFileName = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/shapes/Berkeley/Sphere/sphere.off";
//const string outputOffFileName = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/shapes/Berkeley/Sphere/sphere_out.off";

const string offFileName = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/shapes/Berkeley/models/hand.off";
const string outputOffFileName = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/shapes/Berkeley/models_out/hand_out.off";

//const string offFileName = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/shapes/Berkeley/models/teapot.off";
//const string outputOffFileName = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/shapes/Berkeley/models_out/teapot_out.off";


void testCurvature(void)
{
    testCurvature(offFileName);
}

void testCurvature(const string &offFile)
{
    MyMesh msh;
    CGALPolyhedron2OFF::OFF2MyMesh(msh,offFile);

    const Idx id = 0;
    const string category = "shape";

    calcCurvatureFeature(msh, id, category,
                         offFileName,
                         outputOffFileName,
                         msh.poff);

}
