//
//  hds1.cpp
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 02/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include "hds1.h"
#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>
#include <iomanip>

#include <CGAL/Simple_cartesian.h>
#include <CGAl/Polyhedron_3.h>


using namespace std;
using namespace CGAL;

typedef CGAL::Simple_cartesian<double> Kernel;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;
typedef Polyhedron::Halfedge_handle Halfedge_handle;
typedef Polyhedron::Point_3 Point;
typedef Polyhedron::Vertex_iterator Vertex_iterator;

void hds1(void)
{
    Point p( 1.0, 0.0, 0.0);
    Point q( 0.0, 1.0, 0.0);
    Point r( 0.0, 0.0, 1.0);
    Point s( 0.0, 0.0, 0.0);
    Polyhedron P;
    Halfedge_handle h = P.make_tetrahedron(p,q,r,s);
    CGAL_assertion(P.is_tetrahedron(h));
    //CGAL::set_ascii_mode(std::cout);
    Vertex_iterator begin = P.vertices_begin();
    Vertex_iterator end = P.vertices_end();
    
    for(Vertex_iterator itr = begin; itr != end; ++itr) {
        cout << "(" << itr->point() << ") ";
    }
    cout << endl;
}

