//
//  CGALPolyhedron2OFF.h
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 02/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __HelloCGAL__CGALPolyhedron2OFF__
#define __HelloCGAL__CGALPolyhedron2OFF__

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <string>
#include <iomanip>

#include "common.h"


#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h> 
#include <CGAL/IO/Polyhedron_geomview_ostream.h>


using namespace std;
using namespace CGAL;

class CGALPolyhedron2OFF
{
public:
    static bool poly2OFF(Polyhedron &P, ofstream &out);
    static bool poly2OFF(Polyhedron &P, const string &offFile);
    static bool OFF2Poly(Polyhedron &P, ifstream &in);
    static bool OFF2Poly(Polyhedron &P, const string &offFile);
    static bool MyMesh2OFF(MyMesh &msh, ofstream &out);
    static bool MyMesh2OFF(MyMesh &msh, const string &offFile);
    static bool OFF2MyMesh(MyMesh &msh, ifstream &in);
    static bool OFF2MyMesh(MyMesh &msh, const string &offFile);
    static bool Labels2Parts(part_t &part, partpoints_t &points, ifstream &in);
    static bool Labels2Parts(part_t &part, partpoints_t &points, string &labelFile);
    //static bool Part2Poly(string label, Polyhedron &P);
};

void setIDsIfRequired(Polyhedron &P);
void setIDs(MyMesh &msh);

#endif /* defined(__HelloCGAL__CGALPolyhedron2OFF__) */
