//
//  DihedralAngles.h
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 03/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __HelloCGAL__DihedralAngles__
#define __HelloCGAL__DihedralAngles__

#include <iostream>
#include "common.h"
using namespace std;
using namespace CGAL;

#include <CGAL/Mesh_3/dihedral_angle_3.h>

void calcPairwiseDihedralAngles(MyMesh &msh);
void calcPairwiseDihedralAngles(MyMesh &msh, vector<pair<Idx,Idx> > &F,vector<double> &dAngle,double &minA, double &maxA, double &avgA, double &devA);
Point calcNormal(Facet_iterator F);
#if 0
double dihedralAngle(Point normal1, Point normal2);
double dihedralAngle(Facet_iterator F1, Facet_iterator F2);
#endif
double dihedralAngle(const Point &edge, const Point &normal1, const Point &normal2);
double dihedralAngle(Edge_iterator E, Facet_iterator F1, Facet_iterator F2);

Point cross(const Point &A, const Point &B);
double dot(const Point &A, const Point &B);


#endif /* defined(__HelloCGAL__DihedralAngles__) */
