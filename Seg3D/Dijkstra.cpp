//
//  Dijkstra.cpp
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 02/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//




// Note: Boost based algorithm was drooped as it was too complex
// so some the references used here are void

// References :
// 1. CGAL 4.3 Manual - CGAL and Boost Library
//    http://doc.cgal.org/latest/BGL/index.html
//
// CGAL is comaptible with Boost Graph Library
//
// 2. Boost Graph Library
// http://www.boost.org/doc/libs/1_54_0/libs/graph/doc/index.html
//
// 3. Lecture notes of Prof. J J Cao, Dalian University of Technology, China
// http://jjcao.weebly.com/algorithmdatastructure.html#/
//
// 4. Stuart James Journal : "Boost Dijkstra Shortest Path Example utilising backtracking from target"
// http://stuartjames.info/Journal/boost-dijkstra-shortest-path-example-utilising-backtracking-from-target.aspx
//
// 5. Programming Examples wiki
// http://programmingexamples.net/wiki/CPP/Boost/BGL/DijkstraDirected

#include "Dijkstra.h"

// Standard C++

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <utility>
#include "Graph.h"
#include "Dual.h"
#include "common.h"

// CGAL
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_items_with_id_3.h>



// Import namespaces

using namespace std;
using namespace CGAL;
using namespace boost;


#if 0

void conv2Adj(Polyhedron &mesh, AdjList &A, map<unsigned long, Point> &C)
{
    
    // Convert Polyhedron to AdjList
    
    A.isDirected = false;
    
    Vertex_iterator vertexBegin = mesh.vertices_begin();
    Vertex_iterator vertexEnd = mesh.vertices_end();
    
    unsigned long int i = 0;
    
    for (Vertex_iterator vitr = vertexBegin; vitr != vertexEnd ; ++vitr) {
        
        if (A.add_vertex(i)) {
            vitr->id() = i;
            C.insert(make_pair(i, vitr->point()));
            i++;
        }
    }
    
    i = 0;
    
    Facet_iterator facesBegin = mesh.facets_begin();
    Facet_iterator facesEnd = mesh.facets_end();
    for (Facet_iterator fitr  = facesBegin; fitr != facesEnd; ++fitr) {
        fitr->id() = i++;
    }
    
    for (Facet_iterator fitr  = facesBegin; fitr != facesEnd; ++fitr) {
        Halfedge_facet_circulator circ = fitr->facet_begin();
        do {
            
            unsigned long v1 = circ->vertex()->id();
            unsigned long v2 = circ->next()->vertex()->id();
            cout << "v1 "<< " " << v1 << " " << v2 << endl;
            
            // Add weights
            Point p1 = circ->vertex()->point();  // co-ordinates
            Point p2 = circ->next()->vertex()->point();
            
            Vector p3 = p1 - p2; // Subtraction retirns a Vector ! idiosyncracy !!
            double l = sqrt(p3*p3);   // Mathematically: p3' * p3
            
            A.add_edge(v1, v2, l);
            
        } while ( ++circ != fitr->facet_begin());
    }
    
    
    cout << A << endl;
}
#endif

void conv2Adj(Polyhedron &P, MapAdjList &A, map<Idx, Point> &C, EdgeFn efn, weight_t **M)
{
    //setIDsIfRequired(P);
    cout << "After:" << P.vertices_begin()->id() << endl;
    Polyhedron D = dual(P);
    //cout << "Dual computed for: " <<  P.vertices_begin()->id() << endl;
    
#if 0
    string dfile = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/output/dual_floydtest.off";
    CGALPolyhedron2OFF::poly2OFF(D, dfile);
#endif
    
    A.isDirected = false;
    int n = int(D.size_of_vertices());
    
    Vertex_iterator vertexBegin = D.vertices_begin();
    Vertex_iterator vertexEnd = D.vertices_end();
    //cout << "Dual:" << D.vertices_begin()->id() << endl;
    
    
    // Add vertices to graph data structure
    unsigned long int i = 0;
    for (Vertex_iterator vitr = vertexBegin; vitr != vertexEnd ; ++vitr) {
        
        if (A.add_vertex(i)) {
            C.insert(make_pair(i, vitr->point()));
            i++;
        }
    }
    
    // Initialize adjacency matrix
    for (int i = 0 ; i < n ; i++) {
        for (int j = 0 ; j < n ; j++) {
            M[i][j] = numeric_limits<weight_t>::max();
        }
    }
    
    for (int i = 0 ; i < n ; i++) M[i][i] = 0.0;
    
    // Add edges to adjacency list data structure
    
    for (Vertex_iterator vitr = vertexBegin; vitr != vertexEnd ; ++vitr) {
        Halfedge_handle h = vitr->halfedge();
        Halfedge_vertex_circulator v = h->vertex_begin();
        do {
            //cout << vitr->id() << " , " << v->opposite()->vertex()->id() <<endl;
            weight_t w = efn(*(v->opposite()->vertex()),*vitr);
            label_t x = vitr->id();
            label_t y = v->opposite()->vertex()->id();
            A.add_edge(x, y, w);
            M[x][y] = M[y][x] = w;
            
        } while (++v != h->vertex_begin());
    }
    
    cout << "End of conv2Adj" << endl;
}



void floyd(weight_t **M, weight_t **D, label_t **P, const int N)
{
    
#if 0
    // uncomment for testing
    cout << endl;
    for (int i = 0 ; i < N ; i++) {
        for (int j = 0 ; j < N ; j++) {
            cout << " " << M[i][j] ;
        }
        cout << endl;
    }
#endif

    
    // Initialization
    for (int i = 0 ; i < N ; i++) {
        for (int j = 0 ; j < N ; j++) {
            D[i][j] = M[i][j];
            P[i][j] = numeric_limits<label_t>::max();
        }
        D[i][i] = 0.0;
    }
    
    // Use Dynamic programming
    for (int k = 0 ; k < N ; k++) {
        for (int i = 0 ; i < N ; i++) {
            for (int j = 0 ; j < N ; j++) {
                if (D[i][k] + D[k][j] < D[i][j]) {
                    D[i][j] = D[i][k] + D[k][j];
                    P[i][j] = k;
                }
            }
        }
    }
    
#if 0
    // uncomment for testing
    cout << endl;
    for (int i = 0 ; i < N ; i++) {
        for (int j = 0 ; j < N ; j++) {
            cout << " " << D[i][j] ;
        }
        cout << endl;
    }
#endif
    
}



void dijkstra(MapAdjList &G, label_t source)
{
    // Initialization
    BOOST_FOREACH(dmap_t::value_type v, G.dist) G.dist[v.first] = numeric_limits<weight_t>::infinity();
    BOOST_FOREACH(colormap_t::value_type c, G.color) G.color[c.first] = C_WHITE;
    BOOST_FOREACH(imap_t::value_type p, G.parent) G.parent[p.first] = numeric_limits<label_t>::infinity();
    
    
    G.dist[source] = 0;
    G.color[source] = C_GRAY;
    
    priority_queue<pair<label_t, weight_t>, vector<pair<label_t, weight_t> >, ShortestDist > Q;
    Q.push(make_pair(source,G.dist[source]));
    
    while (!Q.empty()) {
        pair<label_t, weight_t> p;
        p = Q.top();
        label_t u = p.first;
        Q.pop();
        BOOST_FOREACH(elist_t::value_type e, G.adj[u]) {
            label_t v = e.first;
            weight_t w = e.second;
            if (G.color[v] == C_WHITE) {
                G.color[v] = C_GRAY;
                if (  G.dist[u] + w < G.dist[v] ) {
                    G.dist[v] = G.dist[u] + w;
                    G.parent[v] = u;
                    Q.push(make_pair(v,G.dist[v]));
                }
            }
        }
        G.color[u] = C_BLACK;
    }
    
    //cout << endl;
    //BOOST_FOREACH(dmap_t::value_type v, G.dist) cout << " " << v.second;
    //cout << endl;
}

#if 0

void calcAllDistances(Polyhedron &P, vector<Idx> &F,vector<double> &distance)
{
    
    
    MapAdjList A;
    map<unsigned long, Point> C;
    typedef map<unsigned long, Point>::iterator citr_t;
    weight_t **M; weight_t **dist;
    label_t **par;
    int n = int(P.size_of_facets()); // Note: we are interested in the dual mesh
    cout << "n = " << n << endl;
    
    
    M = malloc2D<weight_t>(n, n);
    dist = malloc2D<weight_t>(n, n);
    par = malloc2D<label_t>(n, n);
    
    //conv2Adj(P,A,C,Unweighted);
    conv2Adj(P,A,C,Euclidean,M);
    
    
    
//   Floyd's algorithm
    
//    cout << "Graph:" <<endl;
//    cout << "Vertex Co-ordinates:" << endl;
//    
//    for (citr_t itr = C.begin(); itr != C.end(); ++itr) {
//        cout << itr->first << " : " << itr->second << endl;
//    }
//    cout << endl;
    
    //cout << A << endl;
    
    
#if 0
    clock_t c1 = clock();
    time_t  t1 = time(NULL);
    floyd(M,dist,par,n);
    time_t  t2 = time(NULL);
    clock_t c2 = clock();
    
    double avgdist = 0.0;
    for (int i = 0 ; i < n ; i++) {
        for (int j = 0 ; j < n ; j++) {
            avgdist += dist[i][j];
        }
    }
    avgdist /= (n*n);
    
#if 0
    cout << endl;
    for (int i = 0 ; i < n ; i++) {
        cout << i << " : ";
        for (int j = 0 ; j < n ; j++) {
            cout << " " << setw(15) << dist[i][j];
        }
        cout << endl;
    }
#endif
    
    cout << "Clock: " << (c2 - c1) / CLOCKS_PER_SEC << endl;
    cout << "Time:  " << (t2 - t1) << endl;
    cout << "Average gedesic distance: " << avgdist << endl;
#endif
    
#if 1
    
    // Dijkstra's method
    
    double avgdist = 0.0;
    
    clock_t c1 = clock();
    time_t  t1 = time(NULL);
    for (int i = 0 ; i < n ; i++) {
        dijkstra(A, i);
        double sum = 0.0;
        for (int j = 0 ; j < n ; j++) {
            sum =  sum + A.dist[j];
        }
        avgdist += sum;
    }
    avgdist /= (n*n);
    time_t  t2 = time(NULL);
    clock_t c2 = clock();
    
    cout << "Clock: " << (c2 - c1) / CLOCKS_PER_SEC << endl;
    cout << "Time:  " << (t2 - t1) << endl;
    cout << "Average geodesic distance: " << avgdist << endl;
    cout << flush;
#endif
    
    Facet_iterator fBegin = P.facets_begin();
    Facet_iterator FEnd = P.facets_end();
    for (Facet_iterator fitr = fBegin ; fitr != FEnd ; ++fitr ) {
        F.push_back(fitr->id());
        distance.push_back(avgdist);
    }
    
    free2D<weight_t>(M, n);
    free2D<weight_t>(dist, n);
    free2D<label_t>(par,n);
    
}

#endif


#if 1
void calcAllDistances(MyMesh &msh, vector<Idx> &F,vector<double> &distance)
{
    
    Facet_iterator fBegin = msh.M.facets_begin();
    Facet_iterator FEnd = msh.M.facets_end();
    int i = 0;
    for (Facet_iterator fitr = fBegin ; fitr != FEnd ; ++fitr ) {
        F.push_back(fitr->id());
        // Iterate over face 1
        Halfedge_facet_circulator circ = fitr->facet_begin();
        Point centroid(0,0,0);
        int count = 0;
        do {
            //cout << circ->vertex()->id() << " ";
            //Point p1 = circ->vertex()->point();  // co-ordinates
        centroid = Point(centroid.x() + circ->vertex()->point().x(), centroid.y() + circ->vertex()->point().y(), centroid.z() + circ->vertex()->point().z());
            count++;
        } while ( ++circ != fitr->facet_begin());
        
        centroid = Point(centroid.x() / double(count) , centroid.y() / double(count), centroid.z() / double(count));
        
        double nrm = sqrt(centroid.x() * centroid.x() + centroid.y() * centroid.y() + centroid.z() * centroid.z());
        distance.push_back(nrm);
        msh.UMap[i].distFeature = nrm;
    }
}


#endif

