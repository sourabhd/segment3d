//
//  Graph.cpp
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 03/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include "Graph.h"

using namespace std;


bool MapAdjList::add_vertex(const label_t &v)
{
    if (adj.find(v) == adj.end()) {
        // Not Found
        elist_t empty;
        adj.insert(make_pair(v, empty));
        color.insert(make_pair(v, C_WHITE));
        dist.insert(make_pair(v, numeric_limits<weight_t>::infinity()));
        parent.insert(make_pair(v, numeric_limits<label_t>::infinity()));
        return true;
    }
    
    return false;
}

void MapAdjList::add_edge(const label_t &u, const label_t &v, const weight_t &w)
{
    if (adj.find(u) == adj.end()) {
        // Vertex u  not present;  add it
        add_vertex(u);
    }
    
    if (adj.find(v) == adj.end()) {
        // Vertex v  not present;  add it
        add_vertex(v);
    }
    
    if (adj[u].find(v) == adj[u].end()) {
        // edge (u,v) not found
        adj[u].insert(make_pair(v, w));
    }
    
    if (!isDirected) {
        if (adj[v].find(u) == adj[v].end()) {
            // edge (v,u) not found
            adj[v].insert(make_pair(u, w));
        }
    }
}


unsigned long MapAdjList::numVertices()
{
    return adj.size();
}

unsigned long MapAdjList::numEdges()
{
    unsigned long nEdges = 0;
    vitr_t vBegin = adj.begin();
    vitr_t vEnd = adj.end();
    for (vitr_t itr = vBegin; itr != vEnd; ++itr) {
        nEdges += ((itr->second).size());
    }
    
    return nEdges;
}



ostream& operator<<(ostream& out, MapAdjList &A)
{
    
    vitr_t vBegin = A.adj.begin();
    vitr_t vEnd = A.adj.end();
    for (vitr_t itr = vBegin; itr != vEnd; ++itr) {
        out << itr->first << " : ";
        
        
        eitr_t eBegin = (itr->second).begin();
        eitr_t eEnd = (itr->second).end();
        for (eitr_t itr2 = eBegin; itr2 != eEnd; ++itr2) {
            out << "(" << itr2->first << "," << itr2->second << ")" << " : ";
        }
        out << endl;
        
    }
    return out;
}


