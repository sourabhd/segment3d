#ifndef HMIN_CPP_WRAPPER_H
#define HMIN_CPP_WRAPPER_H
#include <engine.h>
using std::string;
const int hmin_dim = 8150;

class HminCppWrapper
{
	public:
	HminCppWrapper(const std::string &mfilepath, const::string &mfilepath2="" );
	~HminCppWrapper();
	static void destroy();
	void calcHMIN(const std::string &imgname, float hmax[]);
	void disp(float hmax[]);
	void extractHMAXFeatures(const std::string &posClassDir, const std::string &negClassDir,const int rank, const int size, const string &prefix);
	//void trainModel(const int p_size, const string &prefix);
	void trainModel(const int p_size, const string &prefix, const int tstartp, const int tendp, const int tstartn, const int tendn);
	void predictModel(const int p_size, const string &prefix, const int tstartp, const int tendp, const int tstartn, const int tendn, int &tp, int &fp, int &fn, int &N);
	void calcHMINROI(const string & iname, float hmax[], const int x1, const int y1, const int x2, const int y2);
	void* getVarFromMatFile(const string &matfile, const string &var);
	void hmaxMergeAllFeatureFiles(const int p_size, const string &prefix);

	// Path names for source; TODO: read from config file
	static const char hmin_dir1[];
	static const char hmin_dir2[];

	private:
	Engine *ep;	
};

#endif
