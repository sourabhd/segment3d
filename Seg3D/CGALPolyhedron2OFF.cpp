//
//  CGALPolyhedron2OFF.cpp
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 02/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

// Reference CGAL Manual : http://www.cgal.org/Manual/3.5/doc_html/cgal_manual/Polyhedron/Chapter_main.html#Subsection_23.3.6

#include "CGALPolyhedron2OFF.h"
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace CGAL;
using namespace boost;
using namespace boost::algorithm;

bool CGALPolyhedron2OFF::poly2OFF(Polyhedron &P, ofstream &out)
{
    // Write in OFF format
    // Header
    out << "OFF" << endl;
    
    // Number of vertices, faces,
    out  <<  P.size_of_vertices() << " " << P.size_of_facets()
         << " " << P.size_of_halfedges() / 2 << endl;
  
    
    
    // Copy the points (vertices)
    copy(P.points_begin(), P.points_end(), std::ostream_iterator<Point>(out, "\n"));
    
    // Faces
    for (Facet_iterator i = P.facets_begin(); i != P.facets_end() ; ++i) { // for each face
        
        Halfedge_facet_circulator j = i->facet_begin();
        // Assert that faces are atleast triangles
        CGAL_assertion(CGAL::circulator_size(j) >= 3);
        // Put the number of facets
        out << CGAL::circulator_size(j);
        
        // Now we need indices for facets
        
        do {
            out << " " << std::distance(P.vertices_begin(), j->vertex());
        } while (++j != i->facet_begin());  // While we are not back at the vertex
        out << endl;
    }
    
    return true;
}


bool CGALPolyhedron2OFF::poly2OFF(Polyhedron &P, const string &offFile)
{
    bool ret = false;
    ofstream out(offFile.c_str());
    if (!out) {
        cerr << "Could not write to " << offFile << endl;
        return false;
    }
    
    ret = CGALPolyhedron2OFF::poly2OFF(P,out);
    
    out.close();
    return ret;
}

bool CGALPolyhedron2OFF::OFF2Poly(Polyhedron &P, ifstream &in)
{
    in >> P;

    // Polyhedron requires IDs to set explicitly
    
    Vertex_iterator vertexBegin = P.vertices_begin();
    Vertex_iterator vertexEnd = P.vertices_end();
    
    unsigned long int i = 0;
    for (Vertex_iterator vitr = vertexBegin; vitr != vertexEnd ; ++vitr) {
        vitr->id() = i;
        i++;
    }
    
    i = 0;
    
    Facet_iterator facesBegin = P.facets_begin();
    Facet_iterator facesEnd = P.facets_end();
    
    for (Facet_iterator fitr  = facesBegin; fitr != facesEnd; ++fitr) {
        fitr->id() = i;
        i++;
    }
    
    return true;
}


bool CGALPolyhedron2OFF::OFF2Poly(Polyhedron &P, const string &offFile)
{
    bool ret = false;
#if 0
    cout << "Reading " << offFile << endl;
#endif
    ifstream in(offFile.c_str());
    if (!in) {
        cerr << "Could not read " << offFile << endl;
        return false;
    }
    
    ret = CGALPolyhedron2OFF::OFF2Poly(P,in);
    
    in.close();
    
    return ret;
}


bool CGALPolyhedron2OFF::Labels2Parts(part_t &part, partpoints_t &points, ifstream &in)
{
    while (!in.eof()) {
        string key = "", val = "";
        getline(in, key);
        if (in.eof()) break;
        trim(key);
        getline(in, val);
        if (in.eof()) break;
        trim(val);
        tokenizer<> tok(val);
        vector<unsigned long int> idx; idx.clear();
        for (tokenizer<>::iterator itr = tok.begin(); itr != tok.end() ; ++itr) {
            unsigned long int v = lexical_cast<unsigned long int>(*itr);
            //assert(!(v==0));
            idx.push_back(v-1);
            part.insert(make_pair(v-1, key));
        }
        //cout << key << " " << idx.size() << endl;
        points.insert(make_pair(key,idx));
    }
    //cout << part.size() << endl;
    return true;
}


bool CGALPolyhedron2OFF::MyMesh2OFF(MyMesh &msh, ofstream &out)
{
    // Write in OFF format
    // Header
    out << "OFF" << endl;
    
    // Number of vertices, faces,
    out  <<  msh.M.size_of_vertices() << " " << msh.M.size_of_facets()
    << " " << msh.M.size_of_halfedges() / 2 << endl;
    
    
    
    // Copy the points (vertices)
    copy(msh.M.points_begin(), msh.M.points_end(), std::ostream_iterator<Point>(out, "\n"));
    
    // Faces
    for (Facet_iterator i = msh.M.facets_begin(); i != msh.M.facets_end() ; ++i) { // for each face
        
        Halfedge_facet_circulator j = i->facet_begin();
        // Assert that faces are atleast triangles
        CGAL_assertion(CGAL::circulator_size(j) >= 3);
        // Put the number of facets
        out << CGAL::circulator_size(j);
        
        // Now we need indices for facets
        
        do {
            out << " " << std::distance(msh.M.vertices_begin(), j->vertex());
        } while (++j != i->facet_begin());  // While we are not back at the vertex
        out << " ";
        
        out << msh.UMap[i->id()].R << " " <<  msh.UMap[i->id()].G << " " <<  msh.UMap[i->id()].B << " " << msh.UMap[i->id()].A << endl;
    }
    
    return true;
}


bool CGALPolyhedron2OFF::MyMesh2OFF(MyMesh &msh, const string &offFile)
{
    bool ret = false;
    ofstream out(offFile.c_str());
    if (!out) {
        cerr << "Could not write to " << offFile << endl;
        return false;
    }
    
    ret = CGALPolyhedron2OFF::MyMesh2OFF(msh,out);
    
    out.close();
    return ret;
}

bool CGALPolyhedron2OFF::OFF2MyMesh(MyMesh &msh, ifstream &in)
{
    in >> msh.M;
    
    // Polyhedron requires IDs to set explicitly
    
    Vertex_iterator vertexBegin = msh.M.vertices_begin();
    Vertex_iterator vertexEnd = msh.M.vertices_end();
    
    unsigned long int i = 0;
    for (Vertex_iterator vitr = vertexBegin; vitr != vertexEnd ; ++vitr) {
        vitr->id() = i;
        i++;
    }
    
    i = 0;
    
    Facet_iterator facesBegin = msh.M.facets_begin();
    Facet_iterator facesEnd = msh.M.facets_end();
    
    for (Facet_iterator fitr  = facesBegin; fitr != facesEnd; ++fitr) {
        fitr->id() = i;
        i++;
    }
    
    return true;
}


bool CGALPolyhedron2OFF::OFF2MyMesh(MyMesh &msh, const string &offFile)
{
    bool ret = false;
#if 0
    cout << "Reading " << offFile << endl;
#endif
    ifstream in(offFile.c_str());
    if (!in) {
        cerr << "Could not read " << offFile << endl;
        return false;
    }
    
    ret = CGALPolyhedron2OFF::OFF2MyMesh(msh,in);
    
    in.close();
    
    return ret;
}



bool CGALPolyhedron2OFF::Labels2Parts(part_t &part, partpoints_t &points, string &labelFile)
{
    bool ret = false;
    ifstream in(labelFile.c_str());
    if (!in) {
        cerr << "Could not read " << labelFile << endl;
        return false;
        }
        
        ret = CGALPolyhedron2OFF::Labels2Parts(part, points, in);
        
        in.close();
        return ret;
}

void setIDsIfRequired(Polyhedron &P)
{
    Vertex_iterator v = P.vertices_begin();
    //cout << v->id() << endl;
    if (v->id() < numeric_limits<Idx>::max()) {
        return;
    }
    
    Vertex_iterator vBegin = P.vertices_begin();
    Vertex_iterator vEnd = P.vertices_end();
    int i = 0;
    for (Vertex_iterator vitr = vBegin; vitr != vEnd; ++vitr) {
        vitr->id() = i;
        i++;
    }
    
    Facet_iterator fBegin = P.facets_begin();
    Facet_iterator fEnd = P.facets_end();
    i = 0;
    for (Facet_iterator fitr = fBegin; fitr != fEnd; ++fitr) {
        fitr->id() = i++;
    }
    
}

void setIDs(MyMesh &msh)
{
    setIDsIfRequired(msh.M);

    Vertex_iterator vBegin = msh.M.vertices_begin();
    Vertex_iterator vEnd = msh.M.vertices_end();
    int i = 0;
    for (Vertex_iterator vitr = vBegin; vitr != vEnd; ++vitr) {
        msh.vertexID.insert(make_pair(i,vitr->id()));
        msh.vertexNum.insert(make_pair(vitr->id(),i));
        i++;
    }

    Facet_iterator fBegin = msh.M.facets_begin();
    Facet_iterator fEnd = msh.M.facets_end();
    i = 0;
    for (Facet_iterator fitr = fBegin; fitr != fEnd; ++fitr) {
        msh.faceID.insert(make_pair(i,fitr->id()));
        msh.faceNum.insert(make_pair(fitr->id(),i));
        i++;
    }
}


#if 0
static bool Part2Poly(string label, MyMesh &mesh, Polyhedron &P)
{
    VMap V;
    V.clear();
    Vertex_iterator vBegin = mesh.M.vertices_begin();
    Vertex_iterator vEnd = mesh.M.vertices_end();
    
    for (Vertex_iterator vitr = vBegin; vitr != vEnd; ++vitr) {
        V.insert(make_pair(vitr->id(),*vitr));
    }

    Facet_iterator fBegin = mesh.M.facets_begin();
    Facet_iterator fEnd = mesh.M.facets_end();
    for (Facet_iterator fitr = fBegin; fitr != fEnd; ++fitr) {
        if ( mesh.part[fitr->id()] == label ) {
            
    }
    
}
    
#endif
