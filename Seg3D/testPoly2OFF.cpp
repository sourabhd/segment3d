//
//  testPoly2OFF.cpp
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 02/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include "testPoly2OFF.h"

#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>

#include "CGALPolyhedron2OFF.h"
#include "Dijkstra.h"

#include <iostream>
#include <cstdlib>
#include <string>
#include "Graph.h"
#include "Dijkstra.h"

using namespace std;
using namespace CGAL;

//typedef CGAL::Simple_cartesian<double>    Kernel;
//typedef Kernel::Point_3                   Point;
//typedef CGAL::Polyhedron_3<Kernel>        Polyhedron;


void testPoly2OFF(void)
{
    
#if 1
    Polyhedron P;
    MyMesh M;
    
    string infile = "/Users/sourabhdaptardar/Datasets/MeshsegBenchmark-1.0/data/off/241.off";
    string outfile = OutputDir + filesep + "241_tmp.off";
    string outfile2 = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/files/tetrahedron.off";
    
    CGALPolyhedron2OFF::OFF2Poly(P, infile);
    CGALPolyhedron2OFF::poly2OFF(P,outfile);
    
    CGALPolyhedron2OFF::OFF2MyMesh(M, infile);
    CGALPolyhedron2OFF::MyMesh2OFF(M, outfile);
    
#endif
    
}