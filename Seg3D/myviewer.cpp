#include <iostream>
#include <cstdlib>
#include "meshviewer.h"

using namespace std;

int main(int argc, char *argv[])
{
    if (argc != 2) {
        cerr << "myviewer" << endl;
        cerr << "Usage:" << endl;
        cerr << "./myviewer <mesh_file_in_off_format>" << endl;
        exit(1);
    }

    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGBA);
    glutInitWindowSize(800,600);
    glutInitWindowPosition(100,100);
    glutCreateWindow("Hello Triangle");

    MeshViewerInitGlutCallbacks();

    MeshViewer *M = MeshViewer::getInstance();
    M->display(argv[1]);

    MeshViewerInit();
    MeshViewerMainLoop();

}
