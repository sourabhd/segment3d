//
//  ClassifierWrapper.cpp
//  Segment3D
//
//  Created by Sourabh Daptardar on 04/12/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <string>
#include <cstring>
#include <iomanip>
#include <sstream>
#include <mat.h>
#include <matrix.h>
extern "C" {
#include <unistd.h>
}

#include "ClassifierWrapper.h"

using namespace std;

ClassifierWrapper::ClassifierWrapper(const string &mfilepath, const string &mfilepath2):ep(0)
{
	ep = engOpen("\0");
	if (ep == 0) {
		CERR << "Failed to open matlab engine" << endl;
		exit(1);
	}
	string addpathCmd= "addpath('" + mfilepath + "')";
	engEvalString(ep, addpathCmd.c_str());
    
	if (mfilepath2 != "") {
		string addpathCmd2 = "addpath('" + mfilepath2 + "')";
		engEvalString(ep, addpathCmd2.c_str());
	}
}

ClassifierWrapper::~ClassifierWrapper()
{
	engClose(ep);
}


void* ClassifierWrapper::getVarFromMatFile(const string &mtfile, const string &var)
{
    
    
	void* ret = 0;
	MATFile *mfp = NULL;
	mfp = matOpen(mtfile.c_str(), "r");
	if (mfp == NULL) {
		CERR << "could not open " << mtfile.c_str() << endl;
		ret = 0;
		return ret;
	}
    
	mxArray *mxa = NULL;
	mxa = matGetVariable(mfp,var.c_str());
	if (mxa == NULL) {
		CERR << "failed to fetch variable " << var.c_str() << " from " << mtfile.c_str() << endl;
		ret = 0;
		goto out;
	}
    
	ret = mxGetData(mxa);
	if (ret == NULL) {
		CERR << "failed to fetch data for variable " << var.c_str() << " from " << mtfile.c_str() << endl;
		ret = 0;
		goto out;
	}
    
out:
	matClose(mfp);
	return ret;
}


void ClassifierWrapper::convToMatlab(const string &outdir, Idx meshid, bool isUnary, data_t &trdata, data_t &tedata, Meshes_t &meshes, const string &category, tr1::unordered_map<string,int> &CatNumLabels)
{
    //size_t numClasses = ((isUnary) ? distinctPartsId.size() : 2);
    //size_t numClasses = ((isUnary) ? meshes[category][0].partpts.size() : 2);
    //size_t numClasses = ((isUnary) ? 5 : 2);
    //size_t numClasses = ((isUnary) ? 2 : 2);
    size_t numClasses = ((isUnary) ? CatNumLabels[category] : 2);

    // Train data
    
    mwSize m = trdata.X.size();
    mwSize n = ((isUnary) ? dimUnary : dimPairwise);
    
    
    /* Copy feature vectors */
    double *Xraw = new double[m*n];
    for (int i = 0 ; i < int(m) ; i++) {
        for (int j = 0 ; j < int(n) ; j++) {
            Idx colMajorOff = j * m + i;
            Xraw[colMajorOff] = trdata.X[i][j];
        }
    }
    
    mxArray *X = mxCreateDoubleMatrix(m, n, mxREAL);
    
    memcpy( (void *) mxGetPr(X), (void *)Xraw, m * n * sizeof(double));
    
    
    delete [] Xraw;
    engPutVariable(ep, "features", X);
    
    /* Class labels */
    
    double *Craw = new double[m];

    for (int i = 0 ; i < int(m) ; i++) {
            Craw[i] = trdata.C[i];
    }
    
    mxArray *C = mxCreateDoubleMatrix(m,1,mxREAL);
    
    memcpy( (void *) mxGetPr(C), (void *)Craw, m * sizeof(double));

    
    delete [] Craw;

    engPutVariable(ep, "cl", C);
    

    // TEST data
    
    mwSize mTe = tedata.X.size();
    mwSize nTe = ((isUnary) ? dimUnary : dimPairwise);
    
    
    /* Copy feature vectors */
    double *XrawTe = new double[mTe*nTe];
    for (int i = 0 ; i < int(mTe) ; i++) {
        for (int j = 0 ; j < int(nTe) ; j++) {
            Idx colMajorOff = j * mTe + i;
            XrawTe[colMajorOff] = tedata.X[i][j];
        }
    }
    
    mxArray *XTe = mxCreateDoubleMatrix(mTe, nTe, mxREAL);
    
    memcpy( (void *) mxGetPr(XTe), (void *)XrawTe, mTe * nTe * sizeof(double));
    
    
    delete [] XrawTe;
    engPutVariable(ep, "featurestest", XTe);
    
    /* Class labels */
    
    double *CrawTe = new double[mTe];
    for (int i = 0 ; i < int(mTe) ; i++) {
        CrawTe[i] = tedata.C[i];
    }
    
    
    mxArray *CTe = mxCreateDoubleMatrix(mTe,1,mxREAL);
    memcpy( (void *) mxGetPr(CTe), (void *)CrawTe, mTe * sizeof(double));
    delete [] CrawTe;
    engPutVariable(ep, "vl", CTe);
    
    // Find the number of unique labels
        
    //cout << "Number of labels:" << meshes[category][0].partpts.size() << endl;
    
    double np = numClasses; // pairwise features are for binary classification - boundary or not
    double nwc = double(JointBoostNumWeakClassifiers);
    double nth = double(JointBoostNumThresholds);
    double nc = numClasses;
    
    mxArray *nParts = mxCreateDoubleScalar(np);
    mxArray *nClasses = mxCreateDoubleScalar(nc);
    mxArray *nWeakClassifiers = mxCreateDoubleScalar(nwc);
    mxArray *nThresholds = mxCreateDoubleScalar(nth);
    mxArray *mId = mxCreateDoubleScalar(double(meshid));
    mxArray *bUnary = mxCreateLogicalScalar(isUnary);
    mxArray *outDir = mxCreateString(outdir.c_str());
    
    engPutVariable(ep, "nparts", nParts);
    engPutVariable(ep, "NweakClassifiers", nWeakClassifiers);
    engPutVariable(ep, "Nthresholds",nThresholds);
    engPutVariable(ep, "nClasses",nClasses);
    engPutVariable(ep, "mId", mId);
    engPutVariable(ep, "bUnary", bUnary);
    engPutVariable(ep, "outDir", outDir);

}


void ClassifierWrapper::runMatlabCmd(const string &cmd)
{
    engEvalString(ep, cmd.c_str());
    out();
}


void ClassifierWrapper::put(const string &var, mxArray *val)
{
    engPutVariable(ep, var.c_str(), val);
}

void ClassifierWrapper::get(const string &var, void *cresult, size_t &elSize, size_t &nDim, size_t sizes[])
{
    mxArray *mresult;
    mresult = engGetVariable(ep,var.c_str());
    
    mwSize numDims;
    numDims = mxGetNumberOfDimensions(mresult);
    const mwSize *dims = mxGetDimensions(mresult);
    elSize = mxGetElementSize(mresult);
    nDim = numDims;
    
    // cout << "numDims:" << numDims;
    // for (int i = 0 ; i < numDims ; i++ ) cout <<  " " << dims[i];
    // cout << endl;
    
    int bytes = 1;
    for (int i = 0 ; i < int(numDims) ; i++ )  bytes *= dims[i];
    bytes *= elSize;
    
    memcpy(cresult,(void *)mxGetPr(mresult),bytes);
    
    for (int i = 0 ; i < int(numDims) ; i++ )  sizes[i] = dims[i];
    
}


void ClassifierWrapper::out()
{
   cout << "Matlab output stream: ";
   obuffer[MENG_OBUF_SIZE] = '\0';
   engOutputBuffer(ep, obuffer, MENG_OBUF_SIZE);
   cout << "->" << obuffer << "<-" << endl;
   cout << endl;    
}
