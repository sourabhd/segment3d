#ifndef CURVATURE_H
#define CURVATURE_H

#include "common.h"
using namespace std;

void curvature(Facet_iterator fitr, double &k1, double &k2);
typedef struct {
     double r,g,b;
} COLOUR;

COLOUR GetColour(double v,double vmin,double vmax);

#endif // CURVATURE_H
