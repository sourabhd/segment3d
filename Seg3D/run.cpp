//
//  run.cpp
//  Segment3D
//
//  Created by Sourabh Daptardar on 01/12/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include "run.h"
using namespace std;

bool doCalculations = false;

map<string,int> CategoryFileStartNum  = boost::assign::map_list_of
        ("Airplane",61)
        ("Ant",81)
        ("Armadillo",281)
        ("Bearing",341)
        ("Bird",251)
        ("Bust",301)
        ("Chair",101)
        ("Cup",21)
        ("Fish",221)
        ("FourLeg",381)
        ("Glasses",41)
        ("Hand",181)
        ("Human",1)
        ("Mech",321)
        ("Octopus",121)
        ("Plier",201)
        ("Table",141)
        ("Teddy",161)
        ("Vase",361);

map<string,int> CategoryFileEndNum  = boost::assign::map_list_of
        ("Airplane",65)
        ("Ant",85)
        ("Armadillo",285)
        ("Bearing",345)
        ("Bird",255)
        ("Bust",305)
        ("Chair",105)
        ("Cup",25)
        ("Fish",225)
        ("FourLeg",385)
        ("Glasses",45)
        ("Hand",185)
        ("Human",5)
        ("Mech",325)
        ("Octopus",125)
        ("Plier",205)
        ("Table",145)
        ("Teddy",165)
        ("Vase",365);

#if 0
//map<string,int> CategoryFileEndNum  = boost::assign::map_list_of
//        ("Airplane",80)
//        ("Ant",100)
//        ("Armadillo",300)
//        ("Bearing",360)
//        ("Bird",255)
//        ("Bust",320)
//        ("Chair",120)
//        ("Cup",40)
//        ("Fish",240)
//        ("FourLeg",400)
//        ("Glasses",60)
//        ("Hand",200)
//        ("Human",20)
//        ("Mech",340)
//        ("Octopus",140)
//        ("Plier",220)
//        ("Table",160)
//        ("Teddy",180)
//        ("Vase",380);
#endif

map<string,int> CategoryID = boost::assign::map_list_of
        ("Airplane",0)
        ("Ant",1)
        ("Armadillo",2)
        ("Bearing",3)
        ("Bird",4)
        ("Bust",5)
        ("Chair",6)
        ("Cup",7)
        ("Fish",8)
        ("FourLeg",9)
        ("Glasses",10)
        ("Hand",11)
        ("Human",12)
        ("Mech",13)
        ("Octopus",14)
        ("Plier",15)
        ("Table",16)
        ("Teddy",17)
        ("Vase",18);



void run(void)
{
    time_t t1 = time(NULL);
    clock_t c1 = clock();
    
    // Use category variable for
    // Single category quick test
    // For complete dataset use foreach
    //string category = "Bird";
    string category = "Octopus";
    //string category = "Cup";

    data_t trdata;
    Meshes_t Meshes;
    Meshes.clear();
    map<string,int> CatStartNum;
    CatStartNum.clear();
    tr1::unordered_map<string,int> CatNumLabels;
    CatNumLabels.clear();
    int c = 0;
    
    // Read data set
    Idx N = 0;
    //BOOST_FOREACH(string category, Categories) {
    cout << category << " : ";
    CatStartNum[category] = c;

    readDataset(category,CategoryFileStartNum[category], CategoryFileEndNum[category], Meshes,CatNumLabels);

    c += Meshes[category].size();
    N += Meshes[category].size();
    cout << CategoryFileStartNum[category] << " " << CategoryFileEndNum[category] << endl;

    //} //end of boost_foreach category
    
    
    //Idx numTrain = split_x * N / (split_x+split_y) ;
    //Idx numTest = split_y * N / (split_x+split_y) ;
    
    //BOOST_FOREACH(string category, Categories) {
    cout << category << " : ";
    //CatStartNum[category] = c;
    cout << CategoryFileStartNum[category] << " " << CategoryFileEndNum[category] << endl;
    //readDataset(category,CategoryFileStartNum[category], CategoryFileEndNum[category], Meshes);

    vector<int> exemplarIds;
    vector<int> validationIds;

    cout << "Training Set: " << endl;
    splitDataset(split_x, split_y, int(Meshes[category].size()), exemplarIds, validationIds);
    BOOST_FOREACH(int val, exemplarIds) cout << " " << val;
    cout << endl;
    cout << "Validation Set: " << endl;
    BOOST_FOREACH(int val, validationIds) cout << " " << val;
    cout << endl;

    // Set face and vertex IDs
    BOOST_FOREACH(int id, exemplarIds) setIDs(Meshes[category][id]);
    BOOST_FOREACH(int id, validationIds) setIDs(Meshes[category][id]);

    // Calculate features
    //cout << "M: " << Meshes[category][0].M.size_of_vertices() << endl;

    // Memoize distances for training and validation meshes

    int sz = exemplarIds.size() + validationIds.size();
    vector<int> allIds;
    allIds.clear();
    allIds.insert(allIds.end(), exemplarIds.begin(),exemplarIds.end());
    allIds.insert(allIds.end(), validationIds.begin(), validationIds.end());
    //int sz = exemplarIds.size();
    //omp_set_num_threads(2);
    //double t3 = omp_get_wtime();
    //#pragma omp parallel for schedule(dynamic,1)
    for (int i = 0 ; i < sz ; i++) {
        clock_t c5 = clock();
        time_t t5 = time(NULL);
        // BOOST_FOREACH(int id, exemplarIds) {
        //int id = exemplarIds[i];
        int id = allIds[i];
        int numFaces = Meshes[category][id].M.size_of_facets();
        if (doCalculations) {
            cout << "Calculating Distances for : " << id << " : ... ";
            Meshes[category][id].dist = malloc2D<double>(numFaces,numFaces);
            memset(&Meshes[category][id].dist[0][0],0,numFaces*numFaces*sizeof(double));
            calcAllPairsShortestPaths(Meshes[category][id],Euclidean,Meshes[category][id].dist);
            saveMyMesh(Meshes[category][id]);
        } else {
            cout << "Loading Distances for : " << id << " : " << Meshes[category][id].dump << endl;
            loadMyMesh(Meshes[category][id]);
        }

        cout << " done " << endl;

        // Calculate Average geodesic distance
        cout << "Calculating AGD ... ";
        double maxagd = -1, minagd = -1, avgagd = -1, devagd = -1;
        averageGeodesicDistance(Meshes[category][id],minagd,maxagd,avgagd,devagd);

        cout << "MESH # "  << id  << " : Number of unary features : "
             << Meshes[category][id].UMap.size() << endl;
        cout << "MAX AGD:" << category  << " : "<< setw(4) << id << " : " << maxagd << endl;
        cout << "MIN AGD:" << category  << " : "<< setw(4) << id << " : " << minagd << endl;
        cout << "AVG AGD:" << category  << " : "<< setw(4) << id << " : " << avgagd << endl;
        cout << "DEV AGD:" << category  << " : "<< setw(4) << id << " : " << devagd << endl;
        string fnameUnary = Meshes[category][id].off;
        string oname2Unary = fnameUnary.substr(fnameUnary.find_last_of(filesep)+1);
        string onameUnary = OutputDir + filesep + category + filesep + "agd_" + oname2Unary;

        cout << "Saving mesh "<< id << "for visualization ... " << flush;
        showUnaryFeatures(Meshes[category][id],onameUnary,minagd,maxagd);
        cout << " done " << endl;
        if (doCalculations) {
            free2D<double>(Meshes[category][id].dist,numFaces);
        } else {
            // memory mapped matrix
            munmap((void *)(Meshes[category][id].addr), sizeof(Idx) + Meshes[category][id].numFaces * Meshes[category][id].numFaces * sizeof(double));
            delete [] Meshes[category][id].dist;
            close(Meshes[category][id].fd);
        }
        clock_t c6 = clock();
        time_t t6 = time(NULL);
        cout << "Time : " << (t6 - t5) << "seconds" << " " << "Clock : " << (c6-c5) /CLOCKS_PER_SEC << " seconds" << endl;
        //double t4 = omp_get_wtime();
        //cout << "Time for iteration : " << i << " : " << (t4 - t3) << endl;
    }


    // //        BOOST_FOREACH(int id, validationIds) {
    // //            cout << "Distances for : " << id << " : ... ";
    // //            int numFaces = Meshes[category][id].M.size_of_facets();
    // //            Meshes[category][id].dist = malloc2D<double>(numFaces,numFaces);
    // //            calcAllPairsShortestPaths(Meshes[category][id],Euclidean,Meshes[category][id].dist);
    // //            cout << " done " << endl;
    // //        }

    // Unary
    cout << "----------------------------- UNARY ------------------------------------- " << endl;
    calcUnaryFeatures(Meshes, category, exemplarIds, CatStartNum, trdata);
    cout << "Number of unary features in training data : " << trdata.X.size() << endl;

    map<int,data_t> tedata;
    map<int, double*> prob;

    cout << "------------------------------ UNARY (TEST) ------------------------------- " << endl;
    BOOST_FOREACH(int val, validationIds) {
        vector<int> V; V.clear(); V.push_back(val);

        calcUnaryFeatures(Meshes, category, V, CatStartNum, tedata[val]);
        cout << "Number of unary features in test data " << val << ":" << tedata[val].X.size() << endl;


        size_t numTestFaces = tedata[val].X.size();
        size_t numClasses = Meshes[category][0].partpts.size();
        prob[val] = new double[numClasses*numTestFaces];

        stringstream sout;
        sout << OutputDir << filesep << category;

        learn(sout.str(), val, true, trdata,tedata[val],Meshes,category,prob[val],CatNumLabels); // unary


        //for (size_t i = 0 ; i < numTestFaces ; i++) {
        for (size_t j = 0 ; j < numTestFaces ; j++) {
            boost::numeric::ublas::vector<double> P(numClasses);
            double mx = -10000000;
            int mx_j = 0;
            //Idx faceid = boost::get<Idx>(tedata.feature[i]);
            //Idx faceid = tedata[val].unaryIds[j];
            //cout << faceid << " : ";
            //for (int j = 0 ; j < numClasses ; j++) {
            for (int i = 0 ; i < int(numClasses) ; i++) {
                //cout << prob[val][j*numTestFaces+i] << " ";
                //                    P[j] = prob[val][j*numTestFaces+i];
                //                    if (P[j] > mx) {
                //                        mx_j = j;
                //                        mx = P[j];
                //                    }
                P[i] = prob[val][j*numClasses+i];
                if (P[i] > mx) {
                    mx_j = i;
                    mx = P[i];
                }
            }
            Meshes[category][val].UMap[j].probFeatureVector = P;
            Meshes[category][val].UMap[j].maxProb = mx;
            Meshes[category][val].UMap[j].argmaxProb = mx_j;
            //cout << endl;
        }

    }


    // Pairwise
    
    cout << "----------------------------- PAIRWISE ------------------------------------- " << endl;
    data_t trdataPairwise;

    calcPairwiseFeatures(Meshes, category, exemplarIds, CatStartNum, trdataPairwise);
    cout << "Number of pairwise features in training data : " << trdataPairwise.X.size() << endl;

    

    map<int,data_t> tedataPairwise;
    map<int, double*> probPairwise;

    BOOST_FOREACH(int val, validationIds) {
        cout << "------------------------------ PAIRWISE (TEST) ------------------------------- " << endl;
        vector<int> V; V.clear(); V.push_back(val);
        calcPairwiseFeatures(Meshes, category, V, CatStartNum, tedataPairwise[val] );
        cout << "Number of pairwise features in test data "<< val << " : " << tedataPairwise[val].X.size() << endl;


        size_t numTestFacesPairwise = tedataPairwise[val].X.size();
        size_t numClassesPairwise = 2;
        probPairwise[val] = new double[numClassesPairwise*numTestFacesPairwise];

        stringstream sout;
        sout << OutputDir << filesep << category;

        learn(sout.str(), val, false, trdataPairwise,tedataPairwise[val],Meshes,category,probPairwise[val],CatNumLabels); // binary

        int cnt0 = 0;
        int cnt1 = 0;
        //for (size_t i = 0 ; i < numTestFacesPairwise ; i++) {
        for (size_t j = 0 ; j < numTestFacesPairwise ; j++) {
            //cout << tedataPairwise[val].pairWiseIds[i].first << "," << tedataPairwise[val].pairWiseIds[i].second  << " ";

            //Idx u = tedataPairwise[val].pairWiseIds[i].first;
            //Idx v = tedataPairwise[val].pairWiseIds[i].second;
            Idx u = tedataPairwise[val].pairWiseIds[j].first;
            Idx v = tedataPairwise[val].pairWiseIds[j].second;

            boost::numeric::ublas::vector<double> P(2);
            double mx = -10000000;
            int mx_j = -1;
            //for (int j = 0 ; j < numClassesPairwise ; j++) {
            for (int i = 0 ; i < int(numClassesPairwise) ; i++) {
                //cout << probPairwise[val][j*numTestFacesPairwise+i] << " ";
                //P[j] = probPairwise[val][j*numTestFacesPairwise+i];
                //if (P[j] > mx) {
                //    mx_j = j;
                //    mx = P[j];
                //}
                //cout << probPairwise[val][j*numTestFacesPairwise+i] << " ";
                P[i] = probPairwise[val][j*numClassesPairwise+i];
                if (P[i] > mx) {
                    mx_j = i;
                    mx = P[i];
                }
            }
            //cout << endl;
            Meshes[category][val].PMap[make_pair(u,v)].probFeatureVector = P;
            Meshes[category][val].PMap[make_pair(u,v)].maxProb = mx;
            Meshes[category][val].PMap[make_pair(u,v)].argmaxProb = mx_j;
            if (mx_j  == 0) cnt0++;
            if (mx_j  == 1) cnt1++;
        }
        cout << "Count of zero: " << cnt0 << endl;
        cout << "Count of one: " << cnt1 << endl;
    }


#if 0
    BOOST_FOREACH(int val, validationIds) {
        // Segment the meshes
        //graphCut(tedata,prob,tedataPairwise[val],probPairwise[val]);
    }
#endif
    
#if 1
    BOOST_FOREACH(int val, validationIds) {
        string fname = Meshes[category][val].off;
        // cout << fname << endl;
        string oname2 = fname.substr(fname.find_last_of(filesep)+1);
        string onameUnary = OutputDir + filesep + category + filesep + "unary_" + oname2;
        string onamePairwise = OutputDir + filesep + category + filesep + "pairwise_" + oname2;

        cout << "Writing Mesh Output (Unary) : " << onameUnary.c_str() << endl;
        
        Facet_iterator fBegin = Meshes[category][val].M.facets_begin();
        Facet_iterator fEnd = Meshes[category][val].M.facets_end();
        for (Facet_iterator fitr = fBegin ; fitr != fEnd ; ++fitr) {
            int lbl = Meshes[category][val].UMap[fitr->id()].argmaxProb;
            //cout << fitr->id() << " " << lbl << endl;
            if (lbl < 10) {
                Meshes[category][val].UMap[fitr->id()].R = colorR[lbl];
                Meshes[category][val].UMap[fitr->id()].G = colorG[lbl];
                Meshes[category][val].UMap[fitr->id()].B = colorB[lbl];
                Meshes[category][val].UMap[fitr->id()].A = 1;
            } else {
                Meshes[category][val].UMap[fitr->id()].R = 0;
                Meshes[category][val].UMap[fitr->id()].G = 0;
                Meshes[category][val].UMap[fitr->id()].B = 0;
                Meshes[category][val].UMap[fitr->id()].A = 1;
            }
        }
        CGALPolyhedron2OFF::MyMesh2OFF(Meshes[category][val],onameUnary);


        cout << "Writing Mesh Output (Pairwise) : " << onamePairwise.c_str() << endl;
        Edge_iterator eBegin = Meshes[category][val].M.edges_begin();
        Edge_iterator eEnd = Meshes[category][val].M.edges_end();

        for (Edge_iterator itr = eBegin; itr != eEnd; ++itr) { // for each edge
            // get two adjacent faces
            Idx fid1 = itr->face()->id();
            Idx fid2 = itr->opposite()->face()->id();
            pair<Idx,Idx> pr = make_pair(fid1,fid2);
            int lbl = Meshes[category][val].PMap[make_pair(fid1,fid2)].argmaxProb;
            //cout << fid1 << "," << fid2 << " : " << lbl << endl;
            if (lbl < 10) {
                Meshes[category][val].UMap[fid1].R = colorR[lbl];
                Meshes[category][val].UMap[fid1].G = colorG[lbl];
                Meshes[category][val].UMap[fid1].B = colorB[lbl];
                Meshes[category][val].UMap[fid1].A = 1;
                Meshes[category][val].UMap[fid2].R = colorR[lbl];
                Meshes[category][val].UMap[fid2].G = colorG[lbl];
                Meshes[category][val].UMap[fid2].B = colorB[lbl];
                Meshes[category][val].UMap[fid2].A = 1;
            } else {
                Meshes[category][val].UMap[fid1].R = 0;
                Meshes[category][val].UMap[fid1].G = 0;
                Meshes[category][val].UMap[fid1].B = 0;
                Meshes[category][val].UMap[fid1].A = 1;
                Meshes[category][val].UMap[fid2].R = 0;
                Meshes[category][val].UMap[fid2].G = 0;
                Meshes[category][val].UMap[fid2].B = 0;
                Meshes[category][val].UMap[fid2].A = 1;
            }


        }

        CGALPolyhedron2OFF::MyMesh2OFF(Meshes[category][val],onamePairwise);
    }
#endif
    
    BOOST_FOREACH(int val, validationIds) {
        delete [] prob[val];
        delete [] probPairwise[val];
    }

    //}


    BOOST_FOREACH(int id, exemplarIds) {
        int numFaces = Meshes[category][id].M.size_of_facets();
        free2D<double>(Meshes[category][id].dist, numFaces);
    }

    BOOST_FOREACH(int id, validationIds) {
        int numFaces = Meshes[category][id].M.size_of_facets();
        free2D<double>(Meshes[category][id].dist, numFaces);
    }

    clock_t c2 = clock();
    time_t t2 = time(NULL);
    cout << "Time : " << (t2 - t1) << "seconds" << " " << "Clock : " << (c2-c1) /CLOCKS_PER_SEC << " seconds" << endl;
}
