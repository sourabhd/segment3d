#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <string>
#include <cstring>
#include <iomanip>
#include <sstream>
#include <mat.h>
#include <matrix.h>
extern "C" {
#include <unistd.h>
}
#include "HminCppWrapper.h"

using namespace std;

#define CPP_WRAP_DEBUG
#if defined(CPP_WRAP_DEBUG)
#define CERR std::cerr << __FILE__ << ":" << __LINE__ << ":" << __func__ << " :: " 
#define COUT std::cout << __func__ << ":" << __LINE__ << ":" << __func__ << " :: "
#else
#define CERR std::cerr
#define COUT std::cout
#endif

const char HminCppWrapper::hmin_dir1[] = "/nfs/bigeye/sdaptardar/visualsearch/earlier_code/S/eyemodel/code/hmin/hmin";
const char HminCppWrapper::hmin_dir2[] = "/nfs/bigeye/sdaptardar/visualsearch/earlier_code/S/eyemodel/code/hmin/hmin_test";

HminCppWrapper::HminCppWrapper(const string &mfilepath, const string &mfilepath2):ep(0)
{
	ep = engOpen("\0");
	if (ep == 0) {
		CERR << "Failed to open matlab engine" << endl; 
		exit(1);
	}
	string addpathCmd= "addpath('" + mfilepath + "')";
	engEvalString(ep, addpathCmd.c_str());

	if (mfilepath2 != "") {
		string addpathCmd2 = "addpath('" + mfilepath2 + "')";
		engEvalString(ep, addpathCmd2.c_str());
	}
}

HminCppWrapper::~HminCppWrapper()
{
	engClose(ep);
}


void HminCppWrapper::calcHMINROI(const string & iname, float hmax[], const int x1, const int y1, const int x2, const int y2)
{
	// Extract the object and create image file for it
	size_t p = iname.find_last_of('/');
	cout << p << endl;
	string pname = iname.substr(0,p+1);
	string fname = iname.substr(p+1);
	cout << pname << endl;
	cout << fname << endl;

	ostringstream ofilename;
	ofilename << pname << "obj" << "__" << x1 << "_" << x2 << "_" << y1 << "_" << "y2" << "__" << fname;
	ostringstream hminCmd;
	hminCmd << "I = imread('" <<  iname << "'); I2 = I(" << y1 << ":" << y2 << "," << x1 << ":" << x2 << ",:" << "); imwrite(I2,'" << ofilename.str().c_str() << "');";
	cout << hminCmd.str().c_str() << endl << flush;
	//hminCmd << "I = imread('" <<  iname << "'); imshow(I);";
	engEvalString(ep,hminCmd.str().c_str());

#if 0
	// Now, run hmin code on the file to extract hmax features
	calcHMIN(ofilename.str(),hmax);
#endif

}

void HminCppWrapper::calcHMIN(const string &iname, float hmax[])
{
	mxArray *hmin = NULL; 

	string hminCmd = "V = example_run('" + iname + "')";
	engEvalString(ep, hminCmd.c_str());
	if ((hmin = engGetVariable(ep,"V")) == NULL) {
		CERR << "Could not calculate hmax feature vector" << endl;
	} else {
		memset(hmax,0,hmin_dim*sizeof(float));
		memcpy(hmax,mxGetPr(hmin),hmin_dim*sizeof(float));
	} 	

	mxDestroyArray(hmin);
}

void HminCppWrapper::extractHMAXFeatures(const string &posClassDir, const string &negClassDir, const int p_rank, const int p_size, const string &prefix)
{
	 ostringstream hminFeatureCalcCmd;
	 hminFeatureCalcCmd << "hmax_create_dataset('" << posClassDir << "','" << negClassDir << "'," << p_rank << "," << p_size << ",'" << prefix << "')";
	 engEvalString(ep, hminFeatureCalcCmd.str().c_str());
}

//void HminCppWrapper::trainModel(const int p_size, const string &prefix)
void HminCppWrapper::trainModel(const int p_size, const string &prefix, const int tstartp, const int tendp, const int tstartn, const int tendn)
{
	ostringstream hminCmd;
	hminCmd << "hmax_build_model(" << p_size << ",'" << prefix << "'," << tstartp << "," << tendp << "," << tstartn << "," << tendn << ")";
	engEvalString(ep, hminCmd.str().c_str());
}

void HminCppWrapper::predictModel(const int p_size, const string &prefix, const int tstartp, const int tendp, const int tstartn, const int tendn ,int &tp, int &fp, int &fn, int &N)
{
	mxArray *var = NULL;
	double val = 0;

	ostringstream hminCmd;
	hminCmd << "[TP,FP,FN,N] = hmax_run_svm(" << p_size << ",'" << prefix <<  "'," << tstartp << "," << tendp << "," << tstartn << "," << tendn << " );";
	engEvalString(ep, hminCmd.str().c_str());

	if ((var = engGetVariable(ep,"TP")) == NULL) {
		CERR << "Could not calculate TP" << endl;
	} else {
		val = 0;
		memcpy(&val,mxGetPr(var),sizeof(double));
		tp = int(val+0.5);
	} 	

	if ((var = engGetVariable(ep,"FP")) == NULL) {
		CERR << "Could not calculate FP" << endl;
	} else {
		val = 0;
		memcpy(&val,mxGetPr(var),sizeof(double));
		fp = int(val+0.5);
	} 

	if ((var = engGetVariable(ep,"FN")) == NULL) {
		CERR << "Could not calculate FN" << endl;
	} else {
		val = 0;
		memcpy(&val,mxGetPr(var),sizeof(double));
		fn = int(val+0.5);
	} 	

	if ((var = engGetVariable(ep,"N")) == NULL) {
		CERR << "Could not calculate N" << endl;
	} else {
		val = 0;
		memcpy(&val,mxGetPr(var),sizeof(double));
		N = int(val+0.5);
	} 	

	mxDestroyArray(var);
}
void HminCppWrapper::disp(float hmax[])
{
	for (int i = 0 ; i < hmin_dim ; i++ ) {
		if (i % 10 == 0) cout << "\n";
		cout << hmax[i] << "\t";
	}
	cout << endl;
	cout << flush;
}


void* HminCppWrapper::getVarFromMatFile(const string &mtfile, const string &var)
{
	void* ret = 0;
	MATFile *mfp = NULL;
	mfp = matOpen(mtfile.c_str(), "r");
	if (mfp == NULL) {
		CERR << "could not open " << mtfile.c_str() << endl;
		ret = 0;
		return ret;
	}

	mxArray *mxa = NULL;
	mxa = matGetVariable(mfp,var.c_str());
	if (mxa == NULL) {
		CERR << "failed to fetch variable " << var.c_str() << " from " << mtfile.c_str() << endl;
		ret = 0;
		goto out;
	}

	ret = mxGetData(mxa);
	if (ret == NULL) {
		CERR << "failed to fetch data for variable " << var.c_str() << " from " << mtfile.c_str() << endl;
		ret = 0;
		goto out;
	}

out:
	matClose(mfp);
	return ret;
}

void HminCppWrapper::hmaxMergeAllFeatureFiles(const int p_size, const string &prefix)
{
	ostringstream cmd;
	cmd << "hmax_merge_featurevectors(" << p_size  << ",'" << prefix << "')";
	engEvalString(ep, cmd.str().c_str());
}


#if 0
int main(int argc, char *argv[])
{
	string cwd(get_current_dir_name()); 	
	string mfilepath = cwd + "/../hmin";
	float hmax[hmin_dim];
	string iname = cwd + "./../datasets/coil-20-proc/obj10__0.png";
	string iname2 = cwd + "./../datasets/coil-20-proc/obj10__1.png";
	HminCppWrapper hmodel(mfilepath);
	hmodel.calcHMIN(iname, hmax);
	hmodel.disp(hmax);
	hmodel.calcHMIN(iname2, hmax);
	hmodel.disp(hmax);

	return 0;
}
#endif
