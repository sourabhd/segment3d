//
//  utils.h
//  Segment3D
//
//  Created by Sourabh Daptardar on 30/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __Segment3D__projutils__
#define __Segment3D__projutils__

#include <iostream>
#include "common.h"

using namespace std;

template<class T>
T** malloc2D(const int M, const int N)
{
	T** A;
	A = new T*[M];
	A[0] = new T[M*N];
	for(int i = 1 ; i < M ; i++) {
		A[i] = A[0] + i * N;
	}
	return A;
}

template<class T>
void free2D(T** A, const int M)
{
	delete [] A[0];
}

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

#endif /* defined(__Segment3D__utils__) */
