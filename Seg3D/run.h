//
//  run.h
//  Segment3D
//
//  Created by Sourabh Daptardar on 01/12/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __Segment3D__run__
#define __Segment3D__run__

#include <iostream>
#include "common.h"
#include "ReadDataset.h"
#include "training.h"
#include "feature.h"
#include "gco-v3.0/applyGraphCut.h"

using namespace std;

void run(void);

#endif /* defined(__Segment3D__run__) */
