//
//  testDihedral.h
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 03/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __HelloCGAL__testDihedral__
#define __HelloCGAL__testDihedral__

#include <iostream>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>

#include "common.h"
#include "CGALPolyhedron2OFF.h"
#include "Dijkstra.h"

#include <iostream>
#include <cstdlib>
#include <string>
#include "Graph.h"
#include "Dijkstra.h"
#include "DihedralAngles.h"
#include "feature.h"


using namespace std;
using namespace CGAL;

void testDihedral(void);

#endif /* defined(__HelloCGAL__testDihedral__) */
