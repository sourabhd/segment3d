/********************************************************************** 
Computes the parameters of the stump for one class (leaves) so that
you can propagate to other nodes.

function [Ac , Bc, Wp, Wn] = getLeaveStumpC(x, z, w, thresholds); 
It assumes that z = [-1 1]

Ac(f,t) = sum( w * z * d(x(f)>th(t))))
Bc(f,t) = sum( w * z * d(x(f)<=th(t))))

where d(x) is the indicator function.

In a node:
            Wnjn = sum(Wn(:,:,jn),3);
            Wpjn = sum(Wp(:,:,jn),3);
            bS = sum(Bc(:,:,jn),3) ./ Wnjn;
            aS = sum(Ac(:,:,jn),3) ./ Wpjn;
            
            errorS = Wpjn.* (1-aS.^2) + Wnjn .* (1-bS.^2);

...

It returns a regression stump so that:

h(x) = ac+bc   x>th
h(x) = bc     x<=th

as it is now, it not assumes that sum(w) = 1;

error = sum( w * (z - h(x))^2 )
***********************************************************************/



#include <stdio.h>
#include "mex.h"


/* M(i,j) = M(i + nrows*j) since Matlab uses Fortran layout. */

#define INMAT(i,j) M[(i)+nrows*(j)]
#define OUTMAT(i,j) out[(i)+nrows*(j)]

#define MAX(i,j) ((i)>(j) ? (i) : (j))

void mexFunction(
                 int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]
				 )
{
	double *Ac, *Bc, *Wn, *Wp, *x, *z, *w, *thresholds;
	int Nth, Ns, s, thi;
	double wi, thresh;
	double bc, ac, wneg, wpos, Sw, Swz;
	double *ws, *zs, *xs;

	/* read the input args */

	x = mxGetPr(prhs[0]);
	Ns = MAX(mxGetM(prhs[0]), mxGetN(prhs[0]));
	z = mxGetPr(prhs[1]);
	w = mxGetPr(prhs[2]);
	thresholds = mxGetPr(prhs[3]);
	Nth = MAX(mxGetM(prhs[3]), mxGetN(prhs[3]));
	
	/* alloc space for return args */

	plhs[0] = mxCreateDoubleMatrix(1, Nth, mxREAL);
	Ac = mxGetPr(plhs[0]); 
	plhs[1] = mxCreateDoubleMatrix(1, Nth, mxREAL);
	Bc = mxGetPr(plhs[1]); 
	plhs[2] = mxCreateDoubleMatrix(1, Nth, mxREAL);
	Wp = mxGetPr(plhs[2]); 
	plhs[3] = mxCreateDoubleMatrix(1, Nth, mxREAL);
	Wn = mxGetPr(plhs[3]); 
	
	/* main */
	
	/* First compute total sums of weights and weighted labels */
        Sw = 0; Swz  = 0;
        ws = w; zs = z;
	for (s = 0; s < Ns; s++) {
		wi  = *ws; 
		Sw += wi;
		Swz += wi * (*zs); 
		zs++; ws++;
	}

	/* Here compute regression parameters for each threshold */
	for (thi = 0; thi < Nth; thi++) {
	  thresh = thresholds[thi];
		/*bc = 0; wneg = 0; */
		ac = 0; wpos = 0;
		xs = x; ws = w; zs = z;
		for (s = 0; s < Ns; s++) {
			if ((*xs) > thresh) {
				wi  = *ws;
				ac += wi * (*zs);
				wpos += wi;
			}
			xs++;
			ws++;
			zs++;
		}
		Ac [thi] = ac;
		Bc [thi] = Swz - ac;
		Wp [thi] = wpos;
		Wn [thi] = Sw - wpos;
	}
} 



