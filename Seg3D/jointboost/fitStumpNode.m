function [a, b, th, featNdx, bestnode, b0, tmp] = fitStumpNode(x, c, w, thresholds, T, tmp)
%
% All the heavy computations are only done on the leaves of the graph.
% Then, the parameters are propagated to the other nodes.  
% The thresholds are discretized (they can be different for each feature).

[Nfeatures, Nsamples] = size(x);
[Nclasses Nnodes] = size(T);
Nthresholds = size(thresholds, 2);
b0 = zeros(1, Nclasses);
error0 = zeros(1, Nclasses);

% First we compute the best parameters Ac, Bc, Wp, Wn for regression stumps for each
% independent class and for a set of predefined thresholds
if nargin < 6
    tmp = [];
end

if ~isempty(tmp)
    % reuse previous stump parameters. Only update the classes that were
    % updated in the previous round.
    Ac = tmp.Ac;
    Bc = tmp.Bc;
    Wp = tmp.Wp;
    Wn = tmp.Wn;
    b0 = tmp.b0;
    error0 = tmp.error0;
    update = tmp.update; % only update the classes that have changed
    clear tmp
else
    % in the first round, the parameters for each class have to be
    % computed. 
    Ac = zeros([Nfeatures Nthresholds Nclasses]);
    Bc = zeros([Nfeatures Nthresholds Nclasses]);
    Wp = zeros([Nfeatures Nthresholds Nclasses]);
    Wn = zeros([Nfeatures Nthresholds Nclasses]);
    update = 1:Nclasses;
end

disp(sprintf('Update stumps for %d classes out of %d', length(update), Nclasses));
disp('  Loop on leaves...')
disp('                      ')
for jn = update
    disp(sprintf('\b\b\b\b\b\b\b\b\b\b\b\b classe: %2.0f', jn))
   
    % class label for class jn: -1=background, 1=data in classe jn
    yJ = 2*double(c==jn)-1;
    wJ = w{jn};
    %we cannot normalize the weights per class: This is wrong => wJ = wJ / sum(wJ);
    
    % For each leave we fit two models: 1) a constant, 2) a stump
    b0(jn) = sum(yJ .* wJ)/ sum(wJ); 
    error0(jn) = sum(wJ.*(yJ - b0(jn)).^2);
    
    % stump
    for n = 1:Nfeatures
        xx  = double(x(n,:)); 
        thn = double(thresholds(n,:));
        
        [Ac(n,:,jn), Bc(n,:,jn), Wp(n,:,jn), Wn(n,:,jn)] = getLeaveStumpC(xx, yJ, wJ, thn);
        drawnow
    end
end
j = find(Wp == 0); Wp (j) = 1; % to avoid divided by zero
j = find(Wn == 0); Wn (j) = 1; % to avoid divided by zero


% Now, search for best node.
% First evaluate error at each node (for each node look for best feature
% and best threshold to be shared by all the leaves)
for node = 1:Nnodes
    S = find(T(:,node));
    Snot = setdiff(1:Nclasses, S);

    Wns = sum(Wn(:,:,S), 3);    
    Wps = sum(Wp(:,:,S), 3);
    bS = sum(Bc(:,:,S), 3) ./ Wns;
    aS = sum(Ac(:,:,S), 3) ./ Wps;

    SE = Wps.* (1-aS.^2) + Wns .* (1-bS.^2) + sum(error0(Snot));
    
    % for this node, give best feature and best threshold
    [merr, ndx] = min(SE,[],1);
    [foo, thNdx] = min(merr);
    featNdx = ndx(thNdx);
    
    a(node)  = aS(featNdx, thNdx);
    b(node)  = bS(featNdx, thNdx);
    a(node)  = a(node) - b(node);
    th(node) = thresholds(featNdx, thNdx);
    minSE(node) = SE(featNdx, thNdx);
    f(node) = featNdx;
end

% Take best node and return best weak feature parameters and best feature
% index
[minSE, bestnode] = min(minSE);
a = a(bestnode);
b = b(bestnode);
th = th(bestnode);
featNdx = f(bestnode);

% return intermediate values for efficiency. Most of them can be reused in
% the next iteration.
tmp.Ac = Ac;
tmp.Bc = Bc;
tmp.Wp = Wp;
tmp.Wn = Wn;
tmp.b0 = b0;
tmp.error0 = error0;
tmp.update = find(T(:,bestnode))';


