function [Cx, Fx, Fn, Fstumps] = strongJointClassifier(x, classifier, T)
% Applies multiclass classifier with function graph.
%
% Given the input samples 'x', the classifier parameters and the graph
% description T, computes the classifier for each input sample.
%
% Fx is the interesting output. Applying different thresholds to Fx we can
% move across the ROC of the classifier.
%
% Cx is just Fx with the threshold set to 0. Cx = Fx>0 but this might not
% be the best thing to do, so better work with Fx directly and decide
% later. Also, from Fx we can get probabilities.
%
% Fn is the contribution of each node to the final classifier


[Nclasses, Nnodes] = size(T);
[Nfeatures, Nsamples] = size(x); % Nsamples = Number of thresholds that we will consider

nodes = [classifier(:).bestnode];

% 1) compute each node-function
Fn = zeros(Nsamples, Nnodes); % F(n) is the function in node 'n'. 

if nargout == 4
    Nstumps = length(classifier);
    Fstumps = zeros(Nsamples, Nstumps);
    stump = 0;
end

%Each function in Fn is computed by adding the corresponding boosting rounds.
for n = 1:Nnodes
    stumpsInNode = find(nodes == n); % this gives the set of all stumps that correspond to F(n) 
    for m = stumpsInNode
        featureNdx = classifier(m).featureNdx;
        th = classifier(m).th;
        a = classifier(m).a;
        b = classifier(m).b;
        
        fm = (a * double(x(featureNdx,:)>th) + b); % regression stump
        Fn(:, n) = Fn(:, n) + fm(:); 
        
        if nargout == 4
            stump = stump + 1;
            Fstumps(:, stump) = fm';
        end
    end
end

% 2) compute the final classifiers from the node-functions by applying the
% necessary combinations in order to get the classifiers corresponding at
% each class.
Fx = zeros(Nsamples, Nclasses);
for i = 1:Nclasses
    j = find(T(i,:)); % nodes conected to each classifier.
    Fx(:,i) = sum(Fn(:,j),2) + classifier(1).b0(i);
end

Cx = Fx>0;

