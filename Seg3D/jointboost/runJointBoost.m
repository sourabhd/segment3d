function [Cx, Fx, Fn, Fstumps, prob] = runJointBoost(outDir, mId, bUnary, features, featurestest, cl, vl, nClasses, NweakClassifiers, Nthresholds )

inpdatafile = sprintf('%s%sinput_%d_%d.mat', outDir, filesep, mId, bUnary);
save(inpdatafile, 'mId', 'bUnary', 'features', 'featurestest', 'cl', 'vl', 'nClasses', 'NweakClassifiers', 'Nthresholds');

fprintf(2,'features:         %g %g\n', size(features,1), size(features,2));
fprintf(2,'featurestest:     %g %g\n', size(featurestest,1), size(featurestest,2));
fprintf(2,'cl:               %g %g\n', size(cl,1), size(cl,2));
fprintf(2,'vl:               %g %g\n', size(vl,1), size(vl,2));
%fprintf(2,'unique(cl)        %g   \n', unique(cl));
%fprintf(2,'nparts:           %g\n', nparts);
fprintf(2,'NweakClassifiers: %g\n', NweakClassifiers);
fprintf(2,'Nthresholds:      %g\n', Nthresholds);
fprintf(2,'nClasses:         %g\n', nClasses);

%T = [1 0 0; 0 1 0; 0 0 1; 1 1 0; 1 0 1; 0 1 1; 1 1 1]'; % this for 3 classes 
T = de2bi(2:2^nClasses-1)';
% learning 
classifier = jointBoosting(features, cl, T, NweakClassifiers, Nthresholds);

% run classifier
[Cx, Fx, Fn, Fstumps] = strongJointClassifier(featurestest, classifier, T);

FxExp = exp(Fx);
prob = FxExp ./ repmat(sum(FxExp,2),1,nClasses) ;

numTest = length(vl);
corrProb = zeros(numTest,1);
for i = 1:numTest
    corrProb(i,1) = prob(i,vl(1,i));
end

[maxProb, idxMaxProb] = max(prob, [], 2);

CM = confusionmat(vl', idxMaxProb);

R = zeros(numTest,1);
for i = 1:numTest
    [~,~,r] = unique(prob(i,:));
    rk = max(r) - r  + 1;
    R(i,1) = rk(vl(:,i));
end
RankTable = tabulate(R);
Correct = sum(diag(CM)); 
Total = sum(sum(CM));
Acc = 100.0 * sum(diag(CM)) / sum(sum(CM));

fprintf(2,'%d out of %d correct, i.e %g %% accuracy', Correct, Total, Acc);


flen = size(features,1);
featureplot = cell(flen,1);
s = cell(flen,nClasses);
A = cell(flen,nClasses,1);
P = cell(flen,nClasses,1);

for k = 1:flen
    featureplot{k} = figure;
    for i = 1:nClasses
        s{k,i} = subplot(nClasses,1,i);
        A{k,i} = features(:,find(cl==i));
        A2 = A{k,i};
        P{k,i} = A2(k,:);
        hist(s{k,i},P{k,i},0.0:0.01:1);
    end
    outpngfile = sprintf('%s%soutput_%d_%d_feature_%d.png', outDir, filesep, mId, bUnary, k);
    saveas(featureplot{k},outpngfile);
end

outdatafile = sprintf('%s%soutput_%d_%d.mat', outDir, filesep, mId, bUnary );


save(outdatafile, 'mId', 'Cx', 'Fx', 'Fn', 'Fstumps', 'FxExp', 'prob', ...
    'corrProb', 'maxProb', 'idxMaxProb','vl','CM','Correct', 'Total', ...
    'Acc','R','A','s','P','flen');

end
