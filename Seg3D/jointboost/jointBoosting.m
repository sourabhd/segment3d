
function classifier = jointBoosting(x, c, T, Nrounds, Nthresholds, classifier)
%
% classifier = jointBoosting(x, c, T, Nrounds, Nthresholds);
%
% Input parameters:
%  x: training data (each column is one data point). 
%      x = matrix [Nfeature*Nsamples]
%
%  c: class label. 0 = class of background (none of the classes)
%                 c = j (with j > 0) is an object class
%
%  T: sharing matrix: it provides the possible sharing patterns
%      T = matrix [Nclasses, Nnodes]
%      Each column is one sharing pattern. 
%       All the "1" entries indicate the classes that can share a feature.
%
%  Other parameters:
%      Nrounds = number of weak classifiers
%      Nthresholds = number of thresholds used to create each stump
%
% Output 
%  classifier: struct array with
%    classifier(m).featureNdx 
%    classifier(m).th
%    classifier(m).a
%    classifier(m).b
%    classifier(m).bestnode
%    classifier(m).sharing = find(T(:,bestnode));
%
% If you want to add more rounds to a previously trained classifier, you
% can pass the classifier as an additional parameter:
%    classifier = jointBoosting(x, c, T, Nrounds, Nthresholds, classifier);

c = c(:);
[Nfeatures, Nsamples] = size(x)
Nclasses = double(max(c))

% thresholds are discretized for efficiency
if nargin < 5; 
    Nthresholds=100; 
end
thresholds = getThresholds(x, Nthresholds);
Nthresholds = size(thresholds, 2)

% you can pass as an argument a classifier, and it will add more rounds to
% it.
if nargin == 6
    % Initialize variables with the output of the classifier
    [CxS, FxS] = strongJointClassifier(x, classifier, T);
    init = length(classifier)+1;

    for j=1:Nclasses
        yJ = 2*double(c==j)-1;
        Fx{j} = FxS(:,j);
        w{j}  = exp(-yJ.*Fx{j});
    end
else
    % Initialize weights and classifiers
    for j=1:Nclasses
        Fx{j} = zeros(Nsamples, 1);
        w{j}  = ones (Nsamples, 1);
    end
    % Initialize class constants
    b0 = zeros(1,Nclasses);
    classifier(1).b0 = b0;
    
    init = 1;
end
tmp = [];

for m = init:Nrounds
    % 1) Fit shared stump and find best sharing
    [a, b, th, featNdx, bestnode, b0, tmp] = fitStumpNode(x, c, w, thresholds, T, tmp);
    
    % 2) Store parameters:
    classifier(m).featureNdx = featNdx;
    classifier(m).th = th;
    classifier(m).a  = a;
    classifier(m).b  = b;
    classifier(m).bestnode = bestnode;
    classifier(m).sharing = find(T(:,bestnode))';

    % 3) Update Fx and weights:
    %bTmp = classifier(1).b0;
    for jn = classifier(m).sharing %1:Nclasses
        % Class label for class jn: -1=background, 1=data in classe jn
        yJ = 2*double(c==jn)-1;

        %if ismember(jn, classifier(m).sharing)
        Fx{jn} = Fx{jn} + squeeze(a * double(x(featNdx,:) > th) + b)';
        w{jn} = exp(-yJ.*Fx{jn}); % weights
        %else
        %    Fx{jn} = Fx{jn} + b0(jn);
        %    bTmp(jn)  = bTmp(jn) + b0(jn);
        %end

        % Computes percent correct for each class
        cd(jn) = sum(yJ>0 & Fx{jn}>0)/sum(yJ>0);
        fa(jn) = sum(yJ<0 & Fx{jn}>0)/sum(yJ<0);
    end
    %classifier(1).b0 = bTmp;

    disp(sprintf('round %d, best feature = %d, shared among %d classes=[%s]', m, featNdx, length(classifier(m).sharing), num2str(classifier(m).sharing(:)', '%d, ')))
    disp(sprintf('  performance on training, cd = %1.2f [%1.2f, %1.2f]', mean(cd), min(cd), max(cd)))    
    disp(sprintf('  performance on training, fa = %1.2f [%1.2f, %1.2f]', mean(fa), min(fa), max(fa)))
    
    save tmp2 classifier T
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function  thresholds = getThresholds(x, Nthresholds);
% Looks for equally spaced thresholds. Same number of thresholds for each
% feature. But each feature might have a different dinamic range, so the
% thresholds might change.
%
% This function only is called once, at the beggining.

[Nfeatures, Nsamples] = size(x);

thresholds = zeros(Nfeatures, Nthresholds);
for i = 1:Nfeatures
    minX = double(min(x(i,:)));
    maxX = double(max(x(i,:)));
    ths = linspace(minX, maxX, Nthresholds+2);
    ths = ths(2:end-1);
    
    thresholds(i,:) = double(ths);
end
