%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script shows the classifier trained on toy data (2D)
clear all

rand('seed', 1)
randn('seed',1)

% PARAMETERS TOY DATA:
Nsamples = 1000; 
Nclasses = 4;
NumberOfDimensions = 2;
% Create sharing matrix (each column is one node). 
T = [1 0 0; 0 1 0; 0 0 1; 1 1 0; 1 0 1; 0 1 1; 1 1 1]'; % this for 3 classes plus background
% to remove sharing use:
% T = eye(Nclasses-1);

% PARAMETERS TRAINING SET
proportionTrainingSamples = .5;
NsamplesForTrainingPerClass = fix(Nsamples*proportionTrainingSamples);
NsamplesBackgroundPerObjectSample = 6;
NumberOfRuns = 1;

% PARAMETERS BOOSTING
NweakClassifiers = 40;

% Create toy data
samples = rand(2, Nsamples);
centers = rand(2, Nclasses-1);
D = zeros(Nclasses-1, Nsamples);
for c = 1:Nclasses-1
    D(c,:) = sum((samples - repmat(centers(:,c), [1 Nsamples])).^2);
end
[d, cl] = min(D);
cl = cl.*(d<.05);

% visualize classes
colors = 'krgbck';
figure; axis('square')
for c = 0:Nclasses-1
    j = find(cl==c);
    plot(samples(1,j), samples(2,j), 'o', 'color', colors(c+1));
    hold on
    text(mean(samples(1,j)), mean(samples(2,j)), num2str(c), 'fontsize', 16)
end



Nnodes = size(T,2)

% With this we can create features that are rotated stumps
theta = linspace(0,pi,60); theta = theta(1:end-1);
features=[];
for i = 1:length(theta)
    features(i,:) = cos(theta(i))*(samples(1,:)-.5) + sin(theta(i))*(samples(2,:)-.5);
end


% LEARNING CLASSIFIER
Nthresholds = 100;
classifier = jointBoosting(features, cl, T, NweakClassifiers, Nthresholds);


% SHOW THE CLASSIFIER
[xt,yt] = meshgrid(0:.01:1, 0:.01:1); [ncols nrows] = size(xt);
xt = xt(:); yt = yt(:);
theta = linspace(0,pi,60); theta = theta(1:end-1);
featurestest = [];
for i = 1:length(theta)
    featurestest(i,:) = cos(theta(i))*(xt-.5) + sin(theta(i))*(yt-.5);
end

% run classifier
[Cx, Fx, Fn, Fstumps] = strongJointClassifier(featurestest, classifier, T);

% show classifiers for each class
figure('name', 'classifiers for each class')
for n = 1:Nclasses-1
    subplot(1,Nclasses-1,n)
    imagesc(reshape(Fx(:,n), [ncols nrows])); colormap(gray(256))
    title(n)
    axis('xy'); axis('equal'); axis('tight')
end

figure('name', 'nodes')
for n = 1:Nnodes
    subplot(1,Nnodes,n)
    imagesc(reshape(Fn(:,n), [ncols nrows])); colormap(gray(256))
    title(T(:,n))
    axis('xy'); axis('equal'); axis('tight')
end






