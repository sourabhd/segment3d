//
//  testDihedral.cpp
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 03/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include "testDihedral.h"

void testDihedral(void)
{
    MyMesh msh;

#if 0
    Point p(0.0,0.0,0.0);
    Point q(1.0,0.0,0.0);
    Point r(0.0,1.0,0.0);
    Point s(0.0,0.0,1.0);
    
    Polyhedron P;
    P.make_tetrahedron(p,q,r,s);

    msh.M = P;
    setIDs(msh);
#endif



    //CGALPolyhedron2OFF::OFF2MyMesh(msh,"/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/models/geomview/cube.off");
    //CGALPolyhedron2OFF::OFF2MyMesh(msh,"/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/models/geomview/tetra.off");
    //CGALPolyhedron2OFF::OFF2MyMesh(msh,"/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/models/geomview/octa.off");
    //CGALPolyhedron2OFF::OFF2MyMesh(msh,"/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/models/geomview/dodec.off");
    //CGALPolyhedron2OFF::OFF2MyMesh(msh,"/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/models/geomview/dodec2.off");
    CGALPolyhedron2OFF::OFF2MyMesh(msh,"/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/models/geomview/icosa.off");
   //const string onamePairwise = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/output/test/pairwise.off";
    vector<pair<Idx,Idx> > F;
    vector<double> dAngle;

    F.clear();
    dAngle.clear();

    Edge_iterator edgesBegin = msh.M.edges_begin();
    Edge_iterator edgesEnd = msh.M.edges_end();

    for (Edge_iterator itr = edgesBegin; itr != edgesEnd; ++itr) { // for each edge
        // get two adjacent faces
        Idx fid1 = itr->face()->id();
        Idx fid2 = itr->opposite()->face()->id();
        PairwiseFeature P;
        msh.PMap.insert(make_pair(make_pair(fid1,fid2), P));
        F.push_back(make_pair(fid1, fid2));
    }




    //showPairwiseFeatures(msh, onamePairwise);


  #if 1

    double minA = 0.0, maxA = 0.0, avgA = 0.0,devA = 0.0;
    calcPairwiseDihedralAngles(msh,F,dAngle,minA, maxA, avgA,devA);
    Idx numPairwise = F.size(); // num of pair wise features
    cout << " : #PAIR  : " << numPairwise << endl;
    cout << " : MINA   : " << minA << endl;
    cout << " : MAXA   : " << maxA << endl;
    cout << " : AVGA   : " << avgA << endl;
    cout << " : DEVA   : " << devA << endl;

    cout << "Number of edges: " << F.size() << endl;
    int sz = int(F.size());
    for (int i = 0 ; i < sz; i++) {
        cout << F[i].first << " " << F[i].second << " :: " << dAngle[i] << endl;
    }
#endif

}
