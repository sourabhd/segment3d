//
//  testIncrementalBuilder.h
//  Segment3D
//
//  Created by Sourabh Daptardar on 29/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __Segment3D__testIncrementalBuilder__
#define __Segment3D__testIncrementalBuilder__

#include <iostream>
#include "common.h"

int testIncrementalBuilder(void);

#endif /* defined(__Segment3D__testIncrementalBuilder__) */
