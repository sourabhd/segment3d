#include "testSDF.h"
#include "common.h"
#include "CGALPolyhedron2OFF.h"

#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>

using namespace std;
using namespace CGAL;

#include <CGAL/Simple_cartesian.h>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/AABB_polyhedron_triangle_primitive.h>
typedef CGAL::Simple_cartesian<double> K;
typedef K::FT FT;
typedef K::Point_3 Point;
typedef K::Ray_3 Ray;
typedef K::Segment_3 Segment;

typedef CGAL::AABB_polyhedron_triangle_primitive<K,Polyhedron> Primitive;
typedef CGAL::AABB_traits<K, Primitive> ATraits;
typedef CGAL::AABB_tree<ATraits> Tree;
typedef Tree::Object_and_primitive_id Object_and_primitive_id;
typedef Tree::Point_and_primitive_id Point_and_primitive_id;

int testSDF()
{
    const string offFile = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/models/geomview/unitcube.off";
    MyMesh msh;
    CGALPolyhedron2OFF::OFF2MyMesh(msh,offFile);

#if 0
    Point p(1.0, 0.0, 0.0);
    Point q(0.0, 1.0, 0.0);
    Point r(0.0, 0.0, 1.0);
    Point s(0.0, 0.0, 0.0);

    msh.M.make_tetrahedron(p, q, r, s);
#endif


    // constructs AABB tree and computes internal KD-tree
    // data structure to accelerate distance queries
    Tree tree(msh.M.facets_begin(),msh.M.facets_end());
    tree.accelerate_distance_queries();

    // query point
    //Point query(0.0, 0.0, 3.0);
    Point a(0.5,0.5,0.5);
    //Point b(0.5,0.5,-1.0);
    Point b(0.0,0.0,0.0);
    Ray query(a,b);

    tree.do_intersect(query);

    cout << "Intersect: " << tree.number_of_intersected_primitives(query) << endl;

    std::vector<Object_and_primitive_id> intersections;
    tree.all_intersections(query,back_inserter(intersections));

    cout << "# intersections : " << intersections.size() << endl;

    int numInt = intersections.size();
    for (int i = 0; i < numInt ; i++) {
        Polyhedron::Facet_handle f = intersections[i].second;
        cout << f->id() << endl;
    }

#if 0
    // computes squared distance from query
    FT sqd = tree.squared_distance(query);
    std::cout << "squared distance: " << sqd << std::endl;
    // computes closest point
    Point closest = tree.closest_point(query);
    std::cout << "closest point: " << closest << std::endl;
    // computes closest point and primitive id
    Point_and_primitive_id pp = tree.closest_point_and_primitive(query);
    Point closest_point = pp.first;
    Polyhedron::Face_handle f = pp.second; // closest primitive id
    std::cout << "closest point: " << closest_point << std::endl;
    std::cout << "closest triangle: ( "
              << f->halfedge()->vertex()->point() << " , "
              << f->halfedge()->next()->vertex()->point() << " , "
              << f->halfedge()->next()->next()->vertex()->point()
              << " )" << std::endl;

#endif
    return EXIT_SUCCESS;
}

