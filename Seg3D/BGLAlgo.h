//
//  BGLAlgo.h
//  Segment3D
//
//  Created by Sourabh Daptardar on 22/12/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __Segment3D__BGLAlgo__
#define __Segment3D__BGLAlgo__

#include "common.h"
#include "Dual.h"

using namespace std;
using namespace CGAL;
using namespace boost;

void calcAllPairsShortestPaths(BGLGraph &G, double **M);
void poly2BGL(MyMesh &msh, EdgeFn efn);
void calcAllPairsShortestPaths(MyMesh &msh, EdgeFn efn, double **dist);

#endif /* defined(__Segment3D__BGLAlgo__) */
