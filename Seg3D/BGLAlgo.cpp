//
//  BGLAlgo.cpp
//  Segment3D
//
//  Created by Sourabh Daptardar on 22/12/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include "BGLAlgo.h"

using namespace std;
using namespace boost;
using namespace CGAL;


void poly2BGL(MyMesh &msh, EdgeFn efn)
{
    //setIDsIfRequired(P);
    msh.dual = dual(msh.M);

#if 0
    string dfile = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/output/dual_bgl.off";
    CGALPolyhedron2OFF::poly2OFF(msh.M, dfile);
#endif

    //int num_nodes = int(msh.dual.size_of_vertices());
    //int num_vertices = int(msh.dual.size_of_halfedges());
    Vertex_iterator vertexBegin = msh.dual.vertices_begin();
    Vertex_iterator vertexEnd = msh.dual.vertices_end();

    //vector<BGLEdge> edges; edges.clear();
    //vector<double> weights; weights.clear();

    // Add edges to adjacency list data structure

    for (Vertex_iterator vitr = vertexBegin; vitr != vertexEnd ; ++vitr) {
        Halfedge_handle h = vitr->halfedge();
        Halfedge_vertex_circulator v = h->vertex_begin();
        do {
            //cout << vitr->id() << " , " << v->opposite()->vertex()->id() <<endl;
            weight_t w = efn(*(v->opposite()->vertex()),*vitr);
            label_t x = vitr->id();
            label_t y = v->opposite()->vertex()->id();
            //edges.push_back(BGLEdge(x,y));
            //weights.push_back(w);
            add_edge(x,y,EdgeWeightProperty(w),msh.dualG);
        } while (++v != h->vertex_begin());
    }

    cout << "End of poly2BGL" << endl;
}



void calcAllPairsShortestPaths(BGLGraph &G, double **M)
{
    
    
#if 0
    const int V = 6;
    
    BGLEdge edge_array[] =
    { BGLEdge(0, 1), BGLEdge(0, 2), BGLEdge(0, 3), BGLEdge(0, 4), BGLEdge(0, 5),
        BGLEdge(1, 2), BGLEdge(1, 5), BGLEdge(1, 3), BGLEdge(2, 4), BGLEdge(2, 5),
        BGLEdge(3, 2), BGLEdge(4, 3), BGLEdge(4, 1), BGLEdge(5, 4)
    };
    
    const std::size_t E = sizeof(edge_array) / sizeof(BGLEdge);
    for (std::size_t j = 0; j < E; j++) {
        add_edge(edge_array[j].first, edge_array[j].second, G);
    }
    
    BGLEdgeWeight w = get(edge_weight, G);
    
    //double weights[] = { 0, 0, 0, 0, 0, 3, -4, 8, 1, 7, 4, -5, 2, 6 };
    double weights[] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
    double *wp = weights;
    
    BGLEdgeIterator e, e_end;
    for (boost::tie(e, e_end) = edges(G); e != e_end; ++e) {
        w[*e] = *wp++;
    }
#endif
   
    const int V = int(boost::num_vertices(G));
    std::vector <double>d(V, (std::numeric_limits <double>::max)());
    johnson_all_pairs_shortest_paths(G, M, distance_map(&d[0]));
  
#if 0
    std::cout << "       ";
    for (int k = 0; k < V; ++k)
        std::cout << std::setw(5) << k;
    std::cout << std::endl;
    for (int i = 0; i < V; ++i) {
        std::cout << std::setw(3) << i << " -> ";
        for (int j = 0; j < V; ++j) {
            if (M[i][j] == (std::numeric_limits<double>::max)())
                std::cout << std::setw(15) << "inf";
            else
                std::cout << std::setw(15) << M[i][j];
        }
        std::cout << std::endl;
    }
#endif
    
#if 0
    std::ofstream fout("output/Bird/johnson-eg.dot");
    fout << "digraph A {\n"
    << "  rankdir=LR\n"
    << "size=\"5,3\"\n"
    << "ratio=\"fill\"\n"
    << "edge[style=\"bold\"]\n" << "node[shape=\"circle\"]\n";
    
    graph_traits < BGLGraph >::edge_iterator ei, ei_end;
    for (tie(ei, ei_end) = edges(G); ei != ei_end; ++ei)
        fout << source(*ei, G) << " -> " << target(*ei, G)
        << "[label=" << get(edge_weight, G)[*ei] << "]\n";
    
    fout << "}\n";
#endif
    
}

void calcAllPairsShortestPaths(MyMesh &msh, EdgeFn efn, double **dist)
{
    clock_t c1 = clock();
    time_t  t1 = time(NULL);
    poly2BGL(msh, efn);
    calcAllPairsShortestPaths(msh.dualG, dist);
    time_t  t2 = time(NULL);
    clock_t c2 = clock();
    cout << "Clock: " << (c2 - c1) / CLOCKS_PER_SEC << endl;
    cout << "Time:  " << (t2 - t1) << endl;
}
