//
//  hds2.cpp
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 02/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>
#include <iomanip>

#include "hds2.h"
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>


using namespace std;
using namespace CGAL;

typedef CGAL::Simple_cartesian<double>    Kernel;
typedef Kernel::Point_3                   Point;
typedef CGAL::Polyhedron_3<Kernel>        Polyhedron;
typedef Polyhedron::Facet_iterator        Facet_iterator;
typedef Polyhedron::Halfedge_around_facet_circulator Halfedge_facet_circulator;

void hds2(void)
{
    Point p(0.0,0.0,0.0);
    Point q(1.0,0.0,0.0);
    Point r(0.0,1.0,0.0);
    Point s(0.0,0.0,1.0);
    
    Polyhedron P;
    P.make_tetrahedron(p,q,r,s);
    
    // Write in OFF format
    // Header
    cout << "OFF" << endl;
    
    // Number of vertices, faces,
    cout  <<  P.size_of_vertices() << " " << P.size_of_facets() << " 0"
    << endl;
    
    
    // Copy the points (vertices)
    copy(P.points_begin(), P.points_end(), std::ostream_iterator<Point>(cout, "\n"));
    
    // Faces
    for (Facet_iterator i = P.facets_begin(); i != P.facets_end() ; ++i) { // for each face
        
        Halfedge_facet_circulator j = i->facet_begin();
        // Assert that faces are atleast triangles
        CGAL_assertion(CGAL::circulator_size(j) >= 3);
        // Put the number of facets
        cout << CGAL::circulator_size(j);
        
        // Now we need indices for facets
        
        do {
            cout << " " << std::distance(P.vertices_begin(), j->vertex());
        } while (++j != i->facet_begin());  // While we are not back at the vertex
        cout << endl;
    }
    
}