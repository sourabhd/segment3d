#include "meshviewer.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>

extern "C" {
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
}

using namespace std;

MeshViewer* MeshViewer::pInstance = NULL;


const string wspace = "\t\b ";

static void trimR(string &S, const string &trimChar=wspace)
{
    string::size_type pos = S.find_last_not_of(trimChar);
    S.erase(pos+1);
}

static void trimL(string &S, const string &trimChar=wspace)
{
    string::size_type pos = S.find_first_not_of(trimChar);
    S.erase(0,pos);
}

static void trim(string &S, const string &trimChar=wspace)
{
    trimR(S,trimChar);
    trimL(S,trimChar);
}

static void stripComment(string &line)
{
    line.erase( std::find( line.begin(), line.end(), '#' ), line.end() );
}

bool MeshViewer::checkHeader(void)
{
    bool isOFF = false;
    string line = "";
    while (std::getline(ifs, line)) {
        stripComment(line);
        trim(line);
        if (line == "") continue;
        if (line == "OFF") {
            isOFF = true;
            break;
        }
    }
    return isOFF;
}

void MeshViewer::getDimensions(void)
{
    string line = "";
    while (std::getline(ifs, line)) {
        stripComment(line);
        trim(line);
        if (line == "") continue;
        istringstream iss(line);
        //cout << "*" << iss.str().c_str()  << "*" << endl;
        iss >> numVertices >> numFaces >> numEdges;
        break;
    }
}

void MeshViewer::getVertexCoords(void)
{
    int i = 0;
    string line = "";
    while (std::getline(ifs, line)) {
        stripComment(line);
        trim(line);
        if (line == "") continue;
        istringstream iss(line);
        //cout << "*" << iss.str().c_str()  << "*" << endl;
        iss >> vertex_coords[i] >> vertex_coords[i+1] >> vertex_coords[i+2];
        i += 3;
        if (i >= 3*numVertices) break;
    }

    float mn = numeric_limits<float>::max();
    float mx = numeric_limits<float>::min();
    //int mn_i = 0, mx_i = 0;
    for (int i = 0 ; i < 3 * numVertices ; i++ ) {
        if (vertex_coords[i] > mx) {
            mx = vertex_coords[i];
            //mx_i = i;
        }

        if (vertex_coords[i] < mn) {
            mn = vertex_coords[i];
            //mn_i = i;
        }
    }

    for (int i = 0 ; i < 3 * numVertices ; i++ ) vertex_coords[i] -= mn;

    for (int i = 0 ; i < 3 * numVertices ; i++ ) vertex_coords[i] /= (mx-mn);

    for (int i = 0 ; i < numVertices ; i++ ) {
        color_coords[3*i] = 1.0;
        color_coords[3*i+1] = 0.0;
        color_coords[3*i+2] = 0.0;
    }

}

void MeshViewer::getFaceIndices(void)
{
    int i = 0;
    string line = "";
    while (std::getline(ifs, line)) {
        stripComment(line);
        trim(line);
        if (line == "") continue;
        istringstream iss(line);
        int nVertices = 0;
        iss >> nVertices;
        face_vertex_count.push_back(nVertices);
        unsigned int idx = 0;
        for (int j = 0 ; j < nVertices ; j++) {
            iss >> idx;
            face_vertex_indices.push_back(idx);
        }
        float color = 0.0;
        int color_cnt = 0;
        while (iss >> color) {
            face_vertex_color.push_back(color);
            color_cnt++;
        }
        face_color_count.push_back(color_cnt);

        i++;
        if (i >= numFaces) break;
    }
}

MeshViewer::MeshViewer():rot_x(0.0),rot_y(0.0),rot_z(0.0)
{

}

MeshViewer::~MeshViewer()
{
    ifs.close();
    delete [] vertex_coords;
    delete [] color_coords;
    delete [] face_vertex_idx;
}

MeshViewer* MeshViewer::getInstance(void)
{
    if (!pInstance)
        pInstance = new MeshViewer;

    return pInstance;
}

void MeshViewer::display(const string &offFile)
{
    ifs.open(offFile.c_str());
    if (!ifs) {
        cerr << "Error opening file " << offFile.c_str() << endl;
        return;
    }

    bool bHeaderOK = checkHeader();
    if (!bHeaderOK) {
        cerr << "Does not look like an OFF format mesh file " << endl;
        ifs.close();
        return;
    }

    getDimensions();
    cout << "#Vertices : "<< numVertices << " #Faces : " << numFaces << " #Edges : " << numEdges << endl;
    //istringstream iss(line);
    //cout << "*" << iss.str().c_str()  << "*" << endl;

    vertex_coords = new float[3*numVertices];
    color_coords = new float[3*numVertices];

    getVertexCoords();

    // cout << endl;
    // for (int i = 0 ; i < 3*numVertices ; i++) cout << vertex_coords[i] << " " << flush;
    // cout << endl;

    getFaceIndices();

//    cout << endl;
//    int cnt = 0;
//    for (int i = 0 ; i < numFaces ; i++) {
//        for (int j = 0 ; j < face_vertex_count[i] ; j++) {
//            cout << face_vertex_indices[cnt] << " " << flush;
//            cnt++;
//        }
//        cout << endl;
//    }
//    cout << endl;


    //    cout << endl;
    //    cnt = 0;
    //    for (int i = 0 ; i < numFaces ; i++) {
    //        for (int j = 0 ; j < face_vertex_count[i] ; j++) {
    //            cout << face_vertex_color[cnt] << " " << flush;
    //            cnt++;
    //        }
    //        cout << endl;
    //    }
    //    cout << endl;


    numSides = face_vertex_count[0];  // Assuming regularity in meshes
    face_vertex_idx = new unsigned int[numSides*numFaces];
    copy(face_vertex_indices.begin(),face_vertex_indices.end(),face_vertex_idx);
}


void MeshViewerInit(void)
{
    glEnable(GL_DEPTH_TEST);
    glClearColor(0,0,0,0.7);
    glEnable (GL_BLEND);
    //glBlendFunc (GL_ONE, GL_ONE);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //glEnable(GL_POLYGON_STIPPLE);
    // Generate vertex buffer objects
    glGenBuffers(1,&((MeshViewer::getInstance())->vboId));
    glGenBuffers(1,&((MeshViewer::getInstance())->idxId));
    glGenBuffers(1,&((MeshViewer::getInstance())->colorId));
    // Bind buffers
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ((MeshViewer::getInstance())->vboId));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ((MeshViewer::getInstance())->idxId));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ((MeshViewer::getInstance())->colorId));
    // Associate VBO with data
    glBufferData(GL_ARRAY_BUFFER, 3 * ((MeshViewer::getInstance())->numVertices) * sizeof(float), ((MeshViewer::getInstance())->vertex_coords), GL_STATIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, 3 * ((MeshViewer::getInstance())->numVertices) * sizeof(float), ((MeshViewer::getInstance())->color_coords), GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, ((MeshViewer::getInstance())->numSides) * ((MeshViewer::getInstance())->numFaces) * sizeof(unsigned int) , ((MeshViewer::getInstance())->face_vertex_idx), GL_STATIC_DRAW);
}


void MeshViewerDisplay(void)
{
    // Identity transform
    glLoadIdentity();
    // Clear Buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //glEdgeFlag(GL_TRUE);
    //glPolygonMode(GL_FRONT_AND_BACK,GL_LINE|GL_LINE);
    //glPolygonMode(GL_FRONT,GL_LINE|GL_LINE);

    // Handle rotation for user
    glRotatef( (MeshViewer::getInstance())->rot_x, 1.0, 0.0, 0.0 );
    glRotatef( (MeshViewer::getInstance())->rot_y, 0.0, 1.0, 0.0 );
    glRotatef( (MeshViewer::getInstance())->rot_z, 0.0, 0.0, 1.0 );

    // Bind buffers
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ((MeshViewer::getInstance())->vboId));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ((MeshViewer::getInstance())->idxId));
    // Associate VBO with data
    glBufferData(GL_ARRAY_BUFFER, 3 * ((MeshViewer::getInstance())->numVertices) * sizeof(float), ((MeshViewer::getInstance())->vertex_coords), GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, ((MeshViewer::getInstance())->numSides) * ((MeshViewer::getInstance())->numFaces) * sizeof(unsigned int), ((MeshViewer::getInstance())->face_vertex_idx), GL_STATIC_DRAW);

    // Enable client state
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);

    // size, type, stride, pointer
    //glVertexPointer(3, GL_FLOAT, 0, C);
    glVertexPointer(3, GL_FLOAT, 0, ((MeshViewer::getInstance())->vertex_coords));
    glColorPointer(3, GL_FLOAT, 0, ((MeshViewer::getInstance())->color_coords));


    // Draw array
    // start, count
    //glDrawArrays(GL_TRIANGLES,0,3);
    //glDrawElements(GL_TRIANGLES,3,GL_UNSIGNED_INT, (void *)idx);
    glDrawElements(GL_POLYGON,((MeshViewer::getInstance())->numSides) * ((MeshViewer::getInstance())->numFaces) ,GL_UNSIGNED_INT, (void *)0);

    // Disable client states
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);

    // Done with buffer, unbind ...
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

    // Now, swap the buffers
    glFlush();
    glutSwapBuffers();
    cout << "Display sent " << endl << flush;
}


void MeshViewerSpecialKeys(int key, int x, int y)
{
    if (key == GLUT_KEY_RIGHT)
        (MeshViewer::getInstance())->rot_y += 10.0;

    if (key == GLUT_KEY_LEFT)
        (MeshViewer::getInstance())->rot_y -= 10.0;

    if (key == GLUT_KEY_UP)
        (MeshViewer::getInstance())->rot_x += 10.0;

    if (key == GLUT_KEY_DOWN)
        (MeshViewer::getInstance())->rot_x -= 10.0;

    if (key == 'j')
        (MeshViewer::getInstance())->rot_z += 10.0;

    if (key == 'k')
        (MeshViewer::getInstance())->rot_z -= 10.0;

    glutPostRedisplay();
}



void MeshViewerInitGlutCallbacks(void)
{
    glutDisplayFunc(MeshViewerDisplay);
    glutSpecialFunc(MeshViewerSpecialKeys);
}

void MeshViewerMainLoop(void)
{
    glClearColor(0.0f,0.0f,0.0f,0.0f);
    glutMainLoop();
    glDeleteBuffers(1,&((MeshViewer::getInstance())->idxId));
    glDeleteBuffers(1,&((MeshViewer::getInstance())->vboId));
}
