#ifndef RUNCURVATURE_H
#define RUNCURVATURE_H

#include "common.h"

using namespace std;

void runCurvature(string category);
void calcCurvatureFeature(MyMesh &msh,const int meshNum, const string &category,
                          const string &offFile,
                          const string &onameUnary,
                          const string &onamePairwise);

#endif // RUNCURVATURE_H
