//
//  ReadDataset.h
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 03/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __HelloCGAL__ReadDataset__
#define __HelloCGAL__ReadDataset__

#include <iostream>
#include <string>
#include "common.h"
#include "DihedralAngles.h"

using namespace std;

//const string datadir = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/Paper/LabeledDB_new/Bird";
//const string outdir = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/output";



void readDataset(const string &category, const int x, const int y, tr1::unordered_map<string,MeshCategory> &Meshes,tr1::unordered_map<string,int> &CatNumLabels);
void saveMyMesh(MyMesh &msh);
void loadMyMesh(MyMesh &msh);


#endif /* defined(__HelloCGAL__ReadDataset__) */
