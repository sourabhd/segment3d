//
//  testIncrementalBuilder.cpp
//  Segment3D
//
//  Created by Sourabh Daptardar on 29/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

// Based on sample code from CGAL manual
// Aims to test sanity of incremental builder for 3 / 4 sided polygons

#include "testIncrementalBuilder.h"

// A modifier creating a triangle with the incremental builder.
template <class HDS>
class Build_triangle : public CGAL::Modifier_base<HDS> {
public:
    Build_triangle() {}
    void operator()( HDS& hds) {
        // Postcondition: hds is a valid polyhedral surface.
        CGAL::Polyhedron_incremental_builder_3<HDS> B( hds, true);
        //B.begin_surface( 3, 1, 6);
        B.begin_surface( 4, 1, 0,0);
        typedef typename HDS::Vertex   Vertex;
        typedef typename Vertex::Point Point;
        B.add_vertex( Point( 0, 0, 0));
        B.add_vertex( Point( 1, 0, 0));
        B.add_vertex( Point( 0, 1, 0));
        B.add_vertex( Point( 1, 1, 0));
        B.add_vertex( Point( 0, 0, 1));
        B.add_vertex( Point( 1, 0, 0));
        
        B.begin_facet();
        B.add_vertex_to_facet( 0);
        B.add_vertex_to_facet( 1);
        B.add_vertex_to_facet( 2);
        B.add_vertex_to_facet( 3);
        B.end_facet();
        
        //B.end_surface();
        //B.begin_surface(4,1,0,0);
        
        
        //B.add_vertex( Point( 0, 0, 0));
        //B.add_vertex( Point( 1, 0, 0));
        //B.add_vertex( Point( 0, 0, 1));
        //B.add_vertex( Point( 1, 0, 0));
               
        
        B.begin_facet();
        B.add_vertex_to_facet( 1);
        B.add_vertex_to_facet( 0);
        B.add_vertex_to_facet( 4);
        B.add_vertex_to_facet( 5);
        B.end_facet();

        
        B.end_surface();
    }
};

int testIncrementalBuilder(void)
{
    Polyhedron P;
    Build_triangle<HalfedgeDS> triangle;
    P.delegate( triangle);
    //CGAL_assertion( P.is_triangle(P.halfedges_begin()));
    cout << P << endl;
    return 0;
}
