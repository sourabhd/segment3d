#include "common.h"
#include "Curvature.h"
#include "SDF.h"
#include <eigen3/Eigen/Dense>

using namespace std;
using namespace CGAL;
using namespace Eigen;




/* C Source for the "standard" hot-to-cold colour ramp by Paul Bourke */
/* Rference: http://paulbourke.net/texture_colour/colourspace/ */
/*
   Return a RGB colour value given a scalar v in the range [vmin,vmax]
   In this case each colour component ranges from 0 (no contribution) to
   1 (fully saturated), modifications for other ranges is trivial.
   The colour is clipped at the end of the scales if v is outside
   the range [vmin,vmax]
*/


COLOUR GetColour(double v,double vmin,double vmax)
{
    COLOUR c = {1.0,1.0,1.0}; // white
    double dv;

    if (v < vmin)
        v = vmin;
    if (v > vmax)
        v = vmax;
    dv = vmax - vmin;

    if (v < (vmin + 0.25 * dv)) {
        c.r = 0;
        c.g = 4 * (v - vmin) / dv;
    } else if (v < (vmin + 0.5 * dv)) {
        c.r = 0;
        c.b = 1 + 4 * (vmin + 0.25 * dv - v) / dv;
    } else if (v < (vmin + 0.75 * dv)) {
        c.r = 4 * (v - vmin - 0.5 * dv) / dv;
        c.b = 0;
    } else {
        c.g = 1 + 4 * (vmin + 0.75 * dv - v) / dv;
        c.b = 0;
    }

    return(c);
}



double area(MyPolygon &P)
{

    int N = P.numVertices;
    double area = 0.0;
    Vector V(0.0,0.0,0.0);

    for (int i = 1 ; i < N ; i++) {
        V = V + cross_product(P.vertex[i],P.vertex[i-1]);
    }
    V = V + cross_product(P.vertex[N-1],P.vertex[0]);

    area =  0.5 * P.normal * V;
    return area;
}

void curvature(Facet_iterator fitr, double &k1, double &k2)
{
    MyPolygon P;
    facetItrToPolygon(fitr,P);
    COUT << P << endl;


    k1 = 0.0; k2 = 0.0;
    int k1cnt = 0, k2cnt = 0;
    double k1min = LARGE, k2min = LARGE;

    //Point pt = circ->opposite()->vertex()->point();
    cout << "NumVertices:" << P.vertex.size() << endl;
    for (int oV = 0 ; oV < int(P.vertex.size()); oV++) {
        Point pt = Point(P.vertex[oV].x(),P.vertex[oV].y(),P.vertex[oV].z()) ;

        Vector n = Vector(P.normal.x(), P.normal.y(), P.normal.z());
        double n_mag = sqrt(n.squared_length());
        if (n_mag > 0) {
            n = n / n_mag;   // normalize to unit vector
        }

        Vector t = Vector(P.vertex[(oV+1)% int(P.vertex.size())] - P.vertex[oV]);
        double t_mag = sqrt(t.squared_length());
        if (t_mag > 0) {
            t = t / t_mag;   // normalize to unit vector
        }

        Vector s = cross_product(t,n);

        COUT << "n : " << n << endl;
        COUT << "t : " << t << endl;
        COUT << "s : " << s << endl;

        //const int M = 3 * P.numVertices;
        //const int N = 7;
        const int N = 3;

        Eigen::Matrix<double,Dynamic,Dynamic> A;
        Eigen::Matrix<double,Dynamic,1> b;
        //A.setZero(M,N);
        //b.setZero(M,1);

        Halfedge_handle h = fitr->halfedge();
        Halfedge_vertex_circulator circ = h->vertex_begin();
        vector<MyPolygon> V1;
        vector<Halfedge_handle> H1;
        V1.clear();
        H1.clear();
        int jj = 0;
        do {
            if (jj > 0) {
                MyPolygon Q;
                Halfedge_handle hd = circ->opposite()->vertex()->halfedge();
                facetItrToPolygon(hd->face(),Q);
                V1.push_back(Q);
                H1.push_back(hd);
            }
            jj++;

        } while(++circ != h->vertex_begin());


        vector<MyPolygon> V2;
        vector<Halfedge_handle> H2;
        H2.clear();
        V2.clear();
#if 0
        // Uncomment to include ring-2
        //    for (int i = 0 ; i < V1.size() ; i++ ) {
        //        Halfedge_handle h = H1[i]->vertex_begin();
        //        Halfedge_vertex_circulator c = h->vertex_begin();
        //        do {
        //            MyPolygon Q;
        //            Halfedge_handle hh = c->opposite()->vertex()->halfedge();
        //            facetItrToPolygon(hh->face(),Q);
        //            V2.push_back(Q);
        //            H2.push_back(hh);
        //        } while(c != h->vertex_begin());
        //    }
#endif

        vector<MyPolygon> V;
        vector<Halfedge_handle> H;
        V.clear();
        H.clear();
        V.reserve(V1.size()+V2.size());
        H.reserve(H1.size()+H2.size());

        V.insert(V.end(),V1.begin(),V1.end());
        V.insert(V.end(),V2.begin(),V2.end());

        H.insert(H.end(),H1.begin(),H1.end());
        H.insert(H.end(),H2.begin(),H2.end());

        //int M = 3 * jj;
        int M = 3 * V.size();
        A.setZero(M,N);
        b.setZero(M,1);

        //int j = 0;

        //Halfedge_handle hh = fitr->halfedge();
        //Halfedge_vertex_circulator circ2 = hh->vertex_begin();
        //Point pt = circ2->opposite()->vertex()->point();

        //Halfedge_facet_circulator circ = fitr->facet_begin(); // Its around vertex, remember !
        cout << "=================================================" << endl;
        for (int j = 0 ; j < int(V.size()) ; j++) {
            //do {

            //   if (j > 0) {
            //cout << "Vertex2: " << circ2->opposite()->vertex()->point() << endl;
            //MyPolygon Q;
            //facetItrToPolygon(circ->opposite()->face(),Q);
            //facetItrToPolygon(circ2->opposite()->vertex()->halfedge()->face(),Q);
            //       COUT << Q << endl;
            //        Vector diff =
            //                Vector(P.centroid.x(),P.centroid.y(),P.centroid.z())
            //                - Vector(Q.centroid.x(),Q.centroid.y(),Q.centroid.z());
            MyPolygon Q = V[j];
            Vector diff =
                    Vector(Q.centroid.x(), Q.centroid.y(), Q.centroid.z())
                    - Vector(pt.x(),pt.y(),pt.z());


            double df_mag = sqrt(diff.squared_length());
            if (df_mag > 0) {
                diff = diff / df_mag;
            }
            double x = n * diff;
            double y = t * diff;
            double z = s * diff;
            //double norm = area(Q);
            double norm = 1;
            double nl_mag = sqrt(Q.normal.squared_length());
            double xn = 0.0, yn = 0.0, zn = 0.0;
            if (nl_mag > 0) {
                xn = (n *  Q.normal) / nl_mag;
                yn = (t *  Q.normal) / nl_mag;
                zn = (s *  Q.normal) / nl_mag;
            } else {
                xn = n * Q.normal;
                yn = t * Q.normal;
                zn = s * Q.normal;
            }

            double u = -xn / (zn+EPS);
            double v = -yn / (zn+EPS);
            if (zn > EPS) {
                u = xn / zn;
                v = yn / zn;
            }

            COUT << "x: "    << x    << endl;
            COUT << "y: "    << y    << endl;
            COUT << "z: "    << z    << endl;
            COUT << "norm: " << norm << endl;
            COUT << "xn: "   << xn   << endl;
            COUT << "yn: "   << yn   << endl;
            COUT << "zn: "   << zn   << endl;
            COUT << "u: "    << u    << endl;
            COUT << "v: "    << v    << endl;
            //cerr << "0" << endl;

            A(3*j,0)     = 0.5 * x * x * norm;
            A(3*j,1)     =       x * y * norm;
            A(3*j,2)     = 0.5 * y * y * norm;
            //A(3*j,3)     = x   * x * x * norm;
            //A(3*j,4)     = x   * x * y * norm;
            //A(3*j,5)     = x   * y * x * norm;
            //A(3*j,6)     = y   * y * y * norm;
            b(3*j)       =           z * norm;

            //cerr << "1" << endl;

            A(3*j+1,0)   =           x * norm;
            A(3*j+1,1)   =           y * norm;
            A(3*j+1,2)   = 0;
            //A(3*j+1,3)   = 3.0 * x * x * norm;
            //A(3*j+1,4)   = 2.0 * x * y * norm;
            //A(3*j+1,5)   =       y * y * norm;
            //A(3*j+1,6)   = 0;
            b(3*j+1)     =           u * norm;

            //cerr << "2" << endl;


            A(3*j+2,0)   = 0;
            A(3*j+2,1)   =           x * norm;
            A(3*j+2,2)   =           y * norm;
            //A(3*j+2,3)   = 0;
            //A(3*j+2,4)   =       x * x * norm;
            //A(3*j+2,5)   = 2.0 * x * y * norm;
            //A(3*j+2,6)   = 3.0 * y * y * norm;

            //cerr << "3" << endl;


            b(3*j+2)     =           v * norm;

            //cerr << "4" << endl;

            //    }
            j++;
            //} while(++circ != fitr->facet_begin());
            // } while(++circ2 != hh->vertex_begin());

        }

        cout << "-----------------------------------------" << endl;
        cout << "A: " << endl << A << endl;
        cout << "b: " << endl << b << endl;
        cout << "-----------------------------------------" << endl;
        Eigen::Matrix<double,Dynamic,Dynamic> B = A.jacobiSvd(ComputeThinU|ComputeThinV).solve(b);
        cout << "least-square solution is :" << B << endl;

        double dwRelErr = ( A * B - b ).norm() / b.norm() ;
        cout << "Relative Error: " << dwRelErr << endl;
        double trace = B(0) + B(2);
        double det = B(0) * B(2) - B(1) * B(1);
        cout << "B: " << B(0) << " " << B(1) << " " <<  B(2) << endl;
        double kk1 = .5f * ( trace + sqrt( trace*trace - 4.0f * det ) );
        double kk2 = .5f * ( trace - sqrt( trace*trace - 4.0f * det ) );

        if (dwRelErr < 0.7) {

            if (!isnan(kk1) && kk1 < k1min)
                k1min = kk1;
            if (!isnan(kk2) && kk2 < k2min)
                k2min = kk2;
            k1cnt += 1;
            k2cnt += 1;
        }

        cout << "KK1: " << kk1 << endl;
        cout << "KK2: " << kk2 << endl;


        //C = k1 * k2;
        //C = 0.5 * (k1+k2);

    }

//    if (k1cnt > 0) {
//        k1 = k1sum / double(k1cnt);
//        k2 = k2sum / double(k2cnt);
//    }

    k1 = (((k1min > 1000.0) || (k1min < -1000.0) ) ? 0.0 : k1min);
    k2 = (((k2min > 1000.0) || (k2min < -1000.0) ) ? 0.0 : k2min);
    cout << "K1: " << k1 << endl;
    cout << "K2: " << k2 << endl;
    cout << "Gaussian Curvature: " << k1 * k2 << endl;
    cout << "Mean Curvature    : " << 0.5 * (k1+k2) << endl;


}
