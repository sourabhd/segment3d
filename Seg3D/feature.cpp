//
//  feature.cpp
//  Segment3D
//
//  Created by Sourabh Daptardar on 04/12/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include "feature.h"
#include "CGALPolyhedron2OFF.h"


using namespace std;
using namespace boost;
using namespace boost::filesystem;


map<string,Idx> distinctPartsId;
map<Idx,string> distinctPartsName;


void calcUnaryFeatures(Meshes_t &meshes, const string &category, vector<int> &Ids, map<string,int>& CatStartNum, data_t &data)
{
    vector<double> dist;

    // Handle variable number of parts
    int mx = 0;
    int mx_i = 0;
    int numMeshes = meshes[category].size();
    for (int i = 0 ; i < numMeshes ; i++) {
        if ( int(meshes[category][i].partpts.size()) > mx ) {
            mx = int(meshes[category][i].partpts.size());
            mx_i = i;
        }
    }


    vector<string> distinctParts; distinctParts.clear();
    partpoints_t::iterator ptrB = meshes[category][mx_i].partpts.begin();
    partpoints_t::iterator ptrE = meshes[category][mx_i].partpts.end();

    for(partpoints_t::iterator ptr = ptrB ; ptr != ptrE; ++ptr) {
        distinctParts.push_back(ptr->first);
    }
    sort(distinctParts.begin(), distinctParts.end());
    size_t sz = distinctParts.size();
    
    distinctPartsName.clear();
    distinctPartsId.clear();
    for (size_t i = 0 ; i < sz ; ++i ) {
        cout << distinctParts[i] << " ";
        cout << endl;
        distinctPartsId.insert(make_pair(distinctParts[i],i+1));
        distinctPartsName.insert(make_pair(i+1,distinctParts[i]));
    }

    
    BOOST_FOREACH(int id, Ids) {

        dist.clear();

        cout << "ID:" << id << endl;
        //        setIDsIfRequired(meshes[category][id].M);

        //        // Loop through the faces to associate a unary feture with each polyhedron face
        Facet_iterator fBegin = meshes[category][id].M.facets_begin();
        Facet_iterator fEnd = meshes[category][id].M.facets_end();
        for (Facet_iterator fitr = fBegin; fitr != fEnd; ++fitr) {
            UnaryFeature uf;
            uf.assocFace = fitr->id();
            meshes[category][id].UMap.insert(make_pair(fitr->id(),uf));
        }
        

        //Idx numFaces = (Idx)(meshes[category][id].M.size_of_facets());
        //meshes[category][id].dist = malloc2D<double>(numFaces,numFaces);

        //COUT << "ID: " << id << " UMap size : " << msh.UMap.size() << endl;
        
        //calcAllDistances(msh,F,dist);
        //calcAllPairsShortestPaths(meshes[category][id],Euclidean,meshes[category][id].dist);
        //COUT << "calcAllDistances completed " << endl;

        //cout << "Before for " << endl;
        
        //for (Idx i = 0 ; i < numFaces; i++) {
        int i = 0;
        for (Facet_iterator fitr = fBegin; fitr != fEnd; ++fitr) {
            cout << "Face " << fitr->id() <<" started " << endl;
            FeatureID ftid;
            ftid.category = category;
            ftid.meshNum = id; /* The image number */
            ftid.ftype = UNARY;
            ftid.ftidx = CatStartNum[category];
            //cout << "(" << F[i].first << "," << F[i].second << ")" << " : " << angle[i] << endl;
            //cout << "(" << msh.part[F[i].first] << "," << msh.part[F[i].second] << ")" << " : " << angle[i] << endl;
            
            //data.num.insert(make_pair(ftid, F[i]));
            data.id.insert(make_pair( meshes[category][id].faceID[i], ftid));
            //cout << msh.part[F[i]] << endl;
            data.featureNum.insert(make_pair(fitr->id(),i));
            data.feature.insert(make_pair(i,fitr->id()));
            data.unaryIds.push_back(fitr->id());
            double lbl = distinctPartsId[ meshes[category][id].part[fitr->id()]];
            data.C.push_back( lbl );
            meshes[category][id].UMap[fitr->id()].groundTruthLabel = lbl;
            meshes[category][id].UMap[fitr->id()].groundTruthLabelStr = meshes[category][id].part[fitr->id()];

            //            boost::numeric::ublas::vector<double> dataVec(dimUnary);
            //            for (int j = 0 ; j < dimUnary ; j++) {
            //                dataVec(j) = pow(double(dist[i]),double(j+1));
            //            }
            //cout << "Before push" << endl << flush;
            data.X.push_back(meshes[category][id].UMap[fitr->id()].dataVec);
            //cout << "After push " << endl << flush;
            //            meshes[category][id].UMap[i].dataVec = dataVec;
            i++;
            //cout << "Face " << fitr->id() << " complete " << endl;
        }

        //free2D<double>(meshes[category][id].dist,numFaces);
        cout << "Mesh" << id << " complete " << endl;
    }
    
    cout << "End of calCUnaryFeatures" << endl;

}

void calcUnaryFeatures2(Meshes_t &meshes, const string &category, vector<int> &Ids, map<string,int>& CatStartNum, data_t &data)
{
    vector<double> dist;

    // Handle variable number of parts
    int mx = 0;
    int mx_i = 0;
    int numMeshes = meshes[category].size();
    for (int i = 0 ; i < numMeshes ; i++) {
        if ( int(meshes[category][i].partpts.size()) > mx ) {
            mx = int(meshes[category][i].partpts.size());
            mx_i = i;
        }
    }


    vector<string> distinctParts; distinctParts.clear();
    partpoints_t::iterator ptrB = meshes[category][mx_i].partpts.begin();
    partpoints_t::iterator ptrE = meshes[category][mx_i].partpts.end();

    for(partpoints_t::iterator ptr = ptrB ; ptr != ptrE; ++ptr) {
        distinctParts.push_back(ptr->first);
    }
    sort(distinctParts.begin(), distinctParts.end());
    size_t sz = distinctParts.size();

    distinctPartsName.clear();
    distinctPartsId.clear();
    for (size_t i = 0 ; i < sz ; ++i ) {
        cout << distinctParts[i] << " ";
        cout << endl;
        distinctPartsId.insert(make_pair(distinctParts[i],i+1));
        distinctPartsName.insert(make_pair(i+1,distinctParts[i]));
    }


    BOOST_FOREACH(int id, Ids) {

        dist.clear();

        cout << "ID:" << id << endl;

        Facet_iterator fBegin = meshes[category][id].M.facets_begin();
        Facet_iterator fEnd = meshes[category][id].M.facets_end();

        int i = 0;
        for (Facet_iterator fitr = fBegin; fitr != fEnd; ++fitr) {
            cout << "Face " << fitr->id() <<" started " << endl;
            FeatureID ftid;
            ftid.category = category;
            ftid.meshNum = id; /* The image number */
            ftid.ftype = UNARY;
            ftid.ftidx = CatStartNum[category];
            //cout << "(" << F[i].first << "," << F[i].second << ")" << " : " << angle[i] << endl;
            //cout << "(" << msh.part[F[i].first] << "," << msh.part[F[i].second] << ")" << " : " << angle[i] << endl;

            //data.num.insert(make_pair(ftid, F[i]));
            data.id.insert(make_pair( meshes[category][id].faceID[i], ftid));
            //cout << msh.part[F[i]] << endl;
            data.featureNum.insert(make_pair(fitr->id(),i));
            data.feature.insert(make_pair(i,fitr->id()));
            data.unaryIds.push_back(fitr->id());
            double lbl = distinctPartsId[ meshes[category][id].part[fitr->id()]];
            data.C.push_back( lbl );
            meshes[category][id].UMap[fitr->id()].groundTruthLabel = lbl;
            meshes[category][id].UMap[fitr->id()].groundTruthLabelStr = meshes[category][id].part[fitr->id()];

            data.X.push_back(meshes[category][id].UMap[fitr->id()].dataVec);

            i++;
            //cout << "Face " << fitr->id() << " complete " << endl;
        }

        cout << "Mesh" << id << " complete " << endl;


    }

    cout << "End of calcUnaryFeatures2" << endl;

}



void calcPairwiseFeatures(Meshes_t &meshes, const string &category, vector<int> &Ids, map<string,int>& CatStartNum, data_t &data)
{

    vector <pair<Idx, Idx> > F; vector<double> angle;
    
    BOOST_FOREACH(int id, Ids) {

        double meanGeoDist = 0.0;
        calcMeadianGeodesicDistance(meshes[category][id], meanGeoDist);
        cout << category << " : MESH # " << id << " : MEAN GEO : " << meanGeoDist << endl;

        F.clear();
        angle.clear();
        
        Edge_iterator edgesBegin = meshes[category][id].M.edges_begin();
        Edge_iterator edgesEnd = meshes[category][id].M.edges_end();
        
        for (Edge_iterator itr = edgesBegin; itr != edgesEnd; ++itr) { // for each edge
            // get two adjacent faces
            Idx fid1 = itr->face()->id();
            Idx fid2 = itr->opposite()->face()->id();
            PairwiseFeature P;
            meshes[category][id].PMap.insert(make_pair(make_pair(fid1,fid2), P));
            F.push_back(make_pair(fid1, fid2));
        }
        
        double minA = 0.0, maxA = 0.0, avgA = 0.0, devA = 0.0;
        calcPairwiseDihedralAngles(meshes[category][id],F,angle,minA, maxA, avgA,devA);
        Idx numPairwise = F.size(); // num of pair wise features
        cout << category << " : MESH # " << id << " : #PAIR  : " << numPairwise << endl;
        cout << category << " : MESH # " << id << " : MINA   : " << minA << endl;
        cout << category << " : MESH # " << id << " : MAXA   : " << maxA << endl;
        cout << category << " : MESH # " << id << " : AVGA   : " << avgA << endl;
        cout << category << " : MESH # " << id << " : DEVA   : " << devA << endl;

        string fnamePairwise = meshes[category][id].off;
        string oname2Pairwise = fnamePairwise.substr(fnamePairwise.find_last_of(filesep)+1);
        string onamePairwise = OutputDir + filesep + category + filesep + "dihedral_" + oname2Pairwise;

        cout << "Saving mesh "<< id << "for visualization ... " << flush;
        showPairwiseFeatures(meshes[category][id],onamePairwise,minA,maxA);
        cout << " done " << endl;

        for (Idx i = 0 ; i < numPairwise; i++) {
            FeatureID ftid;
            ftid.category = category;
            ftid.meshNum = id;
            ftid.ftype = PAIRWISE;
            ftid.ftidx = CatStartNum[category];
            
            // cout << "(" << F[i].first << "," << F[i].second << ")" << " : " << angle[i] << endl;
            // cout << "(" << msh.part[F[i].first] << "," << msh.part[F[i].second] << ")" << " : " << angle[i] << endl;

            //data.num.insert(make_pair(ftid, F[i]));
            data.id.insert(make_pair(F[i], ftid));
            double lbl = ((meshes[category][id].part[F[i].first] != meshes[category][id].part[F[i].second]) ? 1 : 2);
            data.C.push_back( lbl );
            data.pairWiseIds.push_back(F[i]);

            for (int j = 0 ; j < dimPairwiseUnit ; j++) {
                for (int k = 1 ; k < dimPairwise / dimPairwiseUnit ; k++ ) {
                    meshes[category][id].PMap[F[i]].dataVec(k*dimPairwiseUnit+j) = pow(meshes[category][id].PMap[F[i]].dataVec(j) ,double(k+1));
                }
            }

            data.X.push_back( meshes[category][id].PMap[F[i]].dataVec);
            //meshes[category][id].PMap[make_pair(F[i].second, F[i].first)].dataVec = dataVec;
        }
    }
}

void calcPairwiseFeatures2(Meshes_t &meshes, const string &category, vector<int> &Ids, map<string,int>& CatStartNum, data_t &data)
{

    vector <pair<Idx, Idx> > F;

    BOOST_FOREACH(int id, Ids) {

        F.clear();

        cout << "Mesh Id: " << id <<endl;
        Edge_iterator edgesBegin = meshes[category][id].M.edges_begin();
        Edge_iterator edgesEnd = meshes[category][id].M.edges_end();
        int numPairwise = 0;

        for (Edge_iterator itr = edgesBegin; itr != edgesEnd; ++itr) { // for each edge
            // get two adjacent faces
            Idx fid1 = itr->face()->id();
            Idx fid2 = itr->opposite()->face()->id();
            F.push_back(make_pair(fid1, fid2));
            numPairwise++;
        }

        cout << "#Edges: " << numPairwise << endl;
        double sum1 = 0.0, sumsq1 = 0.0, min1 = LARGE, max1 = SMALL, avg1 = 0.0, dev1 = 0.0;
        double sum2 = 0.0, sumsq2 = 0.0, min2 = LARGE, max2 = SMALL, avg2 = 0.0, dev2 = 0.0;

        for (Idx i = 0 ; i < (Idx)(numPairwise); i++) {

            FeatureID ftid;
            ftid.category = category;
            ftid.meshNum = id;
            ftid.ftype = PAIRWISE;
            ftid.ftidx = CatStartNum[category];

            // cout << "(" << F[i].first << "," << F[i].second << ")" << " : " << angle[i] << endl;
            // cout << "(" << msh.part[F[i].first] << "," << msh.part[F[i].second] << ")" << " : " << angle[i] << endl;

            //data.num.insert(make_pair(ftid, F[i]));
            //cout << "(" << F[i].first << "," << F[i].second << ")" << " : " << meshes[category][id].PMap[F[i]].dataVec[0] <<  endl;
            data.id.insert(make_pair(F[i], ftid));
            //double lbl = ((meshes[category][id].part[F[i].first] != meshes[category][id].part[F[i].second]) ? 1 : 2);
            double lbl = ((meshes[category][id].PMap[F[i]].dataVec[0] < 0.010) ? 1 : 2);
            data.C.push_back( lbl );
            data.pairWiseIds.push_back(F[i]);

            data.X.push_back( meshes[category][id].PMap[F[i]].dataVec);

            if (meshes[category][id].part[F[i].first] != meshes[category][id].part[F[i].second]) {
                sum1 += meshes[category][id].PMap[F[i]].dataVec[0];
                sumsq1 += (meshes[category][id].PMap[F[i]].dataVec[0] * meshes[category][id].PMap[F[i]].dataVec[0]);
                if (meshes[category][id].PMap[F[i]].dataVec[0] < min1) {
                    min1 = meshes[category][id].PMap[F[i]].dataVec[0];
                }
                if (meshes[category][id].PMap[F[i]].dataVec[0] > max1) {
                    if (meshes[category][id].PMap[F[i]].dataVec[0] < 1)
                        max1 = meshes[category][id].PMap[F[i]].dataVec[0];
                }
            } else {
                sum2 += meshes[category][id].PMap[F[i]].dataVec[0];
                sumsq2 += (meshes[category][id].PMap[F[i]].dataVec[0] * meshes[category][id].PMap[F[i]].dataVec[0]);
                if (meshes[category][id].PMap[F[i]].dataVec[0] < min2) {
                    min2 = meshes[category][id].PMap[F[i]].dataVec[0];
                }
                if (meshes[category][id].PMap[F[i]].dataVec[0] > max2) {
                    if (meshes[category][id].PMap[F[i]].dataVec[0] < 1)
                        max2 = meshes[category][id].PMap[F[i]].dataVec[0];
                }
            }
        }

        avg1 = sum1 / double(numPairwise);
        avg2 = sum2 / double(numPairwise);
        dev1 = sqrt(((sumsq1 / double(numPairwise)) - avg1 * avg1 ));
        dev2 = sqrt(((sumsq2 / double(numPairwise)) - avg2 * avg2 ));
        cout << "AVG1: " << avg1 << endl;
        cout << "AVG2: " << avg2 << endl;
        cout << "DEV1: " << dev1 << endl;
        cout << "DEV2: " << dev2 << endl;
        cout << "MIN1: " << min1 << endl;
        cout << "MIN2: " << min2 << endl;
        cout << "MAX1: " << max1 << endl;
        cout << "MAX2: " << max2 << endl;
    }
    cout << "End of calcPairwise 2" << endl;
}



void showUnaryFeatures(MyMesh &msh, const string &onameUnary, const double minf, const double maxf)
{

    Facet_iterator fBegin = msh.M.facets_begin();
    Facet_iterator fEnd = msh.M.facets_end();

    // Iterate over edges
    for (Facet_iterator itr = fBegin; itr != fEnd; ++itr) { // for each edge
        // get two adjacent faces

        Idx fid = itr->id();

        double fval = 0;
        if (msh.UMap.find(fid) == msh.UMap.end()) fval = 1.0;
        else if ((msh.UMap[fid].dataVec[0] <= SMALL) || (msh.UMap[fid].dataVec[0] >= LARGE))
            fval = 1.0;
        else fval = msh.UMap[fid].dataVec[0];
        //cout << "fval: " << fval << endl;
        msh.UMap[fid].R = ((maxf == minf) ? 0.0 : (fval-minf)/(maxf-minf));
        msh.UMap[fid].G = 0.0;
        msh.UMap[fid].B = 0.0;
        msh.UMap[fid].A = 1;
    }

    CGALPolyhedron2OFF::MyMesh2OFF(msh,onameUnary);
}



void showPairwiseFeatures(MyMesh &msh, const string &onamePairwise, const double minf, const double maxf)
{
    
    Edge_iterator eBegin = msh.M.edges_begin();
    Edge_iterator eEnd = msh.M.edges_end();
    
    // Iterate over edges
    for (Edge_iterator itr = eBegin; itr != eEnd; ++itr) { // for each edge
        // get two adjacent faces
        Idx fid1 = itr->face()->id();
        Idx fid2 = itr->opposite()->face()->id();
        
        pair<Idx,Idx> pr = make_pair(fid1,fid2);
        
        // plot the feature
        
        //cout << " PAIR: " << pr.first << " " << pr.second << endl;
        double fval = 0;
        if (msh.PMap.find(pr) == msh.PMap.end()) {
            fval = 1.0;
            cout << "missing: " << pr.first << " " << pr.second << endl;
            continue;
        } else {
            fval = (msh.PMap[pr].dataVec(0) - minf) / (maxf - minf);
            if (fval < 0.010)
                fval = 1.0;
            else
                fval = 0.0;

        }
        //else fval = (msh.PMap[pr].dihedralAngleFeature - minf) / (maxf - minf);
        //cout << "fval: " << fval << endl;
        msh.UMap[fid1].R = fval;
        msh.UMap[fid1].G = 0.0;
        msh.UMap[fid1].B = 0.0;
        msh.UMap[fid1].A = 1;
        msh.UMap[fid2].R = fval;
        msh.UMap[fid2].G = 0.0;
        msh.UMap[fid2].B = 0.0;
        msh.UMap[fid2].A = 1;
    }
    
    CGALPolyhedron2OFF::MyMesh2OFF(msh,onamePairwise);
}


void calcFeatures(Meshes_t &M, const string &category, vector<int> &Ids, map<string,int>& CatStartNum, data_t &data)
{
    calcUnaryFeatures(M, category, Ids, CatStartNum, data);
    calcPairwiseFeatures(M, category, Ids, CatStartNum, data);
}



void averageGeodesicDistance(MyMesh &M, double &minagd, double &maxagd, double & avgagd, double &devagd)
{
#if 0
    int numParts = int(M.partpts.size());
    cout << "numParts : " << numParts << endl;
    maxagd = -1.0 * (numeric_limits<double>::max() -1);

    BOOST_FOREACH(partpoints_t::value_type part, M.partpts) {
        cout << part.first << " "  << part.second.size() << endl;
        int N = int(part.second.size());
        for (int i = 0 ; i < N ; i++) {
            double agd = 0.0;
            for (int j = 0 ; j < N ; j++) {
                double d = M.dist[M.faceNum[part.second[i]]][M.faceNum[part.second[j]]];
                agd += d;
            }
            agd /= double(N);
            if (agd > maxagd) {
                maxagd = agd;
            }
        }
    }


    BOOST_FOREACH(partpoints_t::value_type part, M.partpts) {
        cout << part.first << " "  << part.second.size() << endl;
        int N = int(part.second.size());
        for (int i = 0 ; i < N ; i++) {
            double agd = 0.0;
            for (int j = 0 ; j < N ; j++) {
                double d = M.dist[M.faceNum[part.second[i]]][M.faceNum[part.second[j]]];
                agd += d;
            }
            agd /= double(N);
            agd /= maxagd;
            //cout << agd << " ";

            Idx id = part.second[i];
            boost::numeric::ublas::vector<double> dataVec(dimUnary);
            for (int k = 0 ; k < dimUnary ; k++) {
                dataVec(k) = pow(double(agd),double(k+1));
            }
            M.UMap[id].dataVec = dataVec;
        }

        //cout << endl;
    }
#endif

    int N = int(M.M.size_of_facets());
    double *agd = new double[N];

    for (int i = 0 ; i < N ; i++) { // for each face
        agd[i] = 0.0;

        vector<double> P;
        P.clear();

        for (int j = 0 ; j < N ; j++) {
            double d = M.dist[M.faceNum[i]][M.faceNum[j]];
            agd[i] += d;
            M.avgGeodesicDist += d;
            P.push_back(d);
        }
        agd[i] /= double(N);  // average
        //        //cout << agd[i] << " ";

        // Initialize
        boost::numeric::ublas::vector<double> dataVec(dimUnary);
        M.UMap[M.faceID[i]].dataVec = dataVec;
        // set values
        for (int l = 0 ; l < 9 ; l++) {
            int k  = int((l+1) * 10.0 * N / 100.0);
            vector<double>::iterator nth = P.begin() + k;
            nth_element(P.begin(),nth,P.end());   // Percentile
            M.UMap[M.faceID[i]].dataVec(l+2) = *nth;
        }

    }

    M.avgGeodesicDist /= double(N*N*2.0);

    minagd = numeric_limits<double>::max();
    for (int i = 0 ; i < N; i++) {
        if ( agd[i] < minagd ) {
            minagd = agd[i];
        }
    }

    for (int i = 0 ; i < N; i++) {
        agd[i] /= minagd;
    }

    maxagd = numeric_limits<double>::min();
    minagd = numeric_limits<double>::max();
    double sumagd = 0.0;
    double sumsqagd = 0.0;
    for (int i = 0 ; i < N; i++) {

        if ( agd[i] > maxagd ) {
            maxagd = agd[i];
        }
        if ( agd[i] < minagd ) {
            minagd = agd[i];
        }
        sumagd += agd[i];
        sumsqagd += (agd[i]*agd[i]);
    }

    avgagd = sumagd / double(N);
    devagd = sqrt( ((sumsqagd / double(N)) - avgagd * avgagd) );

#if 1

    // Adjust AGD values, as per definition
    for (int i = 0 ; i < N ; i++) {

        M.UMap[M.faceID[i]].dataVec(0) = agd[i]; // already divided by min
        M.UMap[M.faceID[i]].dataVec(1) = devagd * devagd;  // same here
        for (int l = 0 ; l < 9 ; l++) {
            M.UMap[M.faceID[i]].dataVec(l+2) = M.UMap[M.faceID[i]].dataVec(l+2) / minagd;
            // Note: AGD is a ratio where denominator is min agd  [as per Hilaga paper ]
        }
    }
#endif

#if 0
    for (int i = 0; i < N ; i++) {
        boost::numeric::ublas::vector<double> dataVec(dimUnary);
        for (int k = 0 ; k < dimUnary ; k++) {
            dataVec(k) = pow(double(agd[i]),double(k+1));
            //dataVec(k) = pow(double(agd[i]),1);
        }
        M.UMap[M.faceID[i]].dataVec = dataVec;
    }
#endif


    // Stats

    double *maxagdarr = new double[dimUnary];
    double *minagdarr = new double[dimUnary];
    double *sumagdarr = new double[dimUnary];
    double *sumsqagdarr = new double[dimUnary];
    double *avgagdarr = new double[dimUnary];
    double *devagdarr = new double[dimUnary];

    for (int k = 0 ; k < dimUnary; k++) {
        maxagdarr[k] = numeric_limits<double>::min();
        minagdarr[k] = numeric_limits<double>::max();
        sumagdarr[k] = 0.0;
        sumsqagdarr[k] = 0.0;


        for (int i = 0 ; i < N; i++) {

            if ( M.UMap[M.faceID[i]].dataVec(k) > maxagdarr[k] ) {
                maxagdarr[k] = M.UMap[M.faceID[i]].dataVec(k);
            }
            if ( M.UMap[M.faceID[i]].dataVec(k) < minagdarr[k] ) {
                minagdarr[k] = M.UMap[M.faceID[i]].dataVec(k);
            }
            sumagdarr[k] += M.UMap[M.faceID[i]].dataVec(k);
            sumsqagdarr[k] += M.UMap[M.faceID[i]].dataVec(k);
        }

        avgagdarr[k] = sumagdarr[k] / double(N);
        devagdarr[k] = sqrt( ((sumsqagdarr[k] / double(N)) - avgagdarr[k] * avgagdarr[k]) );

    }

    delete [] agd;

    //cout << endl;

    //showUnaryFeatures(M,"/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/output/agd.off");
    //exit(0);

    normalizeUnaryFeatures(M, minagdarr, maxagdarr);

    delete [] maxagdarr;
    delete [] minagdarr;
    delete [] sumagdarr;
    delete [] sumsqagdarr;
    delete [] avgagdarr;
    delete [] devagdarr;

}

void normalizeUnaryFeatures(MyMesh &M, const double minf[], const double maxf[])
{
    int N = int(M.M.size_of_facets());
    for (int i = 0; i < N ; i++) {
        for (int k = 0 ; k < dimUnary ; k++) {
            M.UMap[M.faceID[i]].dataVec(k) = (M.UMap[M.faceID[i]].dataVec[k] - minf[k]) / (maxf[k] -  minf[k]) ;
        }
    }
}

void calcMeadianGeodesicDistance(MyMesh &msh, double &medianDist)
{

#if 0
    cout << "src exists => " << std::boolalpha << exists(msh.dump.c_str()) << endl;
    cout << "dest exists => " << std::boolalpha << exists(msh.wdump.c_str()) << endl;
    try {
        if (exists(msh.wdump.c_str())) {
            remove(msh.wdump.c_str());
        }
        copy_file(msh.dump.c_str(),msh.wdump.c_str());
    } catch (const boost::filesystem::filesystem_error& e) {
        std::cerr << "File Copy Error: " << e.what() << std::endl;
    }
#endif

    // Make a copy of the file
    const int BUF_SIZE = 4096;
    ifstream in(msh.dump.c_str(), ios_base::in | ios_base::binary);
    if (!in) {
        cerr << "Could not open file " << msh.dump.c_str() << endl;
        return;
    }

    std::ofstream out(msh.wdump.c_str(), ios_base::out | ios_base::binary | ios_base::trunc);
    if (!out) {
        cerr << "Could not open file " << msh.wdump.c_str() << endl;
        return;
    }

    char buf[BUF_SIZE];

    do {
        in.read(&buf[0], BUF_SIZE);
        out.write(&buf[0], in.gcount( ));
    } while (in.gcount( ) > 0);

    in.close( );
    out.close( );

    // Now mmap the copy

    msh.wfd = open(msh.wdump.c_str(), O_RDWR);
    if (msh.wfd < 0) {
        perror("Could not open file");
        return;
    }

    Idx numFaces = (Idx)(msh.M.size_of_facets());
    size_t len = (size_t)(sizeof(Idx)+numFaces*numFaces*sizeof(double));
    off_t off = sizeof(Idx);
    msh.waddr = mmap(0, len, PROT_READ|PROT_WRITE, MAP_SHARED, msh.wfd, 0);

    if (msh.waddr == MAP_FAILED) {
        perror("Memory map failed");
        return;
    }

    msh.wdst = (reinterpret_cast<double *>((char *)msh.waddr + off));
    msh.wdist = new double*[numFaces];
    msh.wdist[0] = msh.wdst;
    for(int i = 1 ; i < int(numFaces) ; i++) {
        msh.wdist[i] = msh.wdist[0] + i * numFaces;
    }

    cout << "Memory mapped " << msh.wdump.c_str() << " at: " << hex << msh.waddr
         << dec << " size " << sizeof(Idx) << " + " << numFaces*numFaces*sizeof(double)
         << " bytes" << endl;


    nth_element(msh.wdst, msh.wdst + ((numFaces*numFaces)/2), msh.wdst+ (numFaces * numFaces));
    medianDist = *(msh.wdst + ((numFaces*numFaces)/2));

    //nth_element(msh.wdst, msh.wdst + numFaces + (numFaces*(numFaces -1))/2, msh.wdst+ (numFaces * numFaces));
    //medianDist = *(msh.wdst + numFaces + (numFaces*(numFaces -1))/2);
    cout << "Median Distance:" << medianDist << endl;
    msh.medianGeodesicDist = medianDist;

    munmap((void *)(msh.waddr), sizeof(Idx) + numFaces * numFaces * sizeof(double));
    delete [] msh.wdist;
    close(msh.wfd);

}
