//
//  main.cpp
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 01/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include <iostream>
//#include "CHull.h"
//#include "hds1.h"
//#include "hds2.h"
//#include "testPoly2OFF.h"
//#include "ReadDataset.h"
//#include "testDihedral.h"
//#include "testDual.h"
//#include "testIncrementalBuilder.h"
//#include "testGraph.h"
//#include "run.h"
//#include "testColor.h"
//#include "testSDF.h"
//#include "testIntersection.h"
#include "runSDF.h"
//#include "runShapeContext3D.h"
//#include "runCurvature.h"
//#include "testCurvature.h"
//#include "Curvature.h"

using namespace std;

int main(int argc, const char * argv[])
{
    try {

        // insert code here...
        std::cout << "Hello, World!\n";
        //chull();
        //hds1();
        //hds2();
        //testPoly2OFF();
        //processFiles(241,260);
        //processFiles(241, 241);
        //cout << "done" << endl;
        //testDihedral();
        //testDual();
        //testIncrementalBuilder();
        //testGraph();
        //testColor();
        //testSDF();
        //testIntersection();

        //testCurvature();

        //run();


        //runCurvature("Octopus");
        //runCurvature("Vase");

        //runShapeContext3D("Octopus");


        runSDF("Octopus");
        //runSDF("Human");
        //runSDF("Ant");
        //runSDF("Airplane");
        //runSDF("Bust");
        //runSDF("Mech");
        //runSDF("Vase");
        //runSDF("Bird");
        //runSDF("Plier");
        //runSDF("Fish");
        //runSDF("Hand");
        //runSDF("Table");
        //runSDF("Chair");
        //runSDF("Bearing");
        //runSDF("Armadillo");

    }
    catch (std::exception &e)
    {
        cerr    << e.what() << endl;
    }

    cout << "completed" << endl;
    return 0;
}

