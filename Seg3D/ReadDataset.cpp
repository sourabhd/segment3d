//
//  ReadDataset.cpp
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 03/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include "ReadDataset.h"
#include "common.h"
#include "Graph.h"
#include "Dijkstra.h"
#include "CGALPolyhedron2OFF.h"


#include <iostream>
#include <sstream>
#include <cstdlib>
#include <iomanip>

using namespace std;
using namespace CGAL;
using namespace boost;
using namespace boost::filesystem;

void readDataset(const string &category, const int x, const int y,
                 tr1::unordered_map<string, MeshCategory> &Meshes,
                 tr1::unordered_map<string,int> &CatNumLabels)
{
    const string datadir = InputDir + filesep + category;
    const string outdir = OutputDir + filesep + category;
    
    create_directories(path(outdir));
    
    MeshCategory MC;
    MC.clear();
    int mxLbls = 1;
    for (int i = x ; i <= y ; i++ ) {
        MyMesh msh;
        stringstream s1;
        s1 << datadir << filesep << i << ".off";
        stringstream s2;
        s2 << datadir << filesep << i << "_" << "labels" << ".txt";
        stringstream s3;
        s3 << outdir << filesep << i << "_out" << ".off";
        stringstream s4;
        s4 << outdir << filesep << i << ".txt";
        stringstream s5;
        s5 << outdir << filesep << i  << "_wdump" << ".txt";
        stringstream s6;
        s6 << outdir << filesep << "unary_"<< i << ".off";
        stringstream s7;
        s7 << outdir << filesep << "pairwise_" << i << ".off";

        string offFile = s1.str();
        string labelsFile = s2.str();
        string outFile = s3.str();
        string dumpFile = s4.str();
        string wdumpFile = s5.str();
        string uoffFile = s6.str();
        string poffFile = s7.str();
        
        cout << offFile << endl;
        cout << labelsFile << endl;
        
        msh.off = offFile;
        msh.lbl = labelsFile;
        msh.dump = dumpFile;
        msh.wdump = wdumpFile;
        msh.uoff = uoffFile;
        msh.poff = poffFile;
        
        // Read the OFF file and convert to polyhedron
        CGALPolyhedron2OFF::OFF2Poly(msh.M, msh.off);
        
        // Read the labels file and convert to labels data structure
        CGALPolyhedron2OFF::Labels2Parts(msh.part, msh.partpts, msh.lbl);
        cout << msh.part.size() << " " << msh.partpts.size() << endl;
        if (int(msh.partpts.size()) > mxLbls) {
            mxLbls = int(msh.partpts.size());
        }
        
        //calcPairwiseDihedralAngles(P);
        
        MC.push_back(msh);
        Meshes[category] = MC;
        
        // Uncomment for testing
        //CGALPolyhedron2OFF::poly2OFF(msh.M, outFile);
        
    }

    if (CatNumLabels.find(category) == CatNumLabels.end())
        CatNumLabels.insert(make_pair(category,mxLbls));
    else
        CatNumLabels[category] = mxLbls;

    
}

void saveMyMesh(MyMesh &msh)
{
    std::ofstream ofs(msh.dump.c_str(), ios::binary);
    Idx numFaces = (Idx)(msh.M.size_of_facets());
    ofs.write((char *)&numFaces,sizeof(Idx));

    for (int i = 0 ; i < int(numFaces) ; i++) {
        //for (int j = 0 ; j <= i ; j++) {
        for (int j = 0 ; j < int(numFaces) ; j++) {
            ofs.write((char *)&msh.dist[i][j],sizeof(double));
        }
    }

    BOOST_FOREACH(IdxMap_t::value_type f, msh.faceID) {
        ofs.write((char *)&f.first,sizeof(Idx));
        ofs.write((char *)&f.second,sizeof(Idx));
    }

    BOOST_FOREACH(IdxMap_t::value_type f, msh.faceNum) {
        ofs.write((char *)&f.first,sizeof(Idx));
        ofs.write((char *)&f.second,sizeof(Idx));
    }

    BOOST_FOREACH(IdxMap_t::value_type f, msh.vertexID) {
        ofs.write((char *)&f.first,sizeof(Idx));
        ofs.write((char *)&f.second,sizeof(Idx));
    }

    BOOST_FOREACH(IdxMap_t::value_type f, msh.vertexNum) {
        ofs.write((char *)&f.first,sizeof(Idx));
        ofs.write((char *)&f.second,sizeof(Idx));
    }

    ofs.close();
}

#if 0

void loadMyMesh(MyMesh &msh)
{
    std::ifstream ifs(msh.dump.c_str(), ios::binary);
    Idx numFaces = (Idx)(msh.M.size_of_facets());
    Idx numFacesFile;
    ifs.read((char *)&numFacesFile,sizeof(Idx));
    assert(numFaces == numFacesFile);

    msh.dist = malloc2D<double>(numFaces,numFaces);
    memset(msh.dist[0],0,numFaces*numFaces*sizeof(double));

    for (int i = 0 ; i < int(numFaces) ; i++) {
        for (int j = 0 ; j <= i ; j++) {
            double d = 0.0;
            ifs.read((char *)&d,sizeof(double));
            msh.dist[i][j] = d;
            msh.dist[j][i] = d;
        }
    }

    msh.faceID.clear();
    for (int i = 0 ; i < int(numFaces) ; i++) {
        Idx k = 0, v = 0;
        ifs.read((char *)&k,sizeof(Idx));
        ifs.read((char *)&v,sizeof(Idx));
        msh.faceID.insert(make_pair(k,v));
    }

    msh.faceNum.clear();
    for (int i = 0 ; i < int(numFaces) ; i++) {
        Idx k = 0, v = 0;
        ifs.read((char *)&k,sizeof(Idx));
        ifs.read((char *)&v,sizeof(Idx));
        msh.faceNum.insert(make_pair(k,v));
    }

    msh.vertexID.clear();
    for (int i = 0 ; i < int(numFaces) ; i++) {
        Idx k = 0, v = 0;
        ifs.read((char *)&k,sizeof(Idx));
        ifs.read((char *)&v,sizeof(Idx));
        msh.vertexID.insert(make_pair(k,v));
    }

    msh.vertexNum.clear();
    for (int i = 0 ; i < int(numFaces) ; i++) {
        Idx k = 0, v = 0;
        ifs.read((char *)&k,sizeof(Idx));
        ifs.read((char *)&v,sizeof(Idx));
        msh.vertexNum.insert(make_pair(k,v));
    }

    ifs.close();
}

#endif

/**
 * @brief loadMyMesh
 * @param msh
 *
 * Memory mapped version
 */

void loadMyMesh(MyMesh &msh)
{
    std::ifstream ifs(msh.dump.c_str(), ios::binary);
    Idx numFaces = (Idx)(msh.M.size_of_facets());
    Idx numFacesFile;
    ifs.read((char *)&numFacesFile,sizeof(Idx));
    assert(numFaces == numFacesFile);
    msh.numFaces = numFaces;
    cout << "NumFaces : " << numFacesFile << endl;

    ifs.seekg(numFacesFile*numFacesFile*sizeof(double),ios::cur);

    msh.faceID.clear();
    for (int i = 0 ; i < int(numFaces) ; i++) {
        Idx k = 0, v = 0;
        ifs.read((char *)&k,sizeof(Idx));
        ifs.read((char *)&v,sizeof(Idx));
        msh.faceID.insert(make_pair(k,v));
    }

    msh.faceNum.clear();
    for (int i = 0 ; i < int(numFaces) ; i++) {
        Idx k = 0, v = 0;
        ifs.read((char *)&k,sizeof(Idx));
        ifs.read((char *)&v,sizeof(Idx));
        msh.faceNum.insert(make_pair(k,v));
    }

    msh.vertexID.clear();
    for (int i = 0 ; i < int(numFaces) ; i++) {
        Idx k = 0, v = 0;
        ifs.read((char *)&k,sizeof(Idx));
        ifs.read((char *)&v,sizeof(Idx));
        msh.vertexID.insert(make_pair(k,v));
    }

    msh.vertexNum.clear();
    for (int i = 0 ; i < int(numFaces) ; i++) {
        Idx k = 0, v = 0;
        ifs.read((char *)&k,sizeof(Idx));
        ifs.read((char *)&v,sizeof(Idx));
        msh.vertexNum.insert(make_pair(k,v));
    }

    ifs.close();

    msh.fd = open(msh.dump.c_str(), O_RDONLY);
    if (msh.fd < 0) {
        perror("Could not open file");
        return;
     }

    size_t len = (size_t)(sizeof(Idx)+numFaces*numFaces*sizeof(double));
    off_t off = sizeof(Idx);
    msh.addr = mmap(0, len, PROT_READ, MAP_SHARED, msh.fd, 0);

     if (msh.addr == MAP_FAILED) {
         perror("Memory map failed");
         return;
     }

     msh.dst = (reinterpret_cast<double *>((char *)msh.addr + off));
     msh.dist = new double*[numFaces];
     msh.dist[0] = msh.dst;
     for(int i = 1 ; i < int(numFaces) ; i++) {
         msh.dist[i] = msh.dist[0] + i * numFaces;
     }

     cout << "Memory mapped " << msh.dump.c_str() << " at: " << hex << msh.addr
          << dec << " size " << sizeof(Idx) << " + " << numFaces*numFaces*sizeof(double)
          << " bytes" << endl;
#if 0
     for (int i = 0 ; i < 10 ; i++) {
         for (int j = 0 ; j < 10 ; j++) {
            cout << msh.dist[i][j] << endl;
         }
     }
     cout << "Outside loop" << endl;
#endif
}
