//
//  common.h
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 03/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef HelloCGAL_common_h
#define HelloCGAL_common_h


#include <fstream>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <queue>
#include <algorithm>
#include <iterator>
#include <map>
#include <string>
#include <utility>
#include <tr1/unordered_map>
#include <tr1/unordered_set>
#include <functional>
#include <limits>
#include <cmath>
#include <ctime>
#include <cassert>
#include <exception>
#include <omp.h>

extern "C" {
#include <fcntl.h>
#include <sys/mman.h>
}

// CGAL
#include <CGAL/Plane_3.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_items_with_id_3.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/boost/graph/properties_Polyhedron_3.h>
#include <CGAL/Polyhedron_traits_with_normals_3.h>
#include <CGAL/function_objects.h>
#include <CGAL/circulator.h>
#include <CGAL/centroid.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/IO/Polyhedron_geomview_ostream.h>
#include <CGAL/squared_distance_3.h>
#include <CGAL/Gmpq.h>
#include <CGAL/IO/Color.h>
#include <CGAL/Ray_3.h>
#include <CGAL/Direction_3.h>

// Boost
#include <boost/config.hpp>
#include <boost/foreach.hpp>
#include <boost/assign.hpp>
#include <boost/filesystem.hpp>
#include <boost/variant.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/functional/hash.hpp>
#include <boost/tuple/tuple.hpp>



// Boost graph library
#include <boost/property_map/property_map.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/johnson_all_pairs_shortest.hpp>

// Eigen matrix library
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Eigenvalues>

using namespace std;
using namespace CGAL;
using namespace boost;
using namespace Eigen;


// Convenience typedef's, the data types are too compicated to embed in the logic


#if 0
template <class Refs>
struct My_face : public CGAL::HalfedgeDS_face_base<Refs> {
    CGAL::Color color;
};
struct My_items : public CGAL::Polyhedron_items_with_id_3 {
    template <class Refs, class Traits>
    struct Face_wrapper {
        typedef My_face<Refs> Face;
    };
};
#endif


typedef CGAL::Simple_cartesian<double>    Kernel;
typedef CGAL::Polyhedron_traits_with_normals_3<Kernel> Traits;
typedef Kernel::Point_3                   Point;
typedef Kernel::Vector_3                  Vector;
typedef Kernel::Ray_3                     Ray;
typedef Kernel::Direction_3               Direction;
typedef CGAL::Polyhedron_3<Traits,CGAL::Polyhedron_items_with_id_3, CGAL::HalfedgeDS_default > Polyhedron;
//typedef CGAL::Polyhedron_3<Traits,My_items> Polyhedron;
typedef Kernel::Plane_3                   Plane;
typedef std::vector<Vector>               Poly;


typedef Polyhedron::Edge_iterator         Edge_iterator;
typedef Polyhedron::Vertex_iterator       Vertex_iterator;
typedef Polyhedron::Halfedge_handle       Halfedge_handle;
typedef Polyhedron::Facet_handle          Facet_handle;
typedef Polyhedron::Vertex_handle         Vertex_handle;
typedef Polyhedron::Facet_iterator        Facet_iterator;
typedef Polyhedron::Halfedge_around_facet_circulator Halfedge_facet_circulator;
typedef Polyhedron::Halfedge_around_vertex_circulator Halfedge_vertex_circulator;

typedef Polyhedron::Facet                  Facet;
typedef Polyhedron::Vertex                 Vertex;

typedef Polyhedron::HalfedgeDS             HalfedgeDS;
typedef CGAL::Polyhedron_incremental_builder_3<HalfedgeDS> Builder;

typedef CGAL::Gmpq                        NT;

//typedef unsigned long int Idx;
typedef size_t Idx;
typedef std::pair<Idx,Idx> PairIdx;

typedef std::vector<Point> VecPoint;
typedef std::pair<Point,Point> PairPoint;
typedef std::vector<VecPoint> VecVecPoint;
typedef std::pair<VecPoint,VecPoint> PairVecPoint;
typedef std::vector<PairVecPoint> VecPairVecPoint;
typedef std::vector<PairPoint> VecPairPoint;

typedef Point Vec;

// Label Point mapping
typedef tr1::unordered_map<std::string, std::vector<Idx> > partpoints_t;
typedef tr1::unordered_map<Idx, std::string> part_t;

// ID point mapping
typedef tr1::unordered_map<Idx, Point> point_id_t;
typedef tr1::unordered_map<Idx, Vertex> vertex_id_t;
typedef tr1::unordered_map<Idx, bool> bitmap_t;

typedef tr1::unordered_map<Idx,Vertex> VMap;

typedef std::vector<Idx> VecIdx;
typedef std::vector<VecIdx> VecVecIdx;

typedef unsigned long int label_t;
typedef  double weight_t;

typedef tr1::unordered_set<unsigned long long> facever_t;
typedef vector<facever_t> faceid_t;
typedef vector<facever_t> faceid_itr_t;

typedef double (*EdgeFn)(Vertex v1,Vertex v2);


// Dataset
const string InputDir = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/Paper/LabeledDB_small/";
//const string OutputDir = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/output";
//const string OutputDir = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/output_c";
//const string OutputDir = "/Volumes/WD/Seg3D/output";
const string OutputDir = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/output_n";
const string JointBoostClassifierDir = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/Seg3D/Seg3D/jointboost";
const string Categories[] = {
    "Airplane",
    "Ant",
    "Armadillo",
    "Bearing",
    "Bird",
    "Bust",
    "Chair",
    "Cup",
    "Fish",
    "FourLeg",
    "Glasses",
    "Hand",
    "Human",
    "Mech",
    "Octopus",
    "Plier",
    "Table",
    "Teddy",
    "Vase"
};
const int NumCategories = 19;


//////////////////////// BOOST GRAPH ///////////////////////////////////////

typedef std::pair<Idx,Idx> BGLEdge;
typedef property<edge_weight_t, double> EdgeWeightProperty;
typedef adjacency_list<vecS,vecS,directedS,no_property,EdgeWeightProperty> BGLGraph;
typedef graph_traits<BGLGraph >::edge_iterator BGLEdgeIterator;
typedef property_map<BGLGraph, edge_weight_t>::type BGLEdgeWeight;


////////////////////////////////////////////////////////////////////////////

extern map<string,int> CategoryID;
extern map<string,int> CategoryFileStartNum;
extern map<string,int> CategoryFileEndNum;


enum featureType_t { UNARY, PAIRWISE };
struct pairst
{
    Idx first;
    Idx second;
};

typedef boost::variant<Idx, pair<Idx,Idx> > feature_t;
//typedef boost::variant<Idx, pairst > feature_t;

struct FeatureID
{
    Idx ftidx;
    string category;
    string part;      /* label */
    int meshNum;
    featureType_t ftype;
    
    bool operator()(const FeatureID &A, const FeatureID &B)
    {
        return ((A.ftidx < B.ftidx) || ((A.ftidx == B.ftidx) && (A.meshNum < B.meshNum)));
    }
};


struct UnaryFeature
{
    Idx assocFace;
    double R;
    double G;
    double B;
    double A;
    double distFeature;
    Idx groundTruthLabel;
    string groundTruthLabelStr;
    boost::numeric::ublas::vector<double> dataVec;
    FeatureID fdetails;
    boost::numeric::ublas::vector<double> probFeatureVector;
    double maxProb;
    int argmaxProb;
    string label;
    int predLabel;
    
    UnaryFeature():assocFace(0),R(1.0),G(0.0),B(0.0),A(1.0){}
};

struct PairwiseFeature
{
    PairIdx assocPair;
    double dihedralAngleFeature;
    boost::numeric::ublas::vector<double> dataVec;
    FeatureID fdetails; 
    boost::numeric::ublas::vector<double> probFeatureVector;
    double maxProb;
    int argmaxProb;
    string label;
};

// Feature maps

struct MyPair
{
    Idx first;
    Idx second;
};

typedef tr1::unordered_map<Idx,UnaryFeature> UnaryFeatureMap_t;
typedef tr1::unordered_map<PairIdx,PairwiseFeature,boost::hash<PairIdx> > PairwiseFeatureMap_t;
typedef tr1::unordered_map<Idx,Idx> IdxMap_t;

struct MyMesh
{
    friend class boost::serialization::access;

    Polyhedron M;                           /* Polyhedron mesh data structure    */
    string off;                             /* OFF filename                      */
    string uoff;                            /* OFF filename : unary feature      */
    string poff;                            /* OFF filename : pairwise feature   */
    string dump;                            /* Filename of serialized data       */
    string wdump;                           /* Dump which can be changed         */
    string lbl;                             /* Mesh type eg: bird                */
    part_t part;                            /* Parts and faces within the part   */
    partpoints_t partpts;                   /* Parts and faces within the part   */
    UnaryFeatureMap_t UMap;                 /* Unary features indexed by face id */
    PairwiseFeatureMap_t PMap;              /* Pairwise features by face id      */
    Polyhedron dual;                        /* Dual mesh                         */
    BGLGraph dualG;                         /* Graph of dual mesh                */
    int fd;                                 /* File descriptor                   */
    void *addr;                             /* Start of file address             */
    double **dist;                          /* Memoized distances                */
    double *dst;                            /* Memoized distances                */
    int wfd;                                /* File descriptor (mutable)         */
    void *waddr;                            /* Start of file address (mutable)   */
    double **wdist;                         /* Memoized distances (mutable)      */
    double *wdst;                           /* Memoized distances (mutable)      */
    IdxMap_t faceID;                        /* Faces and CGAL id                 */
    IdxMap_t faceNum;                       /* Faces and index of storage        */
    IdxMap_t vertexID;                      /* Faces and CGAL id                 */
    IdxMap_t vertexNum;                     /* Faces and index of storage        */
    int numFaces;                           /* Number of faces                   */
    double avgGeodesicDist;                 /* Average Geodesic distance         */
    double medianGeodesicDist;              /* Median Geodesic distance          */
};

typedef vector<MyMesh> MeshCategory;
typedef tr1::unordered_map<string,MeshCategory> Meshes_t;

const string filesep = "/";

struct data_t
{
    std::map<FeatureID, feature_t> num;
    std::map<feature_t, FeatureID> id;
    std::vector<int> C;     // label (part)
    std::map<feature_t,Idx> featureNum;
    std::map<Idx,feature_t> feature;
    std::vector<boost::numeric::ublas::vector<double> > X; // data
    std::vector<pair<Idx,Idx> > pairWiseIds;  // intentional redundancy because of the idiosyncracies of boost variant
    std::vector<Idx> unaryIds;
    
    data_t():num(std::map<FeatureID, feature_t>()),id(std::map<feature_t, FeatureID>()),C(std::vector<int>()), X(std::vector<boost::numeric::ublas::vector<double> >()){}
};

    
#define SEG_DEBUG

#if defined(SEG_DEBUG)
#define CERR std::cerr << __FILE__ << ":" << __LINE__ << ":" << __func__ << " :: "
#define COUT std::cout << __func__ << ":" << __LINE__ << ":" << __func__ << " :: "
#else
#define CERR std::cerr
#define COUT std::cout
#endif


const string MatlabRoot="/Applications/MATLAB_R2011b.app";

const int split_x = 4;
const int split_y = 1;

//const int dimUnary = 11;
//const int dimPairwise = 50;
const int dimUnary = 5;
const int dimPairwise = 5;
const int dimPairwiseUnit = 5;
const int dimUnarySDF = 5;
const int dimPairwiseSDF = 5;

const int dimUnarySC = 30;
const int startVecIdxUnarySC = 0;
const int endVecIdxUnarySC = 29;

const int dimUnaryCurv = 9;
const int startVecIdxUnaryCurv = 0;
const int endVecIdxUnaryCurv = 8;
const int dimPairwiseCurv = 16;
const int startVecIdxPairwiseCurv = 0;
const int endVecIdxPairwiseCurv = 15;

const double colorR[] = { 1.0, 0.0, 0.0, 0.0, 1.0};
const double colorG[] = { 0.0, 1.0, 0.0, 0.0, 1.0};
const double colorB[] = { 0.0, 0.0, 1.0, 0.0, 1.0};

const double EPS = 1e-12;
const double LARGE = 1e+17;
//const double SMALL = -1e-17;
const double SMALL = -1e+17;


typedef pair<double, Idx> GeoIdx;
typedef vector<GeoIdx> VGeoIdx;
typedef vector<GeoIdx>::iterator VGeoIdxItr;


/* My Polygon structure */

struct MyPolygon
{
    Vector normal;
    Point centroid;
    int numVertices;
    Plane plane;
    Poly vertex;
};


const int JointBoostNumWeakClassifiers = 10;
const int JointBoostNumThresholds = 10;

#endif
