//
//  Dual.h
//  Segment3D
//
//  Created by Sourabh Daptardar on 26/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __Segment3D__Dual__
#define __Segment3D__Dual__

#include <iostream>
#include "common.h"
#include "CGALPolyhedron2OFF.h"


class Dual : public CGAL::Modifier_base<HalfedgeDS>
{
    Polyhedron mesh;
public:
    Dual(Polyhedron &mesh_):mesh(mesh_){}
    void operator()(HalfedgeDS &hds);
};


class Dualizer
{
    Polyhedron mesh;
public:
    Dualizer(Polyhedron &mesh_):mesh(mesh_){}

    Polyhedron dual();
};

// The API 

Polyhedron dual(Polyhedron &P);

#endif /* defined(__Segment3D__Dual__) */


