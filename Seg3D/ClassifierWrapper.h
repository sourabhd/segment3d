//
//  ClassifierWrapper.h
//  Segment3D
//
//  Created by Sourabh Daptardar on 04/12/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#ifndef __Segment3D__ClassifierWrapper__
#define __Segment3D__ClassifierWrapper__

#include <iostream>
#include "common.h"
#include <engine.h>
#include "feature.h"

using namespace std;

#define MENG_OBUF_SIZE 1024

class ClassifierWrapper
{
public:
    ClassifierWrapper(const std::string &mfilepath, const::string &mfilepath2="" );
    ~ClassifierWrapper();
    static void destroy();
    void* getVarFromMatFile(const string &mtfile, const string &var);
    void runMatlabCmd(const string &cmd);
    void convToMatlab(const string &outdir, Idx meshid, bool isUnary, data_t &trdata, data_t &tedata, Meshes_t &meshes, const string &category,tr1::unordered_map<string,int> &CatNumLabels);
    void put(const string &var, mxArray *val);
    void get(const string &var, void *cresult, size_t &elSize, size_t &nDim, size_t *sizes);
    void out();
private:
	Engine *ep;
    char obuffer[MENG_OBUF_SIZE+1];
};


    #endif /* defined(__Segment3D__ClassifierWrapper__) */
