//
//  DihedralAngles.cpp
//  HelloCGAL
//
//  Created by Sourabh Daptardar on 03/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include "DihedralAngles.h"
#include "projutils.h"


#if 1

void getFacePoints(MyMesh &msh, VecVecIdx &V,  point_id_t &C, vector<pair<Idx,Idx> > &F)
{
    V.clear();
    
    // Do not reassign IDs
    //Idx i = 0;
    Vertex_iterator vertexBegin = msh.M.vertices_begin();
    Vertex_iterator vertexEnd = msh.M.vertices_end();
    
    for (Vertex_iterator vitr = vertexBegin; vitr != vertexEnd ; ++vitr) {
        C.insert(make_pair(vitr->id(), vitr->point()));
        //i++;
    }
    
    
    Edge_iterator edgesBegin = msh.M.edges_begin();
    Edge_iterator edgesEnd = msh.M.edges_end();
    
    for (Edge_iterator itr = edgesBegin; itr != edgesEnd; ++itr) { // for each edge
        
        VecIdx V1;
        V1.clear();
        // get two adjacent faces
        Facet_iterator f1 =  itr->face();
        //Idx fid1 = itr->face()->id();
        Facet_iterator f2 = itr->opposite()->face();
        //Idx fid2 = itr->opposite()->face()->id();

        // Iterate over face 1
        Halfedge_facet_circulator circ = f1->facet_begin();
        
        do {
            cout << circ->vertex()->id() << " ";
            //Point p1 = circ->vertex()->point();  // co-ordinates
            Idx idx1 = circ->vertex()->id();
            V1.push_back(idx1);
        } while ( ++circ != f1->facet_begin());
        
        // Iterate over face 2
        Halfedge_facet_circulator circ2 = f2->facet_begin();
        VecPoint F2; F2.clear();
        do {
            cout << circ2->vertex()->id() << " ";
            //Point p2 = circ2->vertex()->point();  // co-ordinates
            Idx idx2 = circ2->vertex()->id();
            if (find(V1.begin(),V1.end(),idx2) == V1.end())
                V1.push_back(idx2);
        } while ( ++circ2 != f2->facet_begin());
        
        cout << endl;
        V.push_back(V1);
        
    }

    //cout << "V.size(): "<< V.size() << endl;
    //cout << "F.size():" << F.size() << endl;
}

#endif

#if 0
void calcPairwiseDihedralAngles(MyMesh &msh, vector<pair<Idx,Idx> > &F,vector<double> &dAngle)
{
    VecVecIdx V;
    point_id_t C;

    getFacePoints(msh, V, C, F);
    
    // cout << "Number of adjacent pairs :" << V.size() << endl;
    unsigned long int N = V.size();
    for (unsigned long int i = 0 ; i < N ; i++){

        double dihedralAngle = CGAL::Mesh_3::dihedral_angle(C[V[i][0]],C[V[i][1]],C[V[i][2]],C[V[i][3]]);
        dAngle.push_back(dihedralAngle);
        msh.PMap[F[i]].dihedralAngleFeature = dihedralAngle;
        
        //        unsigned long int M = V[i].size();
        //        cout << CGAL::Mesh_3::dihedral_angle(C[V[i][0]],C[V[i][1]],C[V[i][2]],C[V[i][3]]) << endl;;
        //        for (int j= 0 ; j < M ; j++) {
        //
        //            cout << C[V[i][j]] << " ";
        //        }
        //        cout << endl;
    }
    
    
}

#endif

bool cmp_pair(pair<double,int> p, pair<double,int> q)
{
    return ((p.first < q.first) || ((p.first == q.first) && (p.second < q.second)));
}


void calcPairwiseDihedralAngles(MyMesh &msh,vector<pair<Idx,Idx> > &F,vector<double> &diAngle,
                                double &minA, double &maxA, double &avgA, double &devA)
{
    int numFaces = msh.M.size_of_facets();
    F.clear();
    diAngle.clear();

#if 1  // switch off for testing
    cout << "Median all pairs geodesic distance: " << msh.medianGeodesicDist << endl;

    msh.fd = open(msh.dump.c_str(), O_RDONLY);
    if (msh.fd < 0) {
        perror("Could not open file");
        return;
    }

    size_t len = (size_t)(sizeof(Idx)+numFaces*numFaces*sizeof(double));
    off_t off = sizeof(Idx);
    msh.addr = mmap(0, len, PROT_READ, MAP_SHARED, msh.fd, 0);

    if (msh.addr == MAP_FAILED) {
        perror("Memory map failed");
        return;
    }

    msh.dst = (reinterpret_cast<double *>((char *)msh.addr + off));
    msh.dist = new double*[numFaces];
    msh.dist[0] = msh.dst;
    for(int i = 1 ; i < int(numFaces) ; i++) {
        msh.dist[i] = msh.dist[0] + i * numFaces;
    }

    cout << "Memory mapped " << msh.dump.c_str() << " at: " << hex << msh.addr
         << dec << " size " << sizeof(Idx) << " + " << numFaces*numFaces*sizeof(double)
         << " bytes" << endl;

#endif


    Edge_iterator edgesBegin = msh.M.edges_begin();
    Edge_iterator edgesEnd = msh.M.edges_end();

    double sumA = 0.0;
    double sumsqA = 0.0;
    minA = numeric_limits<double>::max();
    maxA = numeric_limits<double>::min();
    int N = 0;
    for (Edge_iterator itr = edgesBegin; itr != edgesEnd; ++itr) { // for each edge

        // get two adjacent faces
        Facet_iterator f1 =  itr->face();
        Facet_iterator f2 = itr->opposite()->face();

        // Calculate dihedral angle
        double dAngle = dihedralAngle(itr, f1, f2) / 180.0;
        cout << "Dihedral angle : " << dAngle << endl;
        PairIdx edge = make_pair(f1->id(),f2->id());
        if (msh.PMap.find(edge) == msh.PMap.end()) { // not found
            PairwiseFeature P;
            P.dihedralAngleFeature = dAngle;
            boost::numeric::ublas::vector<double> dataVec(dimPairwise);
            dataVec(0) = dAngle;
            P.dataVec = dataVec;
            msh.PMap.insert(make_pair(edge,P));
        } else {
            msh.PMap[edge].dihedralAngleFeature = dAngle;
            boost::numeric::ublas::vector<double> dataVec(dimPairwise);
            dataVec(0) = dAngle;
            msh.PMap[edge].dataVec = dataVec;
        }
        F.push_back(edge);
        diAngle.push_back(dAngle);
        sumA += dAngle;
        sumsqA += (dAngle * dAngle);
        if (dAngle > maxA) maxA = dAngle;
        if (dAngle < minA) minA = dAngle;

        cout << "Mean Geodesic Distance : " << msh.avgGeodesicDist << endl;
        cout << "Median Geodesic Distance : " << msh.medianGeodesicDist << endl;

#if 1 // switch off for test
        // for features based on
        //average dihedral angles around each edge
        // at geodesic radii 0.5%, 1%, 2%, 4%
        // of median of all-pairs geodesic distance

        double per[] = { 0.5, 1, 2, 4};
        int F = sizeof(per) / sizeof(per[0]);

        for (int f = 0 ; f < F ; f++) {

            double avgAngle = 0.0;

            double radius = per[f] * 0.01 * msh.medianGeodesicDist;
            //double delta = 0.25 * (pow(2.0,double(f)) - pow(2.0,double(f-1))) * 0.01 * msh.medianGeodesicDist ;
            double delta = 0.5 * 0.01 * msh.medianGeodesicDist ;
            cout << "GEO RADIUS : " << f << " : " << radius << " = > " << endl;
            cout << "delta : " << delta << endl;
            VGeoIdx D;
            D.clear();
            for (int j = 0 ; j < numFaces ; j++) {
                double d = msh.dist[msh.faceNum[f1->id()]][msh.faceNum[j]];
                pair<double,int> pr(d,msh.faceNum[j]);
                D.push_back(pr);
            }

            sort(D.begin(),D.end());
            //GeoIdx rd(radius-delta,0);
            GeoIdx rd(0,0);
            GeoIdx rd2(radius+delta,numeric_limits<double>::max());
            VGeoIdxItr dbegin = D.begin();
            VGeoIdxItr dend = D.end();
            VGeoIdxItr low = std::lower_bound(dbegin, dend, rd,cmp_pair);
            VGeoIdxItr up = std::upper_bound(dbegin, dend, rd2,cmp_pair);

            tr1::unordered_set<Idx> S;
            S.clear();
            for (VGeoIdxItr vitr = low ; vitr != up; ++vitr) {
                S.insert(vitr->second);
            }

            int count = 0;
            Edge_iterator eBegin = msh.M.edges_begin();
            Edge_iterator eEnd = msh.M.edges_end();

            for (Edge_iterator eitr = eBegin ; eitr != eEnd ; ++eitr) {
                Facet_iterator f1 = eitr->face();
                Facet_iterator f2 = eitr->opposite()->face();
                Idx fid1 = f1->id();
                //Idx fid2 = f2->opposite()->id();

                // Lookup to check if the face is marked visited
                if (S.find(fid1) != S.end()) {
                    double dAngle = dihedralAngle(eitr,f1,f2) / 180.0;
                    avgAngle += dAngle;
                    count++;
                }
            }


            //            for (VGeoIdxItr vitr = low ; vitr != up; ++vitr) { // over faces, not vertices
            //                //cout << vitr->first << " " << vitr->second << " " << flush;
            //                Facet_iterator fc = msh.M.facets_begin();
            //                std::advance(fc,msh.faceNum[vitr->second]);
            //                //cout << "Radial face : " << fc->id() << " : "<< endl;

            //                Halfedge_handle h = fc->halfedge();
            ////                Edge_iterator e = h->edge();
            ////                double angle = dihedralAngle(e,fc,fc->opposite()->face()) / 180.0 ;
            ////                avgAngle += angle;
            ////                count++;



            //                                        Halfedge_facet_circulator c = h->facet_begin();
            //                                        do {
            //                                            double angle = dihedralAngle(fc,c->opposite()->face()) / 180.0 ;
            //                                            //cout << "adj : " << c->opposite()->face()->id()
            //                                            //     << " : " << "angle : " << angle << endl;
            //                                            avgAngle += angle;
            //                                            count++;
            //                                        } while (++c != h->facet_begin() );

            //            }

            if (abs(count) > EPS)
                avgAngle /= double(count);
            else
                avgAngle = 1.0;

            if (isnan(avgAngle))
                avgAngle = 1.0;
            cout << " f: " << f << " :: avgAngle: " << avgAngle << endl;
            msh.PMap[edge].dataVec(f+1) = avgAngle;
        }

#endif
        N++;
    }

    avgA = sumA / double(N);
    devA = sqrt( ( (sumsqA/double(N)) - avgA * avgA ) );

#if 1
    // normalize features

    double *sumF = new double[dimPairwiseUnit];
    double *sumsqF = new double[dimPairwiseUnit];
    double *avgF = new double[dimPairwiseUnit];
    double *devF = new double[dimPairwiseUnit];
    double *minF = new double[dimPairwiseUnit];
    double *maxF = new double[dimPairwiseUnit];

    for (int i = 0 ; i < dimPairwiseUnit ; i++) {
        sumF[i] = 0.0;
        sumsqF[i] = 0.0;
        minF[i] = numeric_limits<double>::max();
        maxF[i] = numeric_limits<double>::min();
    }

    int NF = 0;
    for (Edge_iterator itr = edgesBegin; itr != edgesEnd; ++itr) { // for each edge
        Facet_iterator f1 =  itr->face();
        Facet_iterator f2 = itr->opposite()->face();
        PairIdx edge = make_pair(f1->id(),f2->id());
        for (int i = 0 ; i < dimPairwiseUnit ; i++) {
            double d = msh.PMap[edge].dataVec(i);
            sumF[i] += d;
            sumsqF[i] += (d*d);
            if (d > maxF[i]) maxF[i] = d;
            if (d < minF[i]) minF[i] = d;
        }
        NF++;
    }

    cout << "#EDGES : " << NF << endl;
    for (int i = 0 ; i < dimPairwiseUnit ; i++) {
        avgF[i] = sumF[i] / double(NF);
        devF[i] = sqrt( ( (sumsqF[i]/double(NF)) - avgF[i] * avgF[i] ) );

        cout << "MINF[" << i << "] " << minF[i] << endl;
        cout << "MAXF[" << i << "] " << maxF[i] << endl;
        cout << "AVGF[" << i << "] " << avgF[i] << endl;
        cout << "DEVF[" << i << "] " << devF[i] << endl;

     }

//    for (Edge_iterator itr = edgesBegin; itr != edgesEnd; ++itr) { // for each edge
//        Facet_iterator f1 =  itr->face();
//        Facet_iterator f2 = itr->opposite()->face();
//        PairIdx edge = make_pair(f1->id(),f2->id());
//        for (int i = 0 ; i < dimPairwiseUnit ; i++) {
//            msh.PMap[edge].dataVec(i) /= minF[i];
//        }
//    }


        for (Edge_iterator itr = edgesBegin; itr != edgesEnd; ++itr) { // for each edge
            Facet_iterator f1 =  itr->face();
            Facet_iterator f2 = itr->opposite()->face();
            PairIdx edge = make_pair(f1->id(),f2->id());
            for (int i = 0 ; i < dimPairwiseUnit ; i++) {
                msh.PMap[edge].dataVec(i) -= minF[i];
                msh.PMap[edge].dataVec(i) /= (maxF[i] - minF[i]);
            }
        }


    delete [] sumF;
    delete [] sumsqF;
    delete [] avgF;
    delete [] devF;
    delete [] minF;
    delete [] maxF;

#endif

#if 0
    //    // memory mapped matrix
    //    munmap((void *)(msh.addr), sizeof(Idx) + numFaces * numFaces * sizeof(double));
    //    delete [] msh.dist;
    //    close(msh.fd);
#endif




}



/**
 * @brief calcNormal
 * @param F
 * @return normal vector
 *
 * Use Newell's algorithm to calculate normal
 */


Point calcNormal(Facet_iterator F)
{
    double x = 0.0, y = 0.0, z = 0.0;
    Halfedge_facet_circulator circ = F->facet_begin();
    do {
        //cout << circ->vertex()->id() << " ";
        Point Current = circ->vertex()->point();       // co-ordinates of current and next vertex
        Point Next = circ->next()->vertex()->point();

        x = x + ((Current.y() - Next.y()) * (Current.z() + Next.z()));
        y = y + ((Current.z() - Next.z()) * (Current.x() + Next.x()));
        z = z + ((Current.x() - Next.x()) * (Current.y() + Next.y()));

    } while ( ++circ != F->facet_begin());

    double len = sqrt(x * x + y * y + z * z);
    x = x / len;
    y = y / len;
    z = z / len;

    return Point(x,y,z);
}

Point cross(const Point &A, const Point &B)
{
    return Point( A.y() * B.z() - A.z() * B.y(),
                  A.z() * B.x() - A.x() * B.z(),
                  A.x() * B.y() - A.y() * B.x()
                  );
}

double dot(const Point &A, const Point &B)
{
    return (A.x() * B.x() + A.y() * B.y() + A.z() * B.z());
}

double mag(const Point &X)
{
    return sqrt(dot(X,X));
}

Point scale(const Point &P, const double k)
{
    return Point(k*P.x(),k*P.y(),k*P.z());
}

Point normalize(const Point &P)
{
    double magn = mag(P);
    if (magn == 0.0) return Point(0.0,0.0,0.0);
    return Point(scale(P,1.0/magn));
}

double dihedralAngle(const Point &edge, const Point &norm1, const Point &norm2)
{

    Point normal1 = normalize(norm1);
    Point normal2 = normalize(norm2);
    double cosphi = dot(normal1,normal2);
    Point sinphi(cross(normal1,normal2));
    double dAngle = atan2(mag(sinphi),cosphi);
    Point edgeN(normalize(edge));
    dAngle = dAngle * sgn(dot(sinphi,edgeN));
    //dAngle = (M_PI + dAngle) / M_PI;
    //dAngle = 180.0 + dAngle * 180.0 / M_PI;
    dAngle = 180.0 - dAngle * 180.0 / M_PI;
    cout << "dAngle : " << dAngle << endl;
    return dAngle;
}

double dihedralAngle(Edge_iterator E, Facet_iterator F1, Facet_iterator F2)
{
    Point P(E->vertex()->point());
    Point Q(E->prev()->vertex()->point());
    Point edgeVec(P.x() - Q.x(), P.y() - Q.y(), P.z() - Q.z());
    return dihedralAngle(edgeVec, calcNormal(F1), calcNormal(F2));
}


#if 0
/**
 * @brief dihedralAngle
 * @param normal1
 * @param normal2
 * @return dihedralAngle between normal1 and normal2
 */

double dihedralAngle(Point normal1, Point normal2)
{
    double den  = \
            (  \
                sqrt((double)normal1.x() * normal1.x() + normal1.y() * normal1.y() + normal1.z() * normal1.z()) * \
                sqrt((double)normal2.x() * normal2.x() + normal2.y() * normal2.y() + normal2.z() * normal2.z()) \
                );

    double num =  ((double)normal1.x() * normal2.x() + normal1.y() * normal2.y() + normal1.z() * normal2.z());

    cout << "Sign (num, den) : " << sgn(num) << " " << sgn(den) << endl;

    double dangle =  (( (abs(num) <= abs(den)) && (abs(den) > EPS)  )  ? (180.0 - acos(num / den) * 180.0 / M_PI) : 180.0);
    return (isnan(dangle) ? 0.0 : dangle);
}



/**
 * @brief dihedralAngle
 * @param F1
 * @param F2
 * @return dihedral angle between faces pointed by F1 and F2
 */

double dihedralAngle(Facet_iterator F1, Facet_iterator F2)
{
    return dihedralAngle(calcNormal(F1), calcNormal(F2));
}

#endif
