#ifndef MESHVIEWER_H
#define MESHVIEWER_H

/*
 * References:
 * 1. Singleton Pattern : http://www.yolinux.com/TUTORIALS/C++Singleton.html
 * 2. Vertex Buffer Object : http://www.youtube.com/watch?v=KIeExgOcmv0
 *
 */


#include <fstream>
#include <vector>
#include <string>

extern "C" {
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
}

using namespace std;

class MeshViewer
{
    int numVertices;
    int numFaces;
    int numEdges;
    float *vertex_coords;
    float *color_coords;
    unsigned int *face_vertex_idx;
    vector<unsigned int> face_vertex_indices;
    vector<float> face_vertex_color;
    vector<int> face_vertex_count;
    vector<int> face_color_count;
    ifstream ifs;
    int numSides;
    GLuint vboId;   /* Identifier for Vertex Buffer Object */
    GLuint idxId;   /* Identifier for VBO for indexes      */
    GLuint colorId;

    double rot_x;
    double rot_y;
    double rot_z;

    // Singleton class
    // Constructor, copy constructor and assignemt operator are private
    MeshViewer();
    MeshViewer(MeshViewer const&) {}
    MeshViewer& operator=(MeshViewer const &){return *this;}
    static MeshViewer *pInstance;

protected:
    bool checkHeader(void);
    void getDimensions(void);
    void getVertexCoords(void);
    void getFaceIndices(void);

public:

    void display(const std::string &offFile);
    ~MeshViewer();
    friend void MeshViewerInit(void);
    friend void MeshViewerDisplay(void);
    friend void MeshViewerMainLoop(void);
    friend void MeshViewerSpecialKeys(int key, int x, int y);
    static MeshViewer* getInstance(void);

};



void MeshViewerInit(void);
void MeshViewerDisplay(void);
void MeshViewerSpecialKeys(int key, int x, int y);
void MeshViewerInitGlutCallbacks(void);
void MeshViewerMainLoop(void);

#endif // MESHVIEWER_H
