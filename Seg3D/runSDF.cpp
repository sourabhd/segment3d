#include "run.h"
#include "runSDF.h"
#include "gco-v3.0/runGraphCut.h"
#include "SDF.h"
#include "common.h"


void calcSDFFeature(MyMesh &msh,const int meshNum, const string &category,
                    const string &offFile,
                    const string &onameUnary,
                    const string &onamePairwise)
{
    cout << "Mesh #" << meshNum  << " ... " << flush;
    //CGALPolyhedron2OFF::OFF2MyMesh(msh,offFile);

    Facet_iterator facesBegin = msh.M.facets_begin();
    Facet_iterator facesEnd = msh.M.facets_end();

    clock_t c1 = clock();
    time_t  t1 = time(NULL);

    double maxf[dimUnary];
    for (int k = 0 ; k < dimUnary ; k++)
        maxf[k] = SMALL;
    double minf[dimUnary];
    for (int k = 0 ; k < dimUnary ; k++)
        minf[k] = LARGE;

    for (Facet_iterator fitr  = facesBegin; fitr != facesEnd; ++fitr) {
        if (fitr->id() >= msh.M.size_of_facets() )
            continue;
        MyPolygon poly;
        facetItrToPolygon(fitr,poly);
        //cout << poly << endl;
        Point ray_p = poly.centroid;
        Direction ray_d1 = poly.normal.direction();
        Direction ray_d2 = -poly.normal.direction();
        Ray ray1(ray_p,ray_d1);
        Ray ray2(ray_p,ray_d2);
        double sd = LARGE;
        for (Facet_iterator fitr2 = facesBegin ; fitr2 != facesEnd ; ++fitr2) {
            if (fitr2->id() >= msh.M.size_of_facets() )
                continue;
            MyPolygon poly2;
            facetItrToPolygon(fitr2,poly2);
            //cout << doesRayIntersectPolygon(ray,poly2) << " ";
            double d = LARGE;

            if ((fitr->id() != fitr2->id()) && (doesRayIntersectPolygon(ray1,poly2) || doesRayIntersectPolygon(ray2,poly2) ) ) {
                d = dist(poly.centroid,poly2.centroid);

#if 1
                cout << "F1: ";
                Halfedge_facet_circulator circ = fitr->facet_begin();
                do {
                    Point Current = circ->vertex()->point();       // co-ordinates of current and next vertex
                    cout << circ->vertex()->id() << " : " << Current.x() << " " << Current.y() << " " << Current.z() << endl;
                } while ( ++circ != fitr->facet_begin());

                cout << "F2: ";
                Halfedge_facet_circulator circ2 = fitr2->facet_begin();
                do {
                    Point Current = circ2->vertex()->point();       // co-ordinates of current and next vertex
                    cout << circ2->vertex()->id() << " : " << Current.x() << " " << Current.y() << " " << Current.z() << endl;
                } while ( ++circ2 != fitr2->facet_begin());

                cout << poly.centroid << " " << poly2.centroid << endl;
                cout << fitr->id() << " " << fitr2->id() << " : " << d << endl;
#endif

            }

            if (d < sd) {
                sd = d;
            }
        }

        //cout << fitr->id() << " " << sd;
        //cout << endl;

        Idx fid = fitr->id();

        boost::numeric::ublas::vector<double> dataVec(dimUnary);

        dataVec[0] = sd;
        for (int k = 1 ; k < dimUnarySDF  ; k++ ) {
            if ((sd < LARGE) && (sd > SMALL))
                dataVec[k] = pow(dataVec[0] ,double(k+1));
            else
                dataVec[k] = LARGE;
        }

        UnaryFeature uf;
        uf.dataVec = dataVec;

        if (msh.UMap.find(fid) != msh.UMap.end()) {

            // Found distance entered, check for minimum
            if (uf.dataVec[0] < msh.UMap[fid].dataVec[0]) {

                if ((uf.dataVec[0] < LARGE) && (uf.dataVec[0] > SMALL))
                    msh.UMap[fid].dataVec[0] = uf.dataVec[0];
                else
                    msh.UMap[fid].dataVec[0] = LARGE;

                for (int k = 1 ; k < dimUnarySDF  ; k++ ) {
                    if ((msh.UMap[fid].dataVec[0] < LARGE) && (msh.UMap[fid].dataVec[0] > SMALL))
                        msh.UMap[fid].dataVec[k] = pow(msh.UMap[fid].dataVec[0] ,double(k+1));
                    else
                        msh.UMap[fid].dataVec[k] = LARGE;
                }
            }
        } else {

            // Else enter distance
        msh.UMap.insert(make_pair(fid,uf));

        }

        for (int k = 0 ; k < dimUnarySDF  ; k++ ) {

            if ((dataVec[k] < minf[k]) && (dataVec[k] < LARGE)) {
                minf[k] = dataVec[k];
            }

            if ((dataVec[k] > maxf[k]) && (dataVec[k] < LARGE)) {
                maxf[k] = dataVec[k];
            }
        }

    }


    // Show features (before normalization)

    for (Facet_iterator fitr  = facesBegin; fitr != facesEnd; ++fitr) {
        Idx fid = fitr->id();
        double fval = 0;
        if (msh.UMap.find(fid) == msh.UMap.end()) fval = 1.0;
        else fval = msh.UMap[fid].dataVec[0];
        //cout << "fval: " << fval << endl;
        if (maxf == minf) {
            msh.UMap[fid].R = 0.0;
        } else if ((fval >= LARGE) || (fval <= SMALL)) {
            msh.UMap[fid].R = 1.0;
        } else {
            //msh.UMap[fid].R = (fval-minf)/(maxf-minf);
            msh.UMap[fid].R = fval;

        }
        msh.UMap[fid].G = 0.0;
        msh.UMap[fid].B = 0.0;
        msh.UMap[fid].A = 1;
    }


    CGALPolyhedron2OFF::MyMesh2OFF(msh,onameUnary);

    Edge_iterator edgesBegin = msh.M.edges_begin();
    Edge_iterator edgesEnd = msh.M.edges_end();
    double maxpf[dimPairwise];
    double minpf[dimPairwise];
    for (int k = 0 ; k < dimPairwise ; k++)
        maxpf[k] = SMALL;
    for (int k = 0 ; k < dimPairwise ; k++)
        minpf[k] = LARGE;

    for (Edge_iterator itr = edgesBegin; itr != edgesEnd; ++itr) { // for each edge
        // get two adjacent faces
        Idx fid1 = itr->face()->id();
        Idx fid2 = itr->opposite()->face()->id();
        pair<Idx,Idx> pr = make_pair(fid1,fid2);
        PairwiseFeature P;
        boost::numeric::ublas::vector<double> dataVec(dimPairwise);
        double fval1 = msh.UMap[fid1].dataVec[0];
        double fval2 = msh.UMap[fid2].dataVec[0];
        double fval = LARGE;
        if ((fval1 >= LARGE) || (fval2 >= LARGE) || (fval1 <= SMALL) || (fval2 <= SMALL)) {
            fval = LARGE;
        } else {
            fval = abs(fval1-fval2);
        }
        dataVec[0] = fval;

        for (int k = 1 ; k < dimPairwise  ; k++ ) {
            if ((dataVec[0] > SMALL) && (dataVec[0] < LARGE)) {
                dataVec[k] = pow(dataVec[0] ,double(k+1));
            } else {
                dataVec[k] = LARGE;
            }
        }

        P.dataVec = dataVec;
        if (msh.PMap.find(pr) == msh.PMap.end()) {
            msh.PMap.insert(make_pair(pr, P));
        } else {
            msh.PMap[pr] = P;
        }

        cout << "P: " << msh.PMap[pr].dataVec[0] << endl;

        for (int k = 0 ; k < dimPairwise  ; k++ ) {
            if ((msh.PMap[pr].dataVec[k] > SMALL) && (msh.PMap[pr].dataVec[k] < LARGE) && (msh.PMap[pr].dataVec[k] < minpf[k])) {
                if (msh.PMap[pr].dataVec[k] < 1.0)
                    minpf[k] = msh.PMap[pr].dataVec[k];
            }

            if ((msh.PMap[pr].dataVec[k] > SMALL) && (msh.PMap[pr].dataVec[k] < LARGE) && msh.PMap[pr].dataVec[k] > maxpf[k]) {
                if (msh.PMap[pr].dataVec[k] < 1.0)
                    maxpf[k] = msh.PMap[pr].dataVec[k];
            }
        }
    }

    // Show pairwise features before normalization
    showPairwiseFeatures(msh, onamePairwise, minpf[0], maxpf[0]);


    // Normalize to 0 - 1 range

    for (Facet_iterator fitr  = facesBegin; fitr != facesEnd; ++fitr) {
        Idx fid = fitr->id();

        for (int k = 0 ; k < dimUnary  ; k++ ) {
            if ((msh.UMap[fid].dataVec[k] >= LARGE) || (msh.UMap[fid].dataVec[k] <= SMALL))
                msh.UMap[fid].dataVec[k] = 1.0;
            else
                msh.UMap[fid].dataVec[k] =  (msh.UMap[fid].dataVec[k] - minf[k]) / (maxf[k]-minf[k]);
        }

    }

#if 1
    for (int k = 0 ; k < dimPairwise  ; k++ ) {
        cout << k  << " : " << "MINP: " << minpf[k] << " " << "MAXPF: " << maxpf[k] << endl;
    }

    for (Edge_iterator itr = edgesBegin; itr != edgesEnd; ++itr) { // for each edge
        // get two adjacent faces
        Idx fid1 = itr->face()->id();
        Idx fid2 = itr->opposite()->face()->id();
        pair<Idx,Idx> pr = make_pair(fid1,fid2);
        if (msh.PMap.find(pr) == msh.PMap.end()) {
            cout << "Not found " << pr.first << " " << pr.second << endl;
            continue;
        }

        for (int k = 0 ; k < dimPairwise  ; k++ ) {
            if ( (msh.PMap[pr].dataVec[k] >= LARGE) || (msh.PMap[pr].dataVec[k] <= SMALL)) {
                msh.PMap[pr].dataVec[k] = 1.0;
            } else {
                msh.PMap[pr].dataVec[k] =  (msh.PMap[pr].dataVec[k] - minpf[k]) / (maxpf[k]-minpf[k]) ;
            }

        }
        cout << "Q: " << msh.PMap[pr].dataVec[0] << endl;

    }
#endif

    cout << " done" << endl << flush;

    time_t  t2 = time(NULL);
    clock_t c2 = clock();
    cout << "Clock: " << (c2 - c1) / CLOCKS_PER_SEC << endl;
    cout << "Time:  " << (t2 - t1) << endl;
}


void runSDF(string category)
{
    time_t t1 = time(NULL);
    clock_t c1 = clock();

    // Use category variable for
    // Single category quick test
    // For complete dataset use foreach
    //string category = "Bird";
    //string category = "Octopus";
    //string category = "Cup";

    data_t trdata;
    Meshes_t Meshes;
    Meshes.clear();
    map<string,int> CatStartNum;
    CatStartNum.clear();
    tr1::unordered_map<string,int> CatNumLabels;
    CatNumLabels.clear();
    int c = 0;

    // Read data set
    Idx N = 0;

    cout << category << " : ";
    cout << CategoryFileStartNum[category] << " " << CategoryFileEndNum[category] << endl;
    CatStartNum[category] = c;

    readDataset(category,CategoryFileStartNum[category], CategoryFileEndNum[category], Meshes, CatNumLabels);

    c += Meshes[category].size();
    N += Meshes[category].size();
    cout << CategoryFileStartNum[category] << " " << CategoryFileEndNum[category] << endl;

    vector<int> exemplarIds;
    vector<int> validationIds;

    cout << "Training Set: " << endl;
    splitDataset(split_x, split_y, int(Meshes[category].size()), exemplarIds, validationIds);
    BOOST_FOREACH(int val, exemplarIds) cout << " " << val;
    cout << endl;
    cout << "Validation Set: " << endl;
    BOOST_FOREACH(int val, validationIds) cout << " " << val;
    cout << endl;

    // Set face and vertex IDs
    BOOST_FOREACH(int id, exemplarIds) setIDs(Meshes[category][id]);
    BOOST_FOREACH(int id, validationIds) setIDs(Meshes[category][id]);

    BOOST_FOREACH(int id, exemplarIds) {
        calcSDFFeature(Meshes[category][id], id, category,
                       Meshes[category][id].off,
                       Meshes[category][id].uoff,
                       Meshes[category][id].poff);
    }

    BOOST_FOREACH(int id, validationIds) {
        calcSDFFeature(Meshes[category][id], id, category,
                       Meshes[category][id].off,
                       Meshes[category][id].uoff,
                       Meshes[category][id].poff);
    }



    cout << "----------------------------- UNARY ------------------------------------- " << endl;
    calcUnaryFeatures2(Meshes, category, exemplarIds, CatStartNum, trdata);
    cout << "Number of unary features in training data : " << trdata.X.size() << endl;


    map<int,data_t> tedata;
    map<int, double*> prob;

    cout << "------------------------------ UNARY (TEST) ------------------------------- " << endl;
    BOOST_FOREACH(int val, validationIds) {
        vector<int> V; V.clear(); V.push_back(val);

        data_t ted;
        calcUnaryFeatures2(Meshes, category, V, CatStartNum, ted);
        tedata.insert(make_pair(val,ted));
        cout << "Number of unary features in test data " << val << ":" << tedata[val].X.size() << endl;


        size_t numTestFaces = tedata[val].X.size();
        size_t numClasses = CatNumLabels[category];
        //size_t numClasses = Meshes[category][0].partpts.size();
        //prob[val] = new double[numClasses*numTestFaces];
        prob.insert(make_pair(val, new double[numClasses*numTestFaces]));

        stringstream sout;
        sout << OutputDir << filesep << category;

        learn(sout.str(), val, true, trdata,tedata[val],Meshes,category,prob[val],CatNumLabels); // unary


        for (size_t i = 0 ; i < numTestFaces ; i++) {
            //for (size_t j = 0 ; j < numTestFaces ; j++) {
            boost::numeric::ublas::vector<double> P(numClasses);
            double mx = -10000000;
            int mx_j = 0;
            //Idx faceid = boost::get<Idx>(tedata.feature[i]);
            //Idx faceid = tedata[val].unaryIds[j];
            //cout << faceid << " : ";
            for (int j = 0 ; j < int(numClasses) ; j++) {
                //for (int i = 0 ; i < int(numClasses) ; i++) {
                cout << prob[val][j*numTestFaces+i] << " ";
                P[j] = prob[val][j*numTestFaces+i];
                if (P[j] > mx) {
                    mx_j = j;
                    mx = P[j];
                }
                //                P[i] = prob[val][j*numClasses+i];
                //                if (P[i] > mx) {
                //                    mx_j = i;
                //                    mx = P[i];
                //                }
            }
            Meshes[category][val].UMap[i].probFeatureVector = P;
            Meshes[category][val].UMap[i].maxProb = mx;
            Meshes[category][val].UMap[i].argmaxProb = mx_j;
            //cout << endl;
        }

    }

    // Pairwise
#if 1

    cout << "----------------------------- PAIRWISE ------------------------------------- " << endl;
    data_t trdataPairwise;

    calcPairwiseFeatures2(Meshes, category, exemplarIds, CatStartNum, trdataPairwise);
    cout << "Number of pairwise features in training data : " << trdataPairwise.X.size() << endl;

    map<int,data_t> tedataPairwise;
    map<int, double*> probPairwise;

    BOOST_FOREACH(int val, validationIds) {
        cout << "------------------------------ PAIRWISE (TEST) ------------------------------- " << endl;
        vector<int> V; V.clear(); V.push_back(val);
        data_t td;
        calcPairwiseFeatures2(Meshes, category, V, CatStartNum, td );
        tedataPairwise.insert(make_pair(val,td));
        cout << "Number of pairwise features in test data "<< val << " : " << tedataPairwise[val].X.size() << endl;


        size_t numTestFacesPairwise = tedataPairwise[val].X.size();
        size_t numClassesPairwise = 2;
        //probPairwise[val] = new double[numClassesPairwise*numTestFacesPairwise];
        probPairwise.insert(make_pair(val, new double[numClassesPairwise*numTestFacesPairwise]));

        stringstream sout;
        sout << OutputDir << filesep << category;

        learn(sout.str(), val, false, trdataPairwise,tedataPairwise[val],Meshes,category,probPairwise[val],CatNumLabels); // binary
        cout << "completed learning for " << val << endl;

        int cnt0 = 0;
        int cnt1 = 0;
        for (size_t i = 0 ; i < numTestFacesPairwise ; i++) {
            //for (size_t j = 0 ; j < numTestFacesPairwise ; j++) {
            //cout << tedataPairwise[val].pairWiseIds[i].first << "," << tedataPairwise[val].pairWiseIds[i].second  << " ";

            Idx u = tedataPairwise[val].pairWiseIds[i].first;
            Idx v = tedataPairwise[val].pairWiseIds[i].second;
            //Idx u = tedataPairwise[val].pairWiseIds[j].first;
            //Idx v = tedataPairwise[val].pairWiseIds[j].second;

            boost::numeric::ublas::vector<double> P(2);
            double mx = -10000000;
            int mx_j = -1;
            for (int j = 0 ; j < int(numClassesPairwise) ; j++) {
                //for (int i = 0 ; i < int(numClassesPairwise) ; i++) {
                //               cout << probPairwise[val][j*numTestFacesPairwise+i] << " ";
                P[j] = probPairwise[val][j*numTestFacesPairwise+i];
                if (P[j] > mx) {
                    mx_j = j;
                    mx = P[j];
                }
                //cout << probPairwise[val][j*numTestFacesPairwise+i] << " ";
                //                P[i] = probPairwise[val][j*numClassesPairwise+i];
                //                if (P[i] > mx) {
                //                    mx_j = i;
                //                    mx = P[i];
                //                }
            }
            //cout << endl;
            Meshes[category][val].PMap[make_pair(u,v)].probFeatureVector = P;
            Meshes[category][val].PMap[make_pair(u,v)].maxProb = mx;
            Meshes[category][val].PMap[make_pair(u,v)].argmaxProb = mx_j;
            if (mx_j  == 0) cnt0++;
            if (mx_j  == 1) cnt1++;
        }
        cout << "Count of zero: " << cnt0 << endl;
        cout << "Count of one: " << cnt1 << endl;
    }



#if 1
    BOOST_FOREACH(int val, validationIds) {
        string fname = Meshes[category][val].off;
        // cout << fname << endl;
        string oname2 = fname.substr(fname.find_last_of(filesep)+1);
        string onameUnary = OutputDir + filesep + category + filesep + "out_unary_" + oname2;
        string onamePairwise = OutputDir + filesep + category + filesep + "out_pairwise_" + oname2;

        cout << "Writing Mesh Output (Unary) : " << onameUnary.c_str() << endl;

        Facet_iterator fBegin = Meshes[category][val].M.facets_begin();
        Facet_iterator fEnd = Meshes[category][val].M.facets_end();
        for (Facet_iterator fitr = fBegin ; fitr != fEnd ; ++fitr) {
            int lbl = Meshes[category][val].UMap[fitr->id()].argmaxProb;
            //cout << fitr->id() << " " << lbl << endl;
            if (lbl < 10) {
                Meshes[category][val].UMap[fitr->id()].R = colorR[lbl];
                Meshes[category][val].UMap[fitr->id()].G = colorG[lbl];
                Meshes[category][val].UMap[fitr->id()].B = colorB[lbl];
                Meshes[category][val].UMap[fitr->id()].A = 1;
            } else {
                Meshes[category][val].UMap[fitr->id()].R = 0;
                Meshes[category][val].UMap[fitr->id()].G = 0;
                Meshes[category][val].UMap[fitr->id()].B = 0;
                Meshes[category][val].UMap[fitr->id()].A = 1;
            }
        }

        CGALPolyhedron2OFF::MyMesh2OFF(Meshes[category][val],onameUnary);


        cout << "Writing Mesh Output (Pairwise) : " << onamePairwise.c_str() << endl;
        Edge_iterator eBegin = Meshes[category][val].M.edges_begin();
        Edge_iterator eEnd = Meshes[category][val].M.edges_end();

        for (Edge_iterator itr = eBegin; itr != eEnd; ++itr) { // for each edge
            // get two adjacent faces
            Idx fid1 = itr->face()->id();
            Idx fid2 = itr->opposite()->face()->id();
            pair<Idx,Idx> pr = make_pair(fid1,fid2);
            cout << "FF: "<< Meshes[category][val].M.size_of_facets() << " " << fid1 << "," << fid2 << endl;
            if (fid1 >= (Idx)(Meshes[category][val].M.size_of_facets()) )
                continue;
            if (fid2 >= (Idx)(Meshes[category][val].M.size_of_facets()) )
                continue;
            int lbl = Meshes[category][val].PMap[make_pair(fid1,fid2)].argmaxProb;
            cout << "F: "<< Meshes[category][val].M.size_of_facets() << " " << fid1 << "," << fid2 << " : " << lbl << endl;
            if (lbl < 10) {
                Meshes[category][val].UMap[fid1].R = colorR[lbl];
                Meshes[category][val].UMap[fid1].G = colorG[lbl];
                Meshes[category][val].UMap[fid1].B = colorB[lbl];
                Meshes[category][val].UMap[fid1].A = 1;
                Meshes[category][val].UMap[fid2].R = colorR[lbl];
                Meshes[category][val].UMap[fid2].G = colorG[lbl];
                Meshes[category][val].UMap[fid2].B = colorB[lbl];
                Meshes[category][val].UMap[fid2].A = 1;
            } else {
                Meshes[category][val].UMap[fid1].R = 0;
                Meshes[category][val].UMap[fid1].G = 0;
                Meshes[category][val].UMap[fid1].B = 0;
                Meshes[category][val].UMap[fid1].A = 1;
                Meshes[category][val].UMap[fid2].R = 0;
                Meshes[category][val].UMap[fid2].G = 0;
                Meshes[category][val].UMap[fid2].B = 0;
                Meshes[category][val].UMap[fid2].A = 1;
            }

        }

        CGALPolyhedron2OFF::MyMesh2OFF(Meshes[category][val],onamePairwise);

    }
#endif


    BOOST_FOREACH(int val, validationIds)
    {
        cout << "Graph cut started for " << val << endl;
        runGraphCut(Meshes,category,val,CatNumLabels);
        cout << "Graph cut ended for " << val << endl;

        string fname = Meshes[category][val].off;
        // cout << fname << endl;
        string oname_s = fname.substr(fname.find_last_of(filesep)+1);
        string onameSeg = OutputDir + filesep + category + filesep + "segmentation_" + oname_s;

        cout << "Writing Segmentation Output : " << onameSeg.c_str() << endl;

        Facet_iterator fBegin = Meshes[category][val].M.facets_begin();
        Facet_iterator fEnd = Meshes[category][val].M.facets_end();
        for (Facet_iterator fitr = fBegin ; fitr != fEnd ; ++fitr) {
            int lbl = Meshes[category][val].UMap[fitr->id()].predLabel;
            //cout << fitr->id() << " " << lbl << endl;
            if (lbl < 10) {
                Meshes[category][val].UMap[fitr->id()].R = colorR[lbl];
                Meshes[category][val].UMap[fitr->id()].G = colorG[lbl];
                Meshes[category][val].UMap[fitr->id()].B = colorB[lbl];
                Meshes[category][val].UMap[fitr->id()].A = 1;
            } else {
                Meshes[category][val].UMap[fitr->id()].R = 0;
                Meshes[category][val].UMap[fitr->id()].G = 0;
                Meshes[category][val].UMap[fitr->id()].B = 0;
                Meshes[category][val].UMap[fitr->id()].A = 1;
            }
        }

        CGALPolyhedron2OFF::MyMesh2OFF(Meshes[category][val],onameSeg);


    }




    BOOST_FOREACH(int val, validationIds) {
        delete [] prob[val];
        delete [] probPairwise[val];
    }

    //}

#endif

    clock_t c2 = clock();
    time_t t2 = time(NULL);
    cout << "Time : " << (t2 - t1) << "seconds" << " " << "Clock : " << (c2-c1) /CLOCKS_PER_SEC << " seconds" << endl;
}

