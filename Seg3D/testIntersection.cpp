#include "common.h"
#include "testIntersection.h"
#include "SDF.h"
#include "CGALPolyhedron2OFF.h"
#include "feature.h"


using namespace std;
using namespace CGAL;

void testIntersection(void)
{
#if 0
    // Ray
    Point     r_p(0.0,0.0,0.0);
    //Direction r_d(1.0,0.0,0.0);
    //Direction r_d(0.5,0.5,0.5);
    Direction r_d(0.5,0.5,0.75);
    //Point     r_d(0.5,0.5,0.5);
    //Point     r_d(1.0,0.0,0.0);
    Ray R(r_p,r_d);
#endif

    // Plane
    Point p_p(1.0,1.0,1.0);
    Direction p_d(0.0,0.0,1.0);
    Plane P(p_p,p_d);

    Vector V(-1.0,-1.0,-1.0);

    //cout << " Intersection: " << doesRayIntersectPlane(R,P,V) << endl;

    //const string offFile = "/usr/local/Cellar/geomview/1.9.4/share/geomview/geom/tetra.off";
    //const string offFile = "/usr/local/Cellar/geomview/1.9.4/share/geomview/geom/facecube.off";
    //const string offFile = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/models/geomview/cube.off";
    const string category = "Octopus";
    const string offFilename = "139.off";
    const string offFile = InputDir + filesep + category + filesep + offFilename;

    MyMesh msh;
    CGALPolyhedron2OFF::OFF2MyMesh(msh,offFile);

    Facet_iterator facesBegin = msh.M.facets_begin();
    Facet_iterator facesEnd = msh.M.facets_end();

#if 0
    for (Facet_iterator fitr  = facesBegin; fitr != facesEnd; ++fitr) {
        MyPolygon poly;
        facetItrToPolygon(fitr,poly);
        cout << poly << endl;
        cout << "Intersection: " << doesRayIntersectPolygon(R,poly) << endl << endl;
    }
#endif

    clock_t c1 = clock();
    time_t  t1 = time(NULL);

    double maxf = numeric_limits<double>::min();
    double minf = numeric_limits<double>::max();

    for (Facet_iterator fitr  = facesBegin; fitr != facesEnd; ++fitr) {
        MyPolygon poly;
        facetItrToPolygon(fitr,poly);
        //cout << poly << endl;
        Point ray_p = poly.centroid;
        Direction ray_d1 = poly.normal.direction();
        Direction ray_d2 = -poly.normal.direction();
        Ray ray1(ray_p,ray_d1);
        Ray ray2(ray_p,ray_d2);
        double sd = numeric_limits<double>::max();
        for (Facet_iterator fitr2 = facesBegin ; fitr2 != facesEnd ; ++fitr2) {
            MyPolygon poly2;
            facetItrToPolygon(fitr2,poly2);
            //cout << doesRayIntersectPolygon(ray,poly2) << " ";
            double d = numeric_limits<double>::max();

            if ((fitr->id() != fitr2->id()) && (doesRayIntersectPolygon(ray1,poly2) || doesRayIntersectPolygon(ray2,poly2) ) ) {
                d = dist(poly.centroid,poly2.centroid);
                //cout << setw(16) << d << " ";
            }

            if (d < sd) {
                sd = d;
            }

        }
        cout << fitr->id() << " "<< sd;
        cout << endl;

        Idx fid = fitr->id();

        boost::numeric::ublas::vector<double> dataVec(dimUnary);
        dataVec[0] = sd;
        UnaryFeature uf;
        uf.dataVec = dataVec;
        msh.UMap.insert(make_pair(fid,uf));

        if (sd < minf) {
            minf = sd;
        }

        if (sd > maxf) {
            maxf = sd;
        }

    }

    time_t  t2 = time(NULL);
    clock_t c2 = clock();
    cout << "Clock: " << (c2 - c1) / CLOCKS_PER_SEC << endl;
    cout << "Time:  " << (t2 - t1) << endl;

    for (Facet_iterator fitr  = facesBegin; fitr != facesEnd; ++fitr) {
        Idx fid = fitr->id();
        double fval = 0;
        if (msh.UMap.find(fid) == msh.UMap.end()) fval = 1.0;
        else fval = msh.UMap[fid].dataVec[0];
        //cout << "fval: " << fval << endl;
        if (maxf == minf) {
            msh.UMap[fid].R = 0.0;
        } else if (fval == numeric_limits<double>::max()) {
            msh.UMap[fid].R = 1.0;
        } else {
            msh.UMap[fid].R = (fval-minf)/(maxf-minf);
        }
        msh.UMap[fid].G = 0.0;
        msh.UMap[fid].B = 0.0;
        msh.UMap[fid].A = 1;

    }

    string onameUnary = OutputDir + filesep + category + filesep + "unary_sdf_" + offFilename;
    CGALPolyhedron2OFF::MyMesh2OFF(msh,onameUnary);


    Edge_iterator edgesBegin = msh.M.edges_begin();
    Edge_iterator edgesEnd = msh.M.edges_end();
    double maxpf = numeric_limits<double>::min();
    double minpf = numeric_limits<double>::max();

    for (Edge_iterator itr = edgesBegin; itr != edgesEnd; ++itr) { // for each edge
        // get two adjacent faces
        Idx fid1 = itr->face()->id();
        Idx fid2 = itr->opposite()->face()->id();
        pair<Idx,Idx> pr = make_pair(fid1,fid2);
        PairwiseFeature P;
        boost::numeric::ublas::vector<double> dataVec(dimPairwise);
        double fval1 = msh.UMap[fid1].dataVec[0];
        double fval2 = msh.UMap[fid2].dataVec[0];
        double fval = numeric_limits<double>::max();
        if ((fval1 == numeric_limits<double>::max()) || (fval2 == numeric_limits<double>::max())) {
            fval = numeric_limits<double>::max();
        } else {
            fval = abs(fval1-fval2);
        }
        dataVec[0] = fval;
        P.dataVec = dataVec;
        msh.PMap.insert(make_pair(pr, P));
        if (fval < minpf) {
            minpf = fval;
        }

        if (fval > maxpf) {
            maxpf = fval;
        }

    }

    string onamePairwise = OutputDir + filesep + category + filesep + "pairwise_sdf_diff_" + offFilename;
    showPairwiseFeatures(msh, onamePairwise, minpf, maxpf);

}
