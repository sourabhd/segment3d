//
//  training.cpp
//  Segment3D
//
//  Created by Sourabh Daptardar on 01/12/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include "training.h"

using namespace std;
using namespace boost;
using namespace boost::filesystem;
using namespace boost::system;

void splitDataset(const int ratio_x, const int ratio_y, const int N, vector<int> &exemplars, vector<int> &validation)
{
    vector<int> V;
    V.clear();
    exemplars.clear();
    validation.clear();
    for (int i = 0; i < N; i++) V.push_back(i);
    random_shuffle(V.begin(), V.end());
    const int numTrain = ratio_x * N / (ratio_x +  ratio_y);
    for (int i = 0 ; i < numTrain ; i++) exemplars.push_back(V[i]);
    for (int i = numTrain; i < N; i++) validation.push_back(V[i]);
}


#if 0
void train(Meshes_t &M, const string &category, const vector<int> &exemplarIds)
{

    error_code ec;
    path cwd = current_path();
    current_path(path(MatlabRoot),ec);
    ClassifierWrapper C(JointBoostClassifierDir, JointBoostClassifierDir);
    C.runMatlabCmd("demo");
    current_path(cwd,ec);
}

#endif


void learn(const string &outdir, Idx meshid, bool isUnary, data_t &trdata, data_t &tedata,Meshes_t &meshes, string &category, double prob[], tr1::unordered_map<string,int> &CatNumLabels)
{
    error_code ec;
    path cwd = current_path();
    current_path(path(MatlabRoot),ec);
    ClassifierWrapper C(JointBoostClassifierDir, JointBoostClassifierDir);
    
    //size_t numClasses = ((isUnary) ? distinctPartsId.size() : 2);
    size_t numClasses = ((isUnary) ? meshes[category][0].partpts.size() : 2);
    
    
    //mxArray *X;
    C.convToMatlab(outdir, meshid,isUnary,trdata,tedata,meshes,category,CatNumLabels);
    //C.out();
    //C.put("X", X);
    //C.runMatlabCmd("X=eye(10,10);");
    //C.runMatlabCmd("[Y] = func1(X);");
    //double *val = new double[numData*numDim];
    //C.get("Y", &val);

    string jbcCmd = "[Cx, Fx, Fn, Fstumps, prob] = runJointBoost(outDir, mId, bUnary, features', featurestest',cl', vl',nClasses,NweakClassifiers, Nthresholds);";
    cout << jbcCmd << endl;
    C.runMatlabCmd(jbcCmd);
    //C.out();
   
    //size_t numTest = tedata.X.size();
    size_t nd = 0,  el = 0;
    //size_t numDim = tedata.X[0].size();
    
    //double *prob = new double[numClasses*numTest];
    size_t dm[numClasses];
    
    C.get("prob",(void *)prob,el,nd,dm);
    
    
#if 0
    for (size_t i = 0 ; i < numTest ; i++) {
        for (size_t j = 0 ; j < numClasses ; j++) {
            cout << prob[j*numTest+i] << " ";
        }
        cout << endl;
    }
#endif
    
    current_path(cwd,ec);
    
    // cout << "Outside: ";
    // for (int i = 0 ; i < numData * numDim ; i++)
    //    cout << val[i] << " ";
    // cout << endl;
    // delete [] val;
}

