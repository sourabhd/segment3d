//
//  testGraph.cpp
//  Segment3D
//
//  Created by Sourabh Daptardar on 30/11/13.
//  Copyright (c) 2013 Sourabh Daptardar. All rights reserved.
//

#include "testGraph.h"

using namespace std;

void testGraph(void)
{
#if 0
    Point p(0.0,0.0,0.0);
    Point q(1.0,0.0,0.0);
    Point r(0.0,1.0,0.0);
    Point s(0.0,0.0,1.0);
    
    
    Polyhedron P;
    P.make_tetrahedron(p,q,r,s);
#endif
    
    string modelfile = "/Users/sourabhdaptardar/Datasets/MeshsegBenchmark-1.0/data/off/241.off";
    //string modelfile = "/Users/sourabhdaptardar/CourseWork/Fall2013/ComputerGraphics/SegProject/models/umu/cube.off";
    Polyhedron P;
    CGALPolyhedron2OFF::OFF2Poly(P, modelfile);

    
    
    MapAdjList A;
    map<unsigned long, Point> C;
    typedef map<unsigned long, Point>::iterator citr_t;
    weight_t **M; weight_t **dist;
    label_t **par;
    int n = int(P.size_of_facets()); // Note: we are interested in the dual mesh
    M = malloc2D<weight_t>(n, n);
    dist = malloc2D<weight_t>(n, n);
    par = malloc2D<label_t>(n, n);
    
    //conv2Adj(P,A,C,Unweighted);
    conv2Adj(P,A,C,Euclidean,M);
//    cout << "Graph:" <<endl;
//    cout << "Vertex Co-ordinates:" << endl;
//    
//    for (citr_t itr = C.begin(); itr != C.end(); ++itr) {
//        cout << itr->first << " : " << itr->second << endl;
//    }
//    cout << endl;
    
    //cout << A << endl;
  
    
//////////////////////////////////// JOHNSON /////////////////////////////////

#if 0
    BGLGraph BG;
    clock_t c1 = clock();
    time_t  t1 = time(NULL);
    poly2BGL(P,BG,Euclidean);
    calcAllPairsShortestPaths(BG,dist);
    time_t  t2 = time(NULL);
    clock_t c2 = clock();
    
    double avgdist = 0.0;
    for (int i = 0 ; i < n ; i++) {
        for (int j = 0 ; j < n ; j++) {
            avgdist += dist[i][j];
        }
    }
    avgdist /= (n*n);
    
#if 0
    cout << endl;
    for (int i = 0 ; i < n ; i++) {
        cout << i << " : ";
        for (int j = 0 ; j < n ; j++) {
            cout << " " << setw(15) << dist[i][j];
        }
        cout << endl;
    }
#endif
    
    cout << "Clock: " << (c2 - c1) / CLOCKS_PER_SEC << endl;
    cout << "Time:  " << (t2 - t1) << endl;
    cout << "Average geodesic distance (Johnson): " << avgdist << endl;
#endif

    
////////////////////////// FLOYD /////////////////////////////////////////////
    
#if 0
    clock_t c1 = clock();
    time_t  t1 = time(NULL);
    floyd(M,dist,par,n);
    time_t  t2 = time(NULL);
    clock_t c2 = clock();
    
    double avgdist = 0.0;
    for (int i = 0 ; i < n ; i++) {
        for (int j = 0 ; j < n ; j++) {
            avgdist += dist[i][j];
        }
    }
    avgdist /= (n*n);
 
#if 0
    cout << endl;
    for (int i = 0 ; i < n ; i++) {
        cout << i << " : ";
        for (int j = 0 ; j < n ; j++) {
            cout << " " << setw(15) << dist[i][j];
        }
        cout << endl;
    }
#endif

    cout << "Clock: " << (c2 - c1) / CLOCKS_PER_SEC << endl;
    cout << "Time:  " << (t2 - t1) << endl;
    cout << "Average geodesic distance  (Floyd): " << avgdist << endl;
#endif

/////////////////////// REPEATED DIJKSTRA /////////////////////////////////////
    
#if 0
    double avgdist = 0.0;
    clock_t c1 = clock();
    time_t  t1 = time(NULL);
    for (int i = 0 ; i < n ; i++) {
        dijkstra(A, i);
        double sum = 0.0;
        for (int j = 0 ; j < n ; j++) {
            sum =  sum + A.dist[j];
        }
        avgdist += sum;
    }
    avgdist /= (n*n);
    time_t  t2 = time(NULL);
    clock_t c2 = clock();
    
    cout << "Clock: " << (c2 - c1) / CLOCKS_PER_SEC << endl;
    cout << "Time:  " << (t2 - t1) << endl;
    cout << "Average geodesic distance: (Dijkstra): " << avgdist << endl;
#endif

///////////////////////////////////////////////////////////////////////
    
    free2D<weight_t>(M, n);
    free2D<weight_t>(dist, n);
    free2D<label_t>(par,n);
    
}
