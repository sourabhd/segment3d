# segment3d #

## Author: Sourabh Daptardar [ <saurabh.daptardar@gmail.com> ] ##

This code is an implementation of some of the ideas and algorithms described in "Learning 3D Mesh Segmentation and Labeling" SIGGRAPH 2010 paper by Evangelos Kalogerakis, Aaron Hertzmann, Karan Singh [ [http://people.cs.umass.edu/~kalo/papers/LabelMeshes/](http://people.cs.umass.edu/~kalo/papers/LabelMeshes/) ] as a course project - learning exercise in computer graphics.

## PLATFORMS SUPPORTED ##

Mac OS X 10.7 (Lion)


## DEPENDENCIES ##


This software uses the following libraries:

**Library**       **Description**                         **Version**                **Installation Instructions**

1) CGAL    Computational Geometry              4.2                             sudo brew install cgal
                  Algorithms library
                  libCGAL.10.0.1.dylib
                  libCGAL_Core.10.0.1.dylib
   Dependencies of CGAL:
   i) MPFR                                                        3.1.2                           sudo brew install mpfr
       libmpfr.4.dylib
   ii) GMP                                                          5.1.1                           sudo brew install gmp
       libgmp.10.dylib
                   
2) Boost      collection of C++ template          1.53.0                          sudo brew install
                   based libraries / boost header files
                                                                                                           libboost_system-mt.dylib
                                                                                                           libboost_filesystem-mt.dylib
                   
3) JointBoost Torralba et al's joint                                                       shipped with code
    boost classifier based
    on decision stumps
    Paper: http://people.csail.mit.edu/torralba/publications/sharing.pdf
    Code:  http://people.csail.mit.edu/torralba/code/sharing/sharing.zip
              

4) Matlab     Calling matlab function from C++                                    libmx.dylib
                                                                                                            libeng.h
              
               Solutions to some installation problems:
              
              http://www.cocoabuilder.com/archive/xcode/291535-simple-question-on-locating-dynamic-libraries-at-runtime.html
              http://lists.apple.com/archives/cocoa-dev/2005/Aug/msg01856.html
              http://www.mathworks.com/matlabcentral/answers/91611
              
              convert.m is provided to handle dylib 'rpath' problem 


5) GCO v-3.0  Graph Cuts : alpha expansion                                         shipped with code
                http://vision.csd.uwo.ca/code/    



## ldd output (shows all shared libraries used by the execuatable) : ##


```
#!

Sourabh-Daptardars-MacBook-Pro:Seg3D sourabhdaptardar$ ldd ../Seg3D-build/Seg3D
../Seg3D-build/Seg3D:
    /Applications/MATLAB_R2011b.app/bin/maci64/libmpfr.1.dylib (compatibility version 3.0.0, current version 3.2.0)
    /Applications/MATLAB_R2011b.app/bin/maci64/libgmp.3.dylib (compatibility version 8.0.0, current version 8.1.0)
    /usr/local/lib/libCGAL_Core.10.0.2.dylib (compatibility version 10.0.0, current version 10.0.2)
    /usr/local/lib/libCGAL.10.0.2.dylib (compatibility version 10.0.0, current version 10.0.2)
    /usr/local/opt/boost/lib/libboost_system-mt.dylib (compatibility version 0.0.0, current version 0.0.0)
    /usr/local/opt/boost/lib/libboost_filesystem-mt.dylib (compatibility version 0.0.0, current version 0.0.0)
    /usr/local/opt/boost/lib/libboost_graph-mt.dylib (compatibility version 0.0.0, current version 0.0.0)
    /Applications/MATLAB_R2011b.app/bin/maci64/libmat.dylib (compatibility version 0.0.0, current version 0.0.0)
    /Applications/MATLAB_R2011b.app/bin/maci64/libmex.dylib (compatibility version 0.0.0, current version 0.0.0)
    /Applications/MATLAB_R2011b.app/bin/maci64/libmx.dylib (compatibility version 0.0.0, current version 0.0.0)
    /Applications/MATLAB_R2011b.app/bin/maci64/libeng.dylib (compatibility version 0.0.0, current version 0.0.0)
    /usr/lib/libstdc++.6.dylib (compatibility version 7.0.0, current version 52.0.0)
    /usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 159.1.0)
```


## COMPILER VERSION ##
```
#!

Sourabh-Daptardars-MacBook-Pro:Seg3D sourabhdaptardar$ g++ --version
i686-apple-darwin11-llvm-g++-4.2 (GCC) 4.2.1 (Based on Apple Inc. build 5658) (LLVM build 2336.11.00)
Copyright (C) 2007 Free Software Foundation, Inc.
```

## MAC OS X VERSION ##

```
#!

Sourabh-Daptardars-MacBook-Pro:Seg3D sourabhdaptardar$ sw_vers
ProductName:    Mac OS X
ProductVersion: 10.7.5
BuildVersion:   11G63

i686-apple-darwin11-llvm-g++-4.2 (GCC) 4.2.1 (Based on Apple Inc. build 5658) (LLVM build 2336.11.00)

```
